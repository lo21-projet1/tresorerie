/*!
 * \file qcomboboxdelegate.cpp
 * \author Pascal Quach
 * \date 2020-06-17
 * \brief Définition de la classe QComboBoxDelegate
 * \details On vient déléguer la responsabilité d'édition
 * et d'affichage d'un modèle au QComboBoxDelegate.
 * Ici, on s'en sert pour remplacer les cellules de la colonne
 * "Compte" de la fenêtre "Ajouter Transaction" pour les opérations
 * par une liste de tous les comptes réels.
 */
#include "headers/qcomboboxdelegate.hpp"

#include <QStandardItem>

#include "headers/comptemanager.hpp"

QWidget *QComboBoxDelegate::createEditor(QWidget *parent,
                                         const QStyleOptionViewItem &option,
                                         const QModelIndex &index) const {
  auto *combo = new QComboBox(parent);
  return combo;
}

void QComboBoxDelegate::setEditorData(QWidget *editor,
                                      const QModelIndex &index) const {
  auto combo = qobject_cast<QComboBox *>(editor);
  auto CM = comptes::CompteManager::getInstance();
  auto nombre_comptes = CM->getIdsComptesReels().size();
  auto liste_noms = CM->getNomsComptesReels();
  auto liste_ids = CM->getIdsComptesReels();
  //  qDebug() << "QCBD::setEditorData : liste_ids > " << liste_ids[0] << " "
  //           << liste_ids[1] << " ";
  for (auto i = 0; i != nombre_comptes; i++) {
    combo->addItem(liste_noms[i], liste_ids[i]);
    //    qDebug() << "QCBD::setEditorData : check entry > "
    //             << combo->itemData(i, Qt::UserRole);
  }
}

void QComboBoxDelegate::setModelData(QWidget *editor, QAbstractItemModel *model,
                                     const QModelIndex &index) const {
  auto *combo_box = qobject_cast<QComboBox *>(editor);

  int selected_item = combo_box->currentIndex();
  if (selected_item == -1) {
    model->setData(index, -1, Qt::EditRole);
    model->setData(index, 0, Qt::UserRole);
    //    qDebug() << "QCBD::setModelData : data (UserRole) > "
    //             << model->data(index, Qt::UserRole);
    model->setData(index, combo_box->currentText(), Qt::DisplayRole);
  } else {
    model->setData(index, selected_item, Qt::EditRole);
    model->setData(index, combo_box->itemData(selected_item), Qt::UserRole);
    model->setData(index, combo_box->itemText(selected_item), Qt::DisplayRole);
  }
}
