/*!
 * \file comptemanager.cpp
 * \author Bastien BOURÉ
 * \author Pascal Quach
 * \author Rémy HUET <utc@remyhuet.fr>
 * \date 2020-06-14
 * \brief Déclarations pour la classe CompteManager
 */

#include "headers/comptemanager.hpp"

#include <QMessageBox>
#include <QPrinter>
#include <QTextDocument>

#include "headers/abstractcomptereel.hpp"
#include "headers/comptevirtuel.hpp"

void comptes::CompteManager::Init() {
  auto SM = StorageManager::getStorageManager();

  qDebug() << "Initialisation CM";
  if (!comptes_charges.empty()) {
    qDebug("CM non vide suppression des comptes");
    supprimerComptes();
    for (auto it : root) {
      qDebug() << it->getNom();
    }
  }
  qDebug() << "CM::Init() : SM->getComptesRacine() empty ? >"
           << SM->getComptesRacine().isEmpty();
  for (auto it : SM->getComptesRacine()) {
    qDebug() << "Get comptes racine " << it->getId();
    root.append(it);
    comptes_charges.push_back(it);
    if (it->isVirtuel()) {
      InitFils(it);
    }
  }
}

void comptes::CompteManager::InitFils(comptes::AbstractCompte* pere) {
  std::shared_ptr<StorageManager> SM = StorageManager::getStorageManager();
  for (auto it : SM->getComptesFils(pere)) {
    qDebug() << "Get comptes racine " << pere->getId();
    qDebug() << it->getId();
    qDebug() << it->getNom();
    qDebug() << it->getType();
    if (it == nullptr) {
      qDebug() << "CM::InitFils ERREUR : le compte fils est nul -> error BDD";
    } else {
      comptes_charges.push_back(it);
      if (it->isVirtuel()) {
        InitFils(it);
      }
    }
  }
}

QVector<comptes::AbstractCompte*> comptes::CompteManager::getComptes(
    QString const& nom) const {
  QVector<comptes::AbstractCompte*> occurences;
  for (QVector<comptes::AbstractCompte*>::const_iterator it =
           comptes_charges.begin();
       it != comptes_charges.end(); ++it) {
    if ((*it)->getNom() == nom) {
      occurences.append(*it);
    }
  }
  return occurences;
}

comptes::AbstractCompte* comptes::CompteManager::getCompte(QString const& nom) {
  for (QVector<comptes::AbstractCompte*>::const_iterator it =
           comptes_charges.begin();
       it != comptes_charges.end(); ++it) {
    if ((*it)->getNom() == nom) {
      return *it;
    }
  }

  return nullptr;
}

comptes::AbstractCompte* comptes::CompteManager::getCompte(int id) {
  for (QVector<comptes::AbstractCompte*>::const_iterator it =
           comptes_charges.begin();
       it != comptes_charges.end(); ++it) {
    if ((*it)->getId() == id) {
      return *it;
    }
  }

  return nullptr;
}

QVector<int> comptes::CompteManager::getIdsComptesReels() {
  auto comptes = getComptesReels();
  QVector<int> liste_comptes;
  for (auto compte : comptes) {
    liste_comptes.append(compte->getId());
    qDebug() << liste_comptes << "\n";
  }
  return liste_comptes;
}

QStringList comptes::CompteManager::getNomsComptesReels() {
  auto CM = comptes::CompteManager::getInstance();
  auto comptes = CM->getComptesReels();

  QStringList liste_comptes;
  for (auto compte : comptes) {
    liste_comptes.append(QString(compte->getNom()));
    qDebug() << liste_comptes << "\n";
  }
  return liste_comptes;
}

QStringList comptes::CompteManager::getNomsComptesFilsReelsCompteVirtuel(
    comptes::AbstractCompte* compte) {
  QStringList liste_noms;
  if (compte->isVirtuel()) {
    auto comptes_fils = dynamic_cast<comptes::AbstractCompteVirtuel*>(compte)
                            ->getCompteFilsAbstract();
    for (auto compte_fils : comptes_fils) {
      liste_noms += getNomsComptesFilsReelsCompteVirtuel(compte_fils);
    }
  } else {
    liste_noms.append(compte->getNom());
    return liste_noms;
  }
  return liste_noms;
  qDebug()
      << "comptes::CM::getNomsComptesFilsCompteVirtuel : something went wrong";
}
QStringList comptes::CompteManager::getNomsComptesFilsReelsCompteVirtuel(
    comptes::AbstractCompte const* compte) {
  QStringList liste_noms;
  if (compte->isVirtuel()) {
    auto comptes_fils =
        dynamic_cast<comptes::AbstractCompteVirtuel const*>(compte)
            ->getCompteFilsAbstract();
    for (auto compte_fils : comptes_fils) {
      liste_noms += getNomsComptesFilsReelsCompteVirtuel(compte_fils);
    }
  } else {
    liste_noms.append(compte->getNom());
    return liste_noms;
  }
  return liste_noms;
  qDebug()
      << "comptes::CM::getNomsComptesFilsCompteVirtuel : something went wrong";
}

comptes::AbstractCompte* comptes::CompteManager::creerCompte(
    const QString& nom, AbstractCompte& parent, bool virtuel,
    const Monnaie& solde, Compte<PASSIFS>* compteSInitial,
    const QDate& dateSInitial, const QDate& dateDernierRapprochement) {
  AbstractCompte* c = comptes::CompteFactory::creerCompte(
      nom, parent, virtuel, solde, compteSInitial, dateSInitial,
      dateDernierRapprochement);
  comptes_charges.append(c);

  StorageManager::getStorageManager()->ajouterCompte(c);
  StorageManager::getStorageManager()->updateCompte(&parent);
  StorageManager::getStorageManager()->updateCompte(c);
  return c;
}

comptes::AbstractCompte* comptes::CompteManager::creerCompte(
    const QString& nom, TypeCompte type, bool virtuel, const Monnaie& solde,
    Compte<PASSIFS>* compteSInitial, const QDate& dateSInitial,
    const QDate& dateDernierRapprochement) {
  AbstractCompte* c = comptes::CompteFactory::creerCompte(
      nom, type, virtuel, solde, compteSInitial, dateSInitial,
      dateDernierRapprochement);
  comptes_charges.append(c);
  root.append(c);
  StorageManager::getStorageManager()->ajouterCompte(c);
  StorageManager::getStorageManager()->updateCompte(c);
  return c;
}

comptes::AbstractCompte* comptes::CompteManager::creerCompte(
    unsigned int id, const QString& nom, TypeCompte type, bool virtuel,
    const Monnaie& solde, Compte<PASSIFS>* compteSInitial,
    const QDate& dateSInitial, const QDate& dateDernierRapprochement) {
  AbstractCompte* c = comptes::CompteFactory::creerCompte(
      id, nom, type, virtuel, solde, compteSInitial, dateSInitial,
      dateDernierRapprochement);
  comptes_charges.append(c);
  root.append(c);
  StorageManager::getStorageManager()->ajouterCompte(c);
  StorageManager::getStorageManager()->updateCompte(c);

  return c;
}

comptes::AbstractCompte* comptes::CompteManager::creerCompte(
    unsigned int id, const QString& nom, AbstractCompte& parent, bool virtuel,
    const Monnaie& solde, Compte<PASSIFS>* compteSInitial,
    const QDate& dateSInitial, const QDate& dateDernierRapprochement) {
  AbstractCompte* c = comptes::CompteFactory::creerCompte(
      id, nom, parent, virtuel, solde, compteSInitial, dateSInitial,
      dateDernierRapprochement);
  comptes_charges.append(c);

  StorageManager::getStorageManager()->ajouterCompte(c);
  StorageManager::getStorageManager()->updateCompte(&parent);
  StorageManager::getStorageManager()->updateCompte(c);
  return c;
}

void comptes::CompteManager::debiterCompte(comptes::AbstractCompte* c,
                                           Monnaie debit) {
  dynamic_cast<comptes::AbstractCompteReel*>(c)->debiter(debit);
}

void comptes::CompteManager::crediterCompte(comptes::AbstractCompte* c,
                                            Monnaie credit) {
  dynamic_cast<comptes::AbstractCompteReel*>(c)->crediter(credit);
}

void comptes::CompteManager::supprimerComptes() {
  for (auto it : comptes_charges) {
    delete it;
  }
  comptes_charges.clear();
  root.clear();
}

void comptes::CompteManager::pathStuff() {
  QDir dir = QDir();
  if (!dir.exists("rapports-generes")) {
    dir.mkpath("./rapports-generes");
  }
}

void comptes::CompteManager::updateComptes(const Transaction* tr,
                                           bool suppression) {
  auto CM = comptes::CompteManager::getInstance();
  for (auto op : tr->getOperations()) {
    qDebug() << "On entre dans updateComptes";
    if (op->getDebit() == Monnaie("0,00")) {
      if (suppression) {
        CM->debiterCompte(&(op->getCompte()), op->getCredit().getMontant());
      } else {
        CM->crediterCompte(&(op->getCompte()), op->getCredit().getMontant());
      }

      qDebug() << "Nouveau solde :" << op->getCompte().getSolde().getMontant();

    } else {
      if (suppression) {
        CM->crediterCompte(&(op->getCompte()), op->getDebit().getMontant());
      } else {
        CM->debiterCompte(&(op->getCompte()), op->getDebit().getMontant());
      }
      qDebug() << "Nouveau solde :" << op->getCompte().getSolde().getMontant();
    }
  }
}

bool comptes::CompteManager::isEmpty() { return comptes_charges.isEmpty(); }

QVector<comptes::AbstractCompte*> comptes::CompteManager::getComptesReels() {
  auto comptes = getComptes();
  for (auto it = comptes.begin(); it != comptes.end();) {
    if ((*it)->isVirtuel()) {
      it = comptes.erase(it);
    } else {
      ++it;
    }
  }
  return comptes;
}

bool comptes::CompteManager::isRoot(comptes::AbstractCompte* c) const {
  for (auto it : root) {
    if (c == it) {
      return true;
    }
  }
  return false;
}

QString comptes::CompteManager::arbreCompteToHtml(comptes::AbstractCompte* c,
                                                  QDate const& date,
                                                  int profondeur) const {
  QString str;
  str.append("<TR>");
  str.append("<TD>");
  for (int i(1); i <= 6 * (profondeur); i++) {
    str.append("&nbsp;");
  }
  str.append(c->getNom() + "</TD><TD>");
  for (int i(0); i <= 15; i++) {
    str.append("&nbsp;");
  }
  str.append(c->getSoldeByDate(date).getMontant() + "€</TD>");
  str.append("</TR>");
  if (c->isVirtuel()) {
    for (auto it : dynamic_cast<comptes::AbstractCompteVirtuel*>(c)
                       ->getCompteFilsAbstract()) {
      str.append(arbreCompteToHtml(it, date, profondeur + 1));
    }
  }
  return str;
}

void comptes::CompteManager::genererBilan(const QDate& date,
                                          const QString& filename) {
  pathStuff();
  QTextDocument doc;
  QVector<comptes::AbstractCompte*> actifs;
  QVector<comptes::AbstractCompte*> passifs;
  Monnaie solde_actifs;
  Monnaie solde_passifs;
  /*Récupération des comptes passifs et actifs*/
  for (auto compte : comptes_charges) {
    if (compte->getType() == comptes::TypeCompte::ACTIFS) {
      actifs.push_back(compte);
    } else if (compte->getType() == comptes::TypeCompte::PASSIFS) {
      passifs.push_back(compte);
    }
  }
  QString str;
  str.append(
      "<TABLE BORDER=\"0.5\", width=\"600\"><CAPTION "
      "style=\"font-size:25px\"> "
      "BILAN des compte" +
      getNomAssociation() + " du " + date.toString("dd/MM/yyyy") +
      " </CAPTION>");
  for (auto it : actifs) {
    if (isRoot(it)) {
      str.append(arbreCompteToHtml(it, date));
      solde_actifs += it->getSoldeByDate(date);
    }
  }
  str.append("<TR>");
  str.append("<TD><b>TOTAL ACTIFS : </b></TD>");
  str.append("<TD><b>");
  for (int i(0); i <= 15; i++) {
    str.append("&nbsp;");
  }
  str.append(solde_actifs.getMontant()+"€</b></TD>");
  str.append("</TR>");
  str.append("<TR>");
  str.append("<TD colspan=\"2\"></TD>");
  str.append("</TR>");
  for (auto it : passifs) {
    if (isRoot(it)) {
      str.append(arbreCompteToHtml(it, date));
      solde_passifs += it->getSoldeByDate(date);
    }
  }
  str.append("<TR>");
  str.append("<TD><b>TOTAL PASSIFS : </b></TD>");
  str.append("<TD><b>");
  for (int i(0); i <= 15; i++) {
    str.append("&nbsp;");
  }
  str.append(solde_passifs.getMontant()+"€</b></TD>");
  str.append("</TR>");
  str.append("<TR>");
  str.append("<TD colspan=\"2\"></TD>");
  str.append("</TR>");
  str.append("<TR>");
  str.append("<TD><b>ACTIFS-PASSIFS</b></TD><TD><b>");
  for (int i(0); i <= 15; i++) {
    str.append("&nbsp;");
  }
  str.append(QString(solde_actifs - solde_passifs) + "€</b></TD>");
  str.append("</TR>");
  str.append("</TABLE>");
  doc.setHtml(str);
  QPrinter printer;
  printer.setOutputFileName("rapports-generes/" + filename + ".pdf");
  printer.setOutputFormat(QPrinter::PdfFormat);
  doc.print(&printer);
  printer.newPage();
}

void comptes::CompteManager::genererResultat(const QDate& date,
                                             const QString& filename) {
  pathStuff();
  QTextDocument doc;
  QVector<comptes::AbstractCompte*> revenus;
  QVector<comptes::AbstractCompte*> depenses;
  Monnaie solde_revenus;
  Monnaie solde_depenses;
  /*Récupération des comptes passifs et actifs*/
  for (auto compte : comptes_charges) {
    if (compte->getType() == comptes::TypeCompte::REVENUS) {
      revenus.push_back(compte);
    } else if (compte->getType() == comptes::TypeCompte::DEPENSES) {
      depenses.push_back(compte);
    }
  }
  QString str;
  str.append(
      "<TABLE BORDER=\"0.5\", width=\"600\"><CAPTION "
      "style=\"font-size:25px\"> "
      "Compte de résultat" +
      getNomAssociation() + " du " + date.toString("dd/MM/yyyy") +
      " </CAPTION>");
  for (auto it : revenus) {
    if (isRoot(it)) {
      str.append(arbreCompteToHtml(it, date));
      solde_revenus += it->getSoldeByDate(date);
    }
  }
  str.append("<TR>");
  str.append("<TD><b>TOTAL RECETTES : </b></TD>");
  str.append("<TD><b>");
  for (int i(0); i <= 15; i++) {
    str.append("&nbsp;");
  }
  str.append(solde_revenus.getMontant()+"€</b></TD>");
  str.append("</TR>");
  str.append("<TR>");
  str.append("<TD colspan=\"2\"></TD>");
  str.append("</TR>");
  for (auto it : depenses) {
    if (isRoot(it)) {
      str.append(arbreCompteToHtml(it, date));
      solde_depenses += it->getSoldeByDate(date);
    }
  }
  str.append("<TR>");
  str.append("<TD><b>TOTAL DEPENSES : </b></TD>");
  str.append("<TD><b>");
  for (int i(0); i <= 15; i++) {
    str.append("&nbsp;");
  }
  str.append(solde_depenses.getMontant()+"€</b></TD>");
  str.append("</TR>");
  str.append("<TR>");
  str.append("<TD colspan=\"2\"></TD>");
  str.append("</TR>");
  str.append("<TR>");
  str.append("<TD><b>BÉNÉFICES</b></TD><TD><b>");
  for (int i(0); i <= 15; i++) {
    str.append("&nbsp;");
  }
  str.append(QString(solde_revenus - solde_depenses) + "€</b></TD>");
  str.append("</TR>");
  str.append("</TABLE>");
  doc.setHtml(str);
  QPrinter printer;
  printer.setOutputFileName("rapports-generes/" + filename + ".pdf");
  printer.setOutputFormat(QPrinter::PdfFormat);
  doc.print(&printer);
  printer.newPage();
}
