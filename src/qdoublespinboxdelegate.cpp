/*!
 * \file qdoublespinboxdelegate.cpp
 * \author Pascal Quach
 * \date 2020-06-17
 * \brief Définition de la classe qdoublespinboxdelegate
 * \details On vient déléguer la responsabilité d'édition
 * et d'affichage d'un modèle au qdoublespinboxdelegate.
 * Ici, on s'en sert pour remplacer les cellules des colonnes
 * "Débit" et "Crédit" de la fenêtre "Ajouter Transaction" pour les opérations
 * par un QWidget QDoubleSpinBox
 */
#include "headers/qdoublespinboxdelegate.hpp"

#include <QDoubleSpinBox>
#include <QStandardItem>

#include "headers/comptemanager.hpp"

QWidget *QDoubleSpinBoxDelegate::createEditor(
    QWidget *parent, const QStyleOptionViewItem &option,
    const QModelIndex &index) const {
  auto spinbox = new QDoubleSpinBox(parent);
  spinbox->setMinimum(0.00);
  spinbox->setMaximum(99999.99);
  spinbox->setStepType(QAbstractSpinBox::StepType::AdaptiveDecimalStepType);

  return spinbox;
}

void QDoubleSpinBoxDelegate::setEditorData(QWidget *editor,
                                           const QModelIndex &index) const {
  auto spinbox = qobject_cast<QDoubleSpinBox *>(editor);
  auto CM = comptes::CompteManager::getInstance();
  spinbox->setValue(index.model()->data(index, Qt::EditRole).toDouble());
}

void QDoubleSpinBoxDelegate::setModelData(QWidget *editor,
                                          QAbstractItemModel *model,
                                          const QModelIndex &index) const {
  auto spinbox = qobject_cast<QDoubleSpinBox *>(editor);
  spinbox->interpretText();
  auto current_value = QString::number(spinbox->value(), 'f', 2);
  model->setData(index, current_value, Qt::EditRole);
  model->setData(index, current_value, Qt::DisplayRole);
}
