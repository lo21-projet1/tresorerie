/*!
 * \file fenetreprincipale.cpp
 * \author Rémy HUET <utc@remyhuet.fr>
 * \author Arthur Wacquez <arthur.wacquez@etu.utc.fr>
 * \date 2020-06-13
 * \brief Implémentation de la classe FenetrePrincipale
 */

#include "headers/fenetreprincipale.hpp"

#include <QDebug>
#include <QInputDialog>
#include <QList>
#include <QMessageBox>

#include "headers/abstractcompte.hpp"
#include "headers/comptemanager.hpp"
#include "headers/comptevirtuel.hpp"
#include "headers/fenetreajoutertransaction.hpp"
#include "headers/fenetrecreationcompte.hpp"
#include "headers/fenetregenerationrapport.hpp"
#include "headers/fenetremodifiertransaction.hpp"
#include "headers/fenetrerapprochementcompte.hpp"
#include "headers/modelearbrescomptes.hpp"
#include "headers/monnaie.hpp"
#include "headers/storagemanager.hpp"
#include "headers/transactionmanager.hpp"
#include "ui_fenetrecreationcompte.h"
#include "ui_fenetreprincipale.h"

FenetrePrincipale::FenetrePrincipale(QWidget* parent)
    : QMainWindow(parent), ui(new Ui::FenetrePrincipale) {
  ui->setupUi(this);
  /* Définition du model pour l'affichage des comptes*/
  arbre_comptes = new ModeleArbreCompte{0, 3};
  arbre_comptes->setHorizontalHeaderLabels({"Compte", "Type", "Solde"});

  ui->arbres_comptes->setHeaderHidden(false);
  ui->arbres_comptes->setModel(arbre_comptes);

  ui->arbres_comptes->resizeColumnToContents(0);
  ui->arbres_comptes->resizeColumnToContents(1);
  ui->arbres_comptes->resizeColumnToContents(2);

  /*Affichage Transactions*/
  liste_transactions = new QStandardItemModel{0, 7};
  liste_transactions->setHorizontalHeaderLabels(
      {"Date", "Référence", "Titre", "Comptes", "Débit", "Crédit", "Solde"});

  ui->liste_transactions->setModel(liste_transactions);
  ui->liste_transactions->horizontalHeader()->setSectionResizeMode(
      QHeaderView::ResizeToContents);
  ui->liste_transactions->horizontalHeader()->setStretchLastSection(true);
  ui->liste_transactions->verticalHeader()->setVisible(false);

  /*Connection lors du changement de selection via le selectionModel*/
  QObject::connect(
      ui->arbres_comptes->selectionModel(),
      SIGNAL(selectionChanged(const QItemSelection&, const QItemSelection&)),
      this,
      SLOT(selectionCompte(const QItemSelection&, const QItemSelection&)));

  QObject::connect(
      ui->liste_transactions->selectionModel(),
      SIGNAL(selectionChanged(const QItemSelection&, const QItemSelection&)),
      this,
      SLOT(selectionTransaction(const QItemSelection&, const QItemSelection&)));

  // Load contexte dernier
  QFile context("./context.json");
  context.open(QFile::ReadOnly);
  if (context.exists()) {
    QMessageBox load;
    load.setText("Voulez-vous charger le fichier précedemment ouvert ?");
    load.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
    int choice = load.exec();
    if (choice == QMessageBox::Yes) {
      QJsonDocument values = QJsonDocument::fromJson(context.readAll());
      context.close();
      QString filepath = values.object().value("sqliteFilePath").toString();

      qDebug() << filepath;
      StorageManager::getStorageManager()->ouvrirFichier(filepath);

      // Init
      comptes::CompteManager::getInstance()->Init();
      enableActions();
      RefreshAffichageCompte();
      RefreshAffichageTransaction();
    }
  }
}

FenetrePrincipale::~FenetrePrincipale() {
  delete ui;
  if (!StorageManager::getStorageManager()->getLoadedFile().isEmpty()) {
    QJsonObject context;
    context.insert("sqliteFilePath",
                   StorageManager::getStorageManager()->getLoadedFile());
    QFile contextFile("./context.json");
    contextFile.open(QIODevice::ReadWrite | QIODevice::Truncate);
    contextFile.write(QJsonDocument(context).toJson());
    contextFile.close();
  }
}

/*Slot de selection*/
void FenetrePrincipale::selectionCompte(const QItemSelection& selected,
                                        const QItemSelection& deselected) {
  auto CM = comptes::CompteManager::getInstance();
  auto TM = TransactionManager::getInstance();
  ui->action_modifier_transfert->setEnabled(false);
  comptes::AbstractCompte* compte =
      CM->getCompte(selected.indexes().first().data().toString());
  qDebug() << "FP::selectionCompte : nouveau compte sélectionné : "
           << compte->getNom();
  enableActions(compte->isVirtuel());
  TM->chargerCompte(compte);
  RefreshAffichageTransaction(compte);
  qDebug() << selected.indexes().first().data().toString();
}

void FenetrePrincipale::selectionTransaction(const QItemSelection& selected,
                                             const QItemSelection& deselected) {
  QItemSelectionModel* select = ui->liste_transactions->selectionModel();
  if (select->hasSelection()) {
    int row = select->selectedIndexes().first().row();
    QString ref =
        ui->liste_transactions->model()->index(row, 1).data().toString();
    QDate date = QDate::fromString(
        ui->liste_transactions->model()->index(row, 0).data().toString(),
        "dd/MM/yyyy");
    while (ref.isEmpty() || ref.isNull() || date.isNull()) {
      ref = ui->liste_transactions->model()->index(--row, 1).data().toString();
      date = QDate::fromString(
          ui->liste_transactions->model()->index(row, 0).data().toString(),
          "dd/MM/yyyy");
    }
    Transaction* transaction_modif =
        TransactionManager::getInstance()->getTransaction(ref, date);
    if (transaction_modif->isRapproche()) {
      ui->action_modifier_transfert->setEnabled(false);
    } else {
      ui->action_modifier_transfert->setEnabled(true);
    }
  } else {
    ui->action_modifier_transfert->setEnabled(false);
  }
}

/*Affichage des comptes */
void FenetrePrincipale::AfficherComptes() {
  auto CM = comptes::CompteManager::getInstance();
  if (CM->getComptesRoot().isEmpty()) {
    QMessageBox::warning(
        this, "Impossible d'afficher les comptes",
        "Il n'y a pas de compte racine : affichage des comptes impossible.");
  } else {
    for (auto it : CM->getComptesRoot()) {
      QString type;
      switch (it->getType()) {
        case comptes::TypeCompte::ACTIFS:
          type = "Actif";
          break;
        case comptes::TypeCompte::PASSIFS:
          type = "Passif";
          break;
        case comptes::TypeCompte::DEPENSES:
          type = "Depenses";
          break;
        case comptes::TypeCompte::REVENUS:
          type = "Revenus";
          break;
      }
      QList<QStandardItem*> r;
      QStandardItem* nom = new QStandardItem(it->getNom());
      r << nom << new QStandardItem(type)
        << new QStandardItem(QString(it->getSolde()));
      if (it->isVirtuel()) {
        for (auto it2 : AfficherFils(it)) {
          nom->appendRow(it2);
        }
      }
      arbre_comptes->appendRow(r);
    }
  }
  ui->arbres_comptes->resizeColumnToContents(0);
  ui->arbres_comptes->resizeColumnToContents(1);
  ui->arbres_comptes->resizeColumnToContents(2);
}

QVector<QList<QStandardItem*>> FenetrePrincipale::AfficherFils(
    comptes::AbstractCompte* pere) {
  QVector<QList<QStandardItem*>> vec;
  switch (pere->getType()) {
    case comptes::TypeCompte::ACTIFS:
      for (auto it :
           dynamic_cast<comptes::CompteVirtuel<comptes::TypeCompte::ACTIFS>*>(
               pere)
               ->getSousCompte()) {
        QList<QStandardItem*> sous_compte;
        QStandardItem* nom = new QStandardItem(it->getNom());
        sous_compte << nom << new QStandardItem("Actif")
                    << new QStandardItem(QString(it->getSolde()));
        if (it->isVirtuel()) {
          for (auto it2 : AfficherFils(it)) {
            nom->appendRow(it2);
          }
        }
        vec.append(sous_compte);
      }
      break;
    case comptes::TypeCompte::PASSIFS:
      for (auto it :
           dynamic_cast<comptes::CompteVirtuel<comptes::TypeCompte::PASSIFS>*>(
               pere)
               ->getSousCompte()) {
        QList<QStandardItem*> sous_compte;
        QStandardItem* nom = new QStandardItem(it->getNom());
        sous_compte << nom << new QStandardItem("Passif")
                    << new QStandardItem(QString(it->getSolde()));
        if (it->isVirtuel()) {
          for (auto it2 : AfficherFils(it)) {
            nom->appendRow(it2);
          }
        }
        vec.append(sous_compte);
      }
      break;
    case comptes::TypeCompte::DEPENSES:
      for (auto it :
           dynamic_cast<comptes::CompteVirtuel<comptes::TypeCompte::DEPENSES>*>(
               pere)
               ->getSousCompte()) {
        QList<QStandardItem*> sous_compte;
        QStandardItem* nom = new QStandardItem(it->getNom());
        sous_compte << nom << new QStandardItem("Depense")
                    << new QStandardItem(QString(it->getSolde()));
        if (it->isVirtuel()) {
          for (auto it2 : AfficherFils(it)) {
            nom->appendRow(it2);
          }
        }
        vec.append(sous_compte);
      }
      break;
    case comptes::TypeCompte::REVENUS:
      for (auto it :
           dynamic_cast<comptes::CompteVirtuel<comptes::TypeCompte::REVENUS>*>(
               pere)
               ->getSousCompte()) {
        QList<QStandardItem*> sous_compte;
        QStandardItem* nom = new QStandardItem(it->getNom());
        sous_compte << nom << new QStandardItem("Revenus")
                    << new QStandardItem(QString(it->getSolde()));
        if (it->isVirtuel()) {
          for (auto it2 : AfficherFils(it)) {
            nom->appendRow(it2);
          }
        }
        vec.append(sous_compte);
      }
      break;
  }
  return vec;
}

void FenetrePrincipale::RefreshAffichageCompte(bool affichage) {
  arbre_comptes->clear();
  arbre_comptes->setHorizontalHeaderLabels({"Compte", "Type", "Solde"});
  if (affichage) {
    AfficherComptes();
  }
}

/*Affichage Transactions*/
void FenetrePrincipale::AfficherTransactions(comptes::AbstractCompte* compte) {
  auto TM = TransactionManager::getInstance();
  auto liste_noms = comptes::CompteManager::getInstance()
                        ->getNomsComptesFilsReelsCompteVirtuel(compte);
  Monnaie solde(compte->getSolde());

  QVector<Transaction*> transactions_charges =
      TM->getTransactionsByCompte(*compte);
  if (compte->isVirtuel()) {
    qDebug() << "FP::AfficherTransactions : le compte est virtuel > "
             << compte->getNom();
    if (!transactions_charges.isEmpty())
      qDebug() << "FP::AfficherTransactions : trouvé transactions >";
    for (auto tr : transactions_charges) {
      qDebug() << "tr : " << tr->getDate() << " " << tr->getReference() << " "
               << tr->getDescription();
    }
  }

  for (auto it : transactions_charges) {
    QList<QStandardItem*> trans;
    QList<QStandardItem*> info;
    auto test = it->getReference();
    info << new QStandardItem(it->getDate().toString("dd/MM/yyyy"))
         << new QStandardItem(it->getReference())
         << new QStandardItem(it->getDescription());
    for (auto it1 : it->getOperations()) {
      qDebug() << "Compte : " << QString(it1->getCompte().getNom())
               << " Débit : " << QString(it1->getDebit().getMontant())
               << " Crédit : " << QString(it1->getCredit().getMontant());
      if (liste_noms.contains(it1->getCompte().getNom())) {
        trans << info << new QStandardItem(it1->getCompte().getNom())
              << new QStandardItem(it1->getDebit().getMontant())
              << new QStandardItem(it1->getCredit().getMontant())
              << new QStandardItem(solde.getMontant());
        liste_transactions->appendRow(trans);
        trans.clear();

        // Calcul solde inversé pour "revenir dans le temps"
        if (compte->getType() == comptes::TypeCompte::ACTIFS ||
            compte->getType() == comptes::TypeCompte::DEPENSES) {
          solde += it1->getCredit();
          solde -= it1->getDebit();
        } else {
          solde -= it1->getCredit();
          solde += it1->getDebit();
        }
      }
    }
  }
}

void FenetrePrincipale::RefreshAffichageTransaction(
    comptes::AbstractCompte* compte) {
  liste_transactions->clear();
  liste_transactions->setHorizontalHeaderLabels(
      {"Date", "Référence", "Titre", "Comptes", "Débit", "Crédit", "Solde"});
  if (compte != nullptr) AfficherTransactions(compte);
}

/*Rapprochement*/
void FenetrePrincipale::on_action_rapprocher_triggered() {
  auto CM = comptes::CompteManager::getInstance();
  auto TM = TransactionManager::getInstance();
  if (!TM->isEmpty()) {
    auto compte = CM->getCompte(ui->arbres_comptes->selectionModel()
                                    ->selectedIndexes()
                                    .first()
                                    .data()
                                    .toString());
    FenetreRapprochementCompte r(this, compte);
    auto result = r.exec();
    if (result != QDialog::Accepted) {
      qDebug() << "Pas accepté";
      StorageManager::getStorageManager()->updateCompte(compte);
      r.close();
    } else {
    }
  } else {
    QMessageBox::warning(this, "Erreur rapprochement",
                         "Il n'existe aucune transactions à rapprocher.");
  }
}

/*Cloture du livre*/
void FenetrePrincipale::on_action_clore_livre_triggered() {
  QString ref =
      QInputDialog::getText(this, "Référence clôture",
                            "Quel nom voulez vous donner à la"
                            "référence de cette cloture ? (ex : 'CL2019')");
  QString desc = QInputDialog::getText(
      this, "Description clôture",
      "Quel description voulez vous donner"
      " à la référence de cette cloture ? (ex : 'Cloture 2019')");
  auto CM = comptes::CompteManager::getInstance();
  auto TM = TransactionManager::getInstance();
  QVector<comptes::AbstractCompte*> comptes = CM->getComptes();
  QVector<Operation*> operationsD;
  QVector<Operation*> operationsR;
  Monnaie soldeD("0,00");
  Monnaie soldeR("0,00");
  comptes::AbstractCompte* resultat = nullptr;
  comptes::AbstractCompte* excedent = nullptr;
  comptes::AbstractCompte* deficit = nullptr;
  /* Recherche des opérations des comptes de dépense et de recette */
  for (auto it = comptes.begin(); it != comptes.end(); ++it) {
    if ((*it)->getNom() == "Résultat" &&
        (*it)->getType() == comptes::TypeCompte::PASSIFS)
      resultat = *(it);
    if ((*it)->getNom() == "Excédent" &&
        (*it)->getType() == comptes::TypeCompte::PASSIFS)
      excedent = *(it);
    if ((*it)->getNom() == "Déficit" &&
        (*it)->getType() == comptes::TypeCompte::PASSIFS)
      deficit = *(it);
    if ((*it)->getType() == comptes::TypeCompte::DEPENSES)
      if (!((*it)->isVirtuel()) && (*it)->getSolde() != 0) {
        operationsD.push_back(
            TM->creerOperation(*(*it), Monnaie("0,00"), (*it)->getSolde()));
        soldeD += (*it)->getSolde();
        qDebug() << "Compte Dépense : " << (*it)->getNom();
      }
    if ((*it)->getType() == comptes::TypeCompte::REVENUS)
      if (!((*it)->isVirtuel()) && (*it)->getSolde() != 0) {
        operationsR.push_back(
            TM->creerOperation(*(*it), (*it)->getSolde(), Monnaie("0,00")));
        soldeR += (*it)->getSolde();
        qDebug() << "Compte Recette : " << (*it)->getNom();
      }
  }
  /*QVector<comptes::AbstractCompte*> root = CM->getComptesRoot();
  comptes::AbstractCompte* P = nullptr;
  for (auto it = root.begin(); it != root.end(); ++it)
    if ((*it)->getType() == comptes::TypeCompte::PASSIFS) P = (*it);
  if (P == nullptr) qDebug() << "Le compte racine passif n'a pas été trouvé.";*/
  if (resultat == nullptr)
    resultat = CM->creerCompte("Résultat", comptes::PASSIFS, false);
  /* 1e transaction répartie. Remise à zéro des comptes de dépense */
  operationsD.push_back(TM->creerOperation(*resultat, soldeD, Monnaie("0,00")));
  TM->ajouterTransaction(QDate::currentDate(), ref + 'D', desc, operationsD);
  /* 2e transaction répartie. Remise à zéro des comptes de recette */
  operationsR.push_back(TM->creerOperation(*resultat, Monnaie("0,00"), soldeR));
  TM->ajouterTransaction(QDate::currentDate(), ref + 'R', desc, operationsR);
  /* 2e transaction répartie. Remise à zéro du compte Résultat */
  if (resultat->getSolde() != Monnaie("0,00")) {
    if (resultat->getSolde() > Monnaie("0,00")) {
      if (excedent == nullptr)
        excedent = CM->creerCompte("Excédent", comptes::PASSIFS, false);
      QVector<Operation*> operationsE;
      operationsE.push_back(
          TM->creerOperation(*resultat, resultat->getSolde(), Monnaie("0,00")));
      operationsE.push_back(
          TM->creerOperation(*excedent, Monnaie("0,00"), resultat->getSolde()));
      TM->ajouterTransaction(QDate::currentDate(), ref + 'E', desc,
                             operationsE);
    } else {
      if (deficit == nullptr)
        deficit = CM->creerCompte("Déficit", comptes::PASSIFS, false);
      QVector<Operation*> operationsF;
      operationsF.push_back(
          TM->creerOperation(*resultat, resultat->getSolde(), Monnaie("0,00")));
      operationsF.push_back(
          TM->creerOperation(*deficit, Monnaie("0,00"), resultat->getSolde()));
      TM->ajouterTransaction(QDate::currentDate(), ref + 'F', desc,
                             operationsF);
    }
  }
  RefreshAffichageCompte();
}

/*Transfert*/
void FenetrePrincipale::on_action_ajouter_transfert_triggered() {
  auto CM = comptes::CompteManager::getInstance();
  auto TM = TransactionManager::getInstance();
  FenetreAjouterTransaction form(this);
  auto result = form.exec();
  if (result == QDialog::Accepted) {
    //    qDebug() << "Aye, there. FenetrePrincipale from "
    //                "on_action_ajouter_transfert_TRIGGERED! Grr. Just checking
    //                "
    //                "the operations are actually there, at all.";
    for (auto op : form.getOperations()) {
      qDebug() << "Compte : " << QString(op->getCompte().getNom())
               << " Débit : " << QString(op->getDebit())
               << " Crédit : " << QString(op->getCredit());
    }
    qDebug() << "Everything looks alright to you, uh, UH, UUUH?!";
    auto transaction =
        TM->creerTransaction(form.getDate(), form.getReference(),
                             form.getDescription(), form.getOperations());
    if (transaction == nullptr) {
      QMessageBox::warning(this, "Erreur création de transaction",
                           "La création de la transaction a échoué.");
    } else {
      TM->ajouterTransaction(form.getDate(), form.getReference(),
                             form.getDescription(), form.getOperations());
      qDebug() << "Transaction créée.";
    }
    RefreshAffichageCompte();
  }
}

/*Fichiers*/
void FenetrePrincipale::on_action_nouveau_fichier_triggered() {
  QString titre =
      QInputDialog::getText(this, "Titre",
                            "Quel nom voulez vous donner"
                            "à votre nouveau fichier ? (ex : 'PTE_Start_UT')");
  auto CM = comptes::CompteManager::getInstance();
  auto DBM = StorageManager::getStorageManager();
  if (!titre.isEmpty() && !titre.isNull()) {
    QFile file(titre + ".sqlite");
    DBM->ouvrirFichier(file.fileName());
    DBM->initFichier();
    if (!CM->isEmpty()) {
      CM->supprimerComptes();
    }
    enableActions();
    RefreshAffichageCompte(false);
    RefreshAffichageTransaction();
  }
}

void FenetrePrincipale::on_action_ouvrir_triggered() {
  auto file_name = QFileDialog::getOpenFileName(this);
  if (!file_name.isEmpty() && !file_name.isNull()) {
    DBManager::getInstance()->ouvrirFichier(file_name);
    comptes::CompteManager::getInstance()->Init();
    enableActions();
    RefreshAffichageCompte();
    RefreshAffichageTransaction();
  }
}

/*Création de Comptes*/
void FenetrePrincipale::on_action_nouveau_compte_triggered() {
  FenetreCreationCompte form{this};
  auto result = form.exec();
  if (result == QDialog::Accepted) {
    qDebug("Accepted");
    // Compte sans parent
    if (form.isSansParent()) {
      if (form.hasSoldeInitial()) {
        comptes::CompteManager::getInstance()->creerCompte(
            form.getNom(), form.getType(), form.isVirtuel(),
            form.getMontantSoldeInitial(), form.getCompteSoldeInitial(),
            form.getDateSoldeInitial(), form.getDateSoldeInitial());
      } else {
        comptes::CompteManager::getInstance()->creerCompte(
            form.getNom(), form.getType(), form.isVirtuel());
      }
    }
    // Compte avec parent
    else {
      if (form.hasSoldeInitial()) {
        comptes::CompteManager::getInstance()->creerCompte(
            form.getNom(), *form.getParent(), form.isVirtuel(),
            form.getMontantSoldeInitial(), form.getCompteSoldeInitial(),
            form.getDateSoldeInitial(), form.getDateSoldeInitial());
      } else {
        comptes::CompteManager::getInstance()->creerCompte(
            form.getNom(), *form.getParent(), form.isVirtuel());
      }
    }
  }
  enableActions();
  RefreshAffichageCompte();
}

void FenetrePrincipale::on_action_modifier_transfert_triggered() {
  auto CM = comptes::CompteManager::getInstance();
  auto TM = TransactionManager::getInstance();

  QItemSelectionModel* select = ui->liste_transactions->selectionModel();
  if (!select->hasSelection()) {
    QMessageBox::warning(this, "Erreur modification de transaction",
                         "Aucune transaction n'est sélectionnée.");
  } else {
    int row = select->selectedIndexes().first().row();
    QString ref =
        ui->liste_transactions->model()->index(row, 1).data().toString();
    QDate date = QDate::fromString(
        ui->liste_transactions->model()->index(row, 0).data().toString(),
        "dd/MM/yyyy");
    while (ref.isEmpty() || ref.isNull() || date.isNull()) {
      ref = ui->liste_transactions->model()->index(--row, 1).data().toString();
      date = QDate::fromString(
          ui->liste_transactions->model()->index(row, 0).data().toString(),
          "dd/MM/yyyy");
    }
    Transaction* transaction_modif = TM->getTransaction(ref, date);

    FenetreModifierTransaction form(this, transaction_modif);
    auto result = form.exec();
    if (result == QDialog::Accepted) {
      auto transaction = TM->modifierTransaction(
          transaction_modif, form.getDate(), form.getReference(),
          form.getDescription(), form.getOperations());
      if (transaction == nullptr) {
        QMessageBox::warning(this, "Erreur modification de transaction",
                             "La modification de la transaction a échoué.");
      } else {
        qDebug() << "Transaction modifiée.";
      };
    }
    RefreshAffichageCompte();
    RefreshAffichageTransaction();
  }
}

void FenetrePrincipale::on_action_quitter_triggered() {
  if (QMessageBox::question(
          this, "Quitter l'application", "Voulez-vous quitter l'application ?",
          QMessageBox::Yes, QMessageBox::No) == QMessageBox::Yes) {
    QApplication::quit();
  }
}

void FenetrePrincipale::enableActions(bool isVirtuel) {
  if (isVirtuel) {
    ui->action_rapprocher->setEnabled(false);
  } else {
    ui->action_rapprocher->setEnabled(true);
  }
  ui->action_nouveau_compte->setEnabled(true);
  if (!comptes::CompteManager::getInstance()->isEmpty()) {
    ui->action_ajouter_transfert->setEnabled(true);
  }
  if (!TransactionManager::getInstance()->isEmpty()) {
    ui->action_clore_livre->setEnabled(true);
    ui->menu_Rapports->setEnabled(true);
  }
}

void FenetrePrincipale::on_arbres_comptes_expanded(const QModelIndex& index) {
  ui->arbres_comptes->resizeColumnToContents(0);
  ui->arbres_comptes->resizeColumnToContents(1);
  ui->arbres_comptes->resizeColumnToContents(2);
}

void FenetrePrincipale::on_action_generer_rapport_triggered() {
  auto TM = TransactionManager::getInstance();
  TM->chargerTous();
  FenetreGenerationRapport rapport(this);
  auto result = rapport.exec();
  if (result != QDialog::Accepted) {
    rapport.close();
  }
}
