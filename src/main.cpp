/*!
 * \file main.cpp
 * \author Rémy HUET <utc@remyhuet.fr>
 * \date 2020-04-9
 * \brief Fonction main de l'application de trésorerie
 */

#include <QApplication>
#include <iostream>

#include "headers/abstractcompte.hpp"
#include "headers/comptemanager.hpp"
#include "headers/comptereel.hpp"
#include "headers/comptevirtuel.hpp"
#include "headers/fenetreprincipale.hpp"
#include "headers/storagemanager.hpp"
#include "headers/transactionmanager.hpp"

int main(int argc, char *argv[]) {
  QApplication app{argc, argv};
  FenetrePrincipale f;
  f.show();
  return app.exec();
}
