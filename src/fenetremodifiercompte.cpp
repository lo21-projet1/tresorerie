/*!
 * \file fenetremodifiercompte.cpp
 * \author Pascal Quach <pascal.quach.utc@gmail.com>
 * \date 2020-06-18
 * \brief Source de la fenetre de modification des comptes
 */

#include "headers/fenetremodifiercompte.hpp"
#include "headers/comptemanager.hpp"
#include "ui_fenetrecreationcompte.h"

FenetreModifierCompte::FenetreModifierCompte(QWidget *parent,
                                             comptes::AbstractCompte *compte) {
  ui->supprimerCheckBox->setEnabled(true);
  ui->compteVirtuelCheckBox->setDisabled(true);
  ui->soldeInitialGroupBox->setDisabled(true);
  ui->typeParentGroupBox->setDisabled(true);

  ui->nomLineEdit->setText(compte->getNom());
  ui->nomLineEdit->setProperty("id_compte", compte->getId());
  if (compte->isVirtuel()) {
    ui->compteVirtuelCheckBox->setCheckState(Qt::CheckState::Checked);
  }
  QVector<int> id_roots = {1, 2, 3, 4};
  if (id_roots.contains(compte->getId())) {
    ui->compteSansParentCheckBox->setCheckState(Qt::CheckState::Checked);
  }
  ui->typeCompteComboBox->setCurrentIndex(compte->getType());

  auto CM = comptes::CompteManager::getInstance();
  auto roots = CM->getComptesRoot();
}
