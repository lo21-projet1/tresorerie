/*!
 * \file dbmanager.cpp
 * \author Sébastien Darche
 * \date 2020-06-18
 * \brief Définitions pour la classe DBManager
 */

#include "headers/dbmanager.hpp"

#include <QDebug>
#include <QSqlDatabase>
#include <QtSql>
#include <algorithm>
#include <stdexcept>

#include "headers/comptefactory.hpp"
#include "headers/comptemanager.hpp"
#include "headers/comptevirtuel.hpp"
#include "headers/transactionmanager.hpp"

std::shared_ptr<StorageManager> StorageManager::getStorageManager() {
  return std::static_pointer_cast<StorageManager>(DBManager::getInstance());
}

void DBManager::ouvrirFichier(const QString nom_fichier) {
  if (db.isOpen()) {
    db.close();
  }
  db.setDatabaseName(nom_fichier);
  if (!db.open()) {
    qDebug() << "Impossible d'ouvrir le fichier de db";
  } else {
    dbPath = nom_fichier;
    ouvert = true;
    qDebug() << "Connexion à la db réussie";
  }
}

void DBManager::lireFichier(const QString& file) {
  QFile fichier(file);
  if (!fichier.open(QIODevice::ReadOnly | QFile::Text)) {
    qDebug() << "Impossible d'ouvrir le fichier sql " << file;
  }
  QSqlQuery q_create;
  QString req = fichier.readAll();
  QStringList requetes = req.simplified().split(";");
  for (auto it : requetes) {
    if (it.size() == 0) {
      break;
    }
    qDebug() << "Query : " << it;
    if (!q_create.exec(it)) {
      qDebug() << "Erreur d'execution du fichier " << file;
      qDebug() << q_create.lastQuery();
      qDebug() << q_create.lastError();
      qDebug() << "Erreur sql lecture fichier";
    }
  }
}

void DBManager::initFichier() {
  if (!ouvert) {
    qDebug() << "Aucun fichier de db ouvert, tentative d'init";
    qDebug() << "Aucun fichier de db ouvert";
    return;
  }
  try {
    lireFichier(":/sql/create");
    lireFichier(":/sql/views");
  } catch (std::exception& e) {
    qDebug() << "Fichier déjà initialisé, except : " << e.what();
  }
}

DBManager::DBManager() {
  db = QSqlDatabase::addDatabase("QSQLITE");
  qDebug() << "DB instanciée\n";
}

DBManager::DBManager(const QString nom_fichier) {
  DBManager();
  ouvrirFichier(nom_fichier);
}

bool DBManager::isRacine(int id_compte) const {
  QSqlQuery query;
  query.prepare("SELECT parent FROM Closure WHERE filsV = :id OR filsR = :id");
  query.bindValue(":id", id_compte);
  query.exec();
  if (query.next()) {
    int idParent = query.value(0).toInt();
    if (idParent == ID_ACTIF || idParent == ID_PASSIF ||
        idParent == ID_DEPENSE || idParent == ID_RECETTE) {
      return true;
    } else {
      return false;
    }
  } else {
    qDebug() << "Compte absent de la closure table";
  }
}

QVector<comptes::AbstractCompte*> DBManager::getComptesRacine() const {
  QSqlQuery query;
  QVector<comptes::AbstractCompte*> vec;
  comptes::AbstractCompte* pt = nullptr;
  comptes::TypeCompte t;
  Monnaie solde;
  bool isVirtuel;
  QDate d(QDate::currentDate());

  // Selection des comptes ayant comme père les comptes "dummy" actif, passif,
  // depense ou recette
  query.prepare(
      "SELECT c.parent, f.* FROM Closure c JOIN Comptes f ON c.filsV = f.id "
      "WHERE (c.parent = 1 OR c.parent = 2 OR c.parent = 3 OR c.parent = 4) "
      "AND parent <> filsV UNION SELECT c.parent, f.* FROM Closure c JOIN "
      "Comptes f ON c.filsR = f.id WHERE (c.parent = 1 OR c.parent = 2 OR "
      "c.parent = 3 OR c.parent = 4);");
  query.bindValue(":id_a", ID_ACTIF);
  query.bindValue(":id_p", ID_PASSIF);
  query.bindValue(":id_d", ID_DEPENSE);
  query.bindValue(":id_r", ID_RECETTE);
  if (!query.exec()) {
    qDebug() << "Erreur chargement racine";
  }
  /* Tableau renvoyé :
   * idParent, idFils, isVirtuel, nom, solde
   */

  while (query.next()) {
    int idParent = query.value(0).toInt();
    qDebug() << "DBM::getComptesRacine() : id_parent >" << idParent;
    switch (idParent) {
      case ID_ACTIF:
        t = comptes::ACTIFS;
        break;
      case ID_PASSIF:
        t = comptes::PASSIFS;
        break;
      case ID_DEPENSE:
        t = comptes::DEPENSES;
        break;
      case ID_RECETTE:
        t = comptes::REVENUS;
        break;
    }

    solde.montant = query.value(5).toInt();
    isVirtuel = query.value(2).toInt() == 1;
    if (!isVirtuel) {
      QSqlQuery getter;
      getter.prepare(
          "SELECT dernierRapprochement FROM CompteReel WHERE id = :id");
      getter.bindValue(":id", query.value(1).toInt());
      if (!getter.exec()) {
        qDebug() << "Erreur récupération dernier rapprochement";
      }
      getter.next();
      d = getter.value(0).toDate();
      qDebug() << getter.value(0).toString();
      qDebug() << getter.value(0).toDate();
    } else {
      d = QDate::currentDate();
    }

    pt = comptes::CompteFactory::loadCompte(query.value(1).toInt(),  // idFils
                                            query.value(3).toString(),  // nom
                                            isVirtuel,
                                            t,  // Type compte
                                            solde, d);

    qDebug() << "DBM::getComptesRacine() : Compte >" << pt->getId() << " "
             << pt->getNom() << " " << pt->getType() << " "
             << pt->getSolde().getMontant();
    vec.push_back(pt);
  }
  return vec;
}

QVector<comptes::AbstractCompte*> DBManager::getComptesFils(
    comptes::AbstractCompte* compte) const {
  QSqlQuery query;
  QVector<comptes::AbstractCompte*> vec;
  query.prepare(
      "SELECT c.id FROM Closure JOIN Comptes c ON filsV = c.id OR filsR = c.id "
      "WHERE parent = :idParent");
  query.bindValue(":idParent", compte->getId());
  query.exec();
  while (query.next()) {
    vec.push_back(getCompte(query.value(0).toInt()));
  }
  qDebug() << "Hello there DBManager from getComptesFils reporting live!";
  qDebug() << "id compte : " << query.value(0).toInt();
  qDebug() << "Is the id displayed correctly?";
  return vec;
}

comptes::AbstractCompte* DBManager::getCompte(QString nom_com) const {
  int id = 0;
  Monnaie solde;
  QSqlQuery query;
  auto cm = comptes::CompteManager::getInstance();

  query.prepare("SELECT id, t FROM CompteVirtuel WHERE nom = :nom");
  query.bindValue(":nom", nom_com);

  query.exec();
  if (query.next()) {
    id = query.value(0).toInt();
    QString t = query.value(1).toString();
    if (isRacine(id))
      return comptes::CompteFactory::loadCompte(id, nom_com, true,
                                                stringToTCompte(t));
    else
      return comptes::CompteFactory::loadCompte(
          id, nom_com, true, cm->getCompte(getVirtualParentId(id)));
  } else {
    query.prepare(
        "SELECT id, solde, dernierRapprochement FROM CompteReel WHERE nom = "
        ":nom");
    qDebug() << query.lastError();
    query.bindValue(":nom", nom_com);
    query.exec();
    qDebug() << query.lastError();
    if (query.next()) {
      id = query.value(0).toInt();
      solde.montant = query.value(1).toInt();
      QString t = query.value(1).toString();
      if (isRacine(id)) {
        comptes::TypeCompte tCompte;
        switch (getVirtualParentId(id)) {
          case ID_ACTIF:
            tCompte = comptes::ACTIFS;
            break;
          case ID_PASSIF:
            tCompte = comptes::PASSIFS;
            break;
          case ID_DEPENSE:
            tCompte = comptes::DEPENSES;
            break;
          case ID_RECETTE:
            tCompte = comptes::REVENUS;
            break;
          default:
            throw std::runtime_error("Père racine non conforme");
        }
        try {
          qDebug() << query.value(2).toDate();
        } catch (std::exception& e) {
          qDebug() << "Erreur récupération date";
          qDebug() << e.what();
        }
        auto c = comptes::CompteFactory::loadCompte(
            id, nom_com, false, tCompte, solde, query.value(2).toDate());

        return c;
      } else {
        // auto d = dynamic_cast<comptes::AbstractCompteReel*>(c);
        try {
          qDebug() << query.value(2).toDate();
          // d->rapprocher(query.value(2).toDate());
          // d->dernier_rapprochement = query.value(2).toDate();
        } catch (std::exception& e) {
          qDebug() << "Erreur récupération date";
          qDebug() << e.what();
        }
        auto c = comptes::CompteFactory::loadCompte(
            id, nom_com, false, cm->getCompte(getVirtualParentId(id)), solde,
            query.value(2).toDate());
        return c;
      }
    } else {
      // Le compte n'a pas été trouvé, il n'a pas été créé et on retourne
      // nullptr
      return nullptr;
    }
  }
}

comptes::AbstractCompte* DBManager::getCompte(int id_compte) const {
  QString nom;
  Monnaie solde;
  QSqlQuery query;
  auto cm = comptes::CompteManager::getInstance();

  query.prepare("SELECT nom, t FROM CompteVirtuel WHERE id = :id");
  query.bindValue(":id", id_compte);
  query.exec();
  if (query.next()) {
    nom = query.value(0).toString();
    QString t = query.value(1).toString();
    if (isRacine(id_compte))
      return comptes::CompteFactory::loadCompte(id_compte, nom, true,
                                                stringToTCompte(t));
    else
      return comptes::CompteFactory::loadCompte(
          id_compte, nom, true, cm->getCompte(getVirtualParentId(id_compte)));
  } else {
    query.prepare(
        "SELECT nom, solde, dernierRapprochement FROM CompteReel WHERE id = "
        ":id");
    query.bindValue(":id", id_compte);
    query.exec();

    if (query.next()) {
      nom = query.value(0).toString();
      solde.montant = query.value(1).toLongLong();
      qDebug() << "Solde chargé : " << solde.getMontant() << " , "
               << query.value(1).toLongLong();

      if (isRacine(id_compte)) {
        comptes::TypeCompte tCompte;
        switch (getVirtualParentId(id_compte)) {
          case ID_ACTIF:
            tCompte = comptes::ACTIFS;
            break;
          case ID_PASSIF:
            tCompte = comptes::PASSIFS;
            break;
          case ID_DEPENSE:
            tCompte = comptes::DEPENSES;
            break;
          case ID_RECETTE:
            tCompte = comptes::REVENUS;
            break;
          default:
            throw std::runtime_error("Père racine non conforme");
        }

        try {
          qDebug() << query.value(2).toDate();
        } catch (std::exception& e) {
          qDebug() << "Erreur récupération date";
          qDebug() << e.what();
        }
        auto c = comptes::CompteFactory::loadCompte(
            id_compte, nom, false, tCompte, solde, query.value(2).toDate());
        return c;
      } else {
        try {
          qDebug() << query.value(2).toDate();
          // d->rapprocher(query.value(2).toDate());
          // d->dernier_rapprochement = query.value(2).toDate();
        } catch (std::exception& e) {
          qDebug() << "Erreur récupération date";
          qDebug() << e.what();
        }
        auto c = comptes::CompteFactory::loadCompte(
            id_compte, nom, false,
            (cm->getCompte(getVirtualParentId(id_compte))), solde,
            query.value(2).toDate());
        return c;
      }

    } else {
      // Le compte n'a pas été trouvé, il n'a pas été créé et on retourne
      // nullptr
      return nullptr;
    }
  }
}

QVector<Transaction*> DBManager::getTransactionsByCompte(
    comptes::AbstractCompte* compte) const {
  auto tm = TransactionManager::getInstance();
  Transaction* new_transac;
  QSqlQuery query;
  QVector<Transaction*> res;
  QVector<Operation*> op_associees;
  QVector<Transaction*> transac_presentes =
      tm->getTransactionsByCompte(*compte);
  QDate date;
  QString ref;

  query.prepare(
      "SELECT t.ref, t.tStamp, t.tDesc, t.isRapproche FROM Transac t JOIN "
      "Operation o "
      "ON o.tRef = t.ref AND o.tStamp = t.tStamp WHERE o.cId = :c");
  query.bindValue(":c", compte->getId());
  query.exec();
  while (query.next()) {
    date = query.value(1).toDateTime().date();
    ref = query.value(0).toString();
    // Si la transaction est déjà rattachée au compte, on n'a pas à l'ajouter
    auto isPresent =
        std::find_if(transac_presentes.begin(), transac_presentes.end(),
                     [date, ref](Transaction* t) -> bool {
                       return t->getDate() == date && t->getReference() == ref;
                     });
    if (isPresent ==
        std::end(transac_presentes)) {  // Aucun résultat de la recherche
      op_associees = getOperationsByTransaction(date, ref);
      new_transac = tm->creerTransaction(date, ref, query.value(2).toString(),
                                         op_associees);
      if (query.value(3).toInt() == 1) new_transac->setRapproche();

      if (new_transac != nullptr) res.push_back(new_transac);
    }
  }
  return res;
}

QVector<Transaction*> DBManager::getTransactionsByIdCompte(
    int id_compte) const {
  auto cm = comptes::CompteManager::getInstance();
  return getTransactionsByCompte(cm->getCompte(id_compte));
}

QVector<Operation*> DBManager::getOperationsByTransaction(
    Transaction& transac) const {
  return getOperationsByTransaction(transac.getDate(), transac.getReference());
}

QVector<Operation*> DBManager::getOperationsByTransaction(
    const QDate& date, const QString& ref) const {
  auto tm = TransactionManager::getInstance();
  auto cm = comptes::CompteManager::getInstance();

  QSqlQuery query;
  QVector<Operation*> res;
  query.prepare("SELECT * FROM Operation WHERE tRef = :r AND tStamp = :s");
  query.bindValue(":r", ref);
  query.bindValue(":s", date.toString(Qt::ISODate).append(" 00:00:00"));
  query.exec();
  while (query.next()) {
    int id_compte = query.value(2).toInt();
    Monnaie c;
    Monnaie d;
    c.montant = query.value(4).toInt();
    d.montant = query.value(3).toInt();
    Operation* o = tm->creerOperation(*(cm->getCompte(id_compte)), d, c);
    res.push_back(o);
  }
  return res;
}

int DBManager::getNextIndexCompte() {
  QSqlQuery query;
  query.exec(
      "SELECT max(m) FROM (SELECT max(id) m FROM CompteVirtuel UNION SELECT "
      "max(id) m FROM CompteReel)");
  // Pour une raison inconnue SqLite ne veut pas le max de l'union
  if (query.next()) {
    return query.value(0).toInt() + 1;
  } else {
    // Si il n'y a aucun compte, on renvoie l'id 1 (qui sera alors le premier
    // id)
    return 1;
  }
}

const QString StorageManager::tCompteToString(comptes::TypeCompte t) {
  switch (t) {
    case comptes::ACTIFS:
      return "actif";
    case comptes::PASSIFS:
      return "passif";
    case comptes::DEPENSES:
      return "depense";
    case comptes::REVENUS:
      return "recette";
    default:
      return NULL;
  }
}

comptes::TypeCompte StorageManager::stringToTCompte(const QString s) {
  if (s == "actif")
    return comptes::ACTIFS;
  else if (s == "passif")
    return comptes::PASSIFS;
  else if (s == "depense")
    return comptes::DEPENSES;
  else
    return comptes::REVENUS;
}

void DBManager::ajouterCompte(comptes::AbstractCompte* compte) {
  QSqlQuery query;
  query.prepare(
      "SELECT id FROM CompteVirtuel WHERE id = :id UNION SELECT id FROM "
      "CompteReel WHERE id = :id");
  query.bindValue(":id", compte->getId());
  query.exec();
  if (query.next()) {
    qDebug() << "Tentative d'enregistrement d'un compte qui existe déjà";
    return;
  } else {
    QSqlQuery inserter;
    if (compte->isVirtuel()) {
      inserter.prepare(
          "INSERT INTO CompteVirtuel (id, t, nom) VALUES (:id, :type, "
          ":nom)");
      inserter.bindValue(":id", compte->getId());
      inserter.bindValue(":nom", compte->getNom());
      inserter.bindValue(":type", tCompteToString(compte->getType()));
      inserter.exec();

      auto ptVirtuel = dynamic_cast<comptes::AbstractCompteVirtuel*>(compte);
      if (ptVirtuel == nullptr) {
        qDebug() << "Erreur dynamic cast vers AbstractCompteVirtuel";
        return;
      } else {
        for (auto it : ptVirtuel->getCompteFilsAbstract()) {
          inserter.prepare(
              "INSERT INTO Closure (parent, :tFils) VALUES (:idParent, "
              ":idFils);");
          inserter.bindValue(":tFils", it->isVirtuel() ? "filsV" : "filsR");
          inserter.bindValue(":idParent", compte->getId());
          inserter.bindValue(":idFils", it->getId());
          inserter.exec();
        }
      }
    } else {  // Compte Réel
      inserter.prepare(
          "INSERT INTO CompteReel (id, nom, solde) VALUES (:id,"
          ":nom, :solde)");
      inserter.bindValue(":id", compte->getId());
      inserter.bindValue(":nom", compte->getNom());
      inserter.bindValue(":solde", compte->getSolde().montant);
      inserter.exec();
    }
    // if (isNotInClosure(compte->getId())) setCompteInRacine(compte);
  }
}

void DBManager::updateCompte(comptes::AbstractCompte* compte) {
  QSqlQuery query;
  // Update nom
  query.prepare("UPDATE :table SET nom = :n WHERE id = :id");
  query.bindValue(":table",
                  compte->isVirtuel() ? "CompteVirtuel" : "CompteReel");
  query.bindValue(":n", compte->getNom());
  // Si virtuel : update closure des fils
  // Si réel, update solde
  QSqlQuery inserter;
  if (compte->isVirtuel()) {
    inserter.prepare("DELETE FROM Closure WHERE parent = :id");
    inserter.bindValue(":id", compte->getId());
    inserter.exec();
    // TODO : vérifier validité (cast horizontal avec )
    auto ptVirtuel = dynamic_cast<comptes::AbstractCompteVirtuel*>(compte);
    if (ptVirtuel == nullptr) {
      qDebug() << "Erreur dynamic cast vers compteVirtuel";
      return;
    } else {
      for (auto it : ptVirtuel->getCompteFilsAbstract()) {
        inserter.prepare(
            QString("INSERT INTO Closure (parent, %1) VALUES (:idParent, "
                    ":idFils);")
                .arg(it->isVirtuel() ? "filsV" : "filsR"));
        inserter.bindValue(":idParent", compte->getId());
        inserter.bindValue(":idFils", it->getId());
        if (!inserter.exec()) {
          qDebug() << "Erreur closure update";
          qDebug() << inserter.lastError();
        }
      }
    }
  } else {
    int old_solde = 0;
    inserter.prepare("SELECT solde FROM CompteReel WHERE id = :id");
    inserter.bindValue(":id", compte->getId());
    inserter.exec();
    if (!inserter.next()) {
      qDebug() << "Impossible de récuperer l'ancien solde du compte";
      return;
    } else {
      old_solde = inserter.value(0).toInt();
      inserter.prepare("UPDATE CompteReel SET solde = :s WHERE id = :id");
      inserter.bindValue(":s", compte->getSolde().montant);
      inserter.bindValue(":id", compte->getId());
      if (!inserter.exec()) {
        qDebug() << "Erreur update solde";
      }
      qDebug() << "Update solde compte réel";
      qDebug() << "Ancien solde : " << old_solde;
      qDebug() << "Nouveau solde : " << compte->getSolde().getMontant();
      // Mise à jour du solde des comptes pères
      /*updateVirtualParentSolde(compte->getId(),
                               compte->getSolde().montant - old_solde);*/
      inserter.prepare("SELECT solde FROM CompteReel WHERE id = :id");
      inserter.bindValue(":id", compte->getId());
      inserter.exec();
      inserter.next();
      qDebug() << inserter.value(0).toInt();
      auto c = dynamic_cast<comptes::AbstractCompteReel*>(compte);
      inserter.prepare(
          "UPDATE CompteReel SET dernierRapprochement = :d WHERE id = :id");
      inserter.bindValue(":d", c->getDernierRapprochement()
                                   .toString(Qt::ISODate)
                                   .append(" 00:00:00"));
      inserter.bindValue(":id", compte->getId());
      inserter.exec();
    }
  }
  if (isNotInClosure(compte->getId())) setCompteInRacine(compte);
}

void DBManager::supprimerTransaction(Transaction* transac) {
  QSqlQuery query;
  qDebug() << "SUPPRESSION TRANSAC";
  query.prepare("DELETE FROM Operation WHERE tRef = :ref AND tStamp = :t");
  qDebug() << query.lastError();
  query.bindValue(":ref", transac->getReference());
  query.bindValue(":t",
                  transac->getDate().toString(Qt::ISODate).append(" 00:00:00"));
  if (!query.exec()) {
    qDebug() << query.lastError();
    throw std::runtime_error("Erreur suppression Opération");
  }
  query.prepare("DELETE FROM Transac WHERE ref = :ref AND tStamp = :t");
  qDebug() << query.lastError();
  query.bindValue(":ref", transac->getReference());
  query.bindValue(":t",
                  transac->getDate().toString(Qt::ISODate).append(" 00:00:00"));
  if (!query.exec()) {
    qDebug() << query.lastError();
    throw std::runtime_error("Erreur suppression Transaction");
  }
}

void DBManager::supprimerCompte(comptes::AbstractCompte* compte) {
  QSqlQuery query;
  query.prepare("DELETE FROM :table WHERE id = :id");
  query.bindValue(":id", compte->getId());
  query.bindValue(":table",
                  compte->isVirtuel() ? "CompteVirtuel" : "CompteReel");
  query.exec();
}

void DBManager::ajouterTransaction(Transaction* transac) {
  QSqlQuery query;
  query.prepare("INSERT INTO Transac VALUES (:ref, :date, :desc, :isRapp)");
  qDebug() << query.lastError();
  query.bindValue(":ref", transac->getReference());
  query.bindValue(":date",
                  transac->getDate().toString(Qt::ISODate).append(" 00:00:00"));
  query.bindValue(":desc", transac->getDescription());
  query.bindValue(":isRapp", transac->isRapproche() ? 1 : 0);
  if (!query.exec()) {
    qDebug() << query.lastError();
    throw std::runtime_error("Erreur ajout Transaction");
  }
  for (auto it : transac->getOperations()) {
    query.prepare(
        "INSERT INTO Operation VALUES (:ref, :date, :cId, :deb, :cred)");
    qDebug() << query.lastError();
    query.bindValue(":ref", transac->getReference());
    query.bindValue(
        ":date", transac->getDate().toString(Qt::ISODate).append(" 00:00:00"));
    query.bindValue(":cId", it->getCompte().getId());
    query.bindValue(":deb", it->getDebit().montant);
    query.bindValue(":cred", it->getCredit().montant);
    if (!query.exec()) {
      qDebug() << query.lastError();
      qDebug("Erreur ajouterTransaction : Operation");
      throw std::runtime_error("Erreur ajout Transaction");
    }
  }
}

void DBManager::updateTransaction(Transaction* transac) {
  // Aucun risque de supprimer la transaction pour la remplacer
  // après, aucune référence n'est faite dessus dans la DB
  supprimerTransaction(transac);
  ajouterTransaction(transac);
}

void DBManager::rapprocherTransaction(Transaction* transac) {
  QSqlQuery query;
  query.prepare(
      "UPDATE Transac set IsRapproche = 1 WHERE ref = :r AND tStamp = :d");
  query.bindValue(":r", transac->getReference());
  query.bindValue(":d",
                  transac->getDate().toString(Qt::ISODate).append(" 00:00:00"));
  if (!query.exec()) {
    qDebug() << "Erreur Rapprochement transaction";
    throw std::runtime_error("Erreur rapprochement transaction");
  }
}

int DBManager::getVirtualParentId(int id_fils) const {
  QSqlQuery query;
  query.prepare("SELECT parent FROM Closure WHERE filsV = :id OR filsR = :id");
  query.bindValue(":id", id_fils);
  query.exec();
  if (query.next() &&
      query.value(0).toInt() != id_fils) {  // Si on a effectivement un père
    return query.value(0).toInt();
  } else {
    return 0;  // Pas de père
  }
}

bool DBManager::isNotInClosure(int id_compte) const {
  QSqlQuery query;
  query.prepare("SELECT parent FROM Closure WHERE filsV = :id OR filsR = :id");
  query.bindValue(":id", id_compte);
  query.exec();
  if (query.next())
    return false;
  else
    return true;
}

void DBManager::setCompteInRacine(comptes::AbstractCompte* compte) const {
  QSqlQuery query;
  QString formatter =
      QString("INSERT INTO Closure (parent, %1) VALUES (:idRacine, :id)")
          .arg(compte->isVirtuel() ? "filsV" : "filsR");
  int id_racine = 0;

  query.prepare(formatter);
  query.bindValue(":id", compte->getId());

  switch (compte->getType()) {
    case comptes::ACTIFS:
      id_racine = ID_ACTIF;
      break;
    case comptes::PASSIFS:
      id_racine = ID_PASSIF;
      break;
    case comptes::DEPENSES:
      id_racine = ID_DEPENSE;
      break;
    case comptes::REVENUS:
      id_racine = ID_RECETTE;
      break;
  }
  query.bindValue(":idRacine", id_racine);
  query.exec();
}
