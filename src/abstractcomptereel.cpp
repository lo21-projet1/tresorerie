/*!
 * \file abstractcomptereel.cpp
 * \author Rémy HUET <utc@remyhuet.fr>
 * \date 2020-06-17
 * \brief Définitions pour la classe AbstractCompteReel
 */

#include "headers/abstractcomptereel.hpp"

#include <QVector>
#include <stdexcept>

#include "headers/storagemanager.hpp"
#include "headers/transaction.hpp"
#include "headers/transactionmanager.hpp"

using namespace comptes;

void AbstractCompteReel::rapprocher(const QDate &date) {
  QVector<Transaction *> trans =
      TransactionManager::getInstance()->getTransactionsByCompte(*this);
  if (date <= dernier_rapprochement) {
    throw std::invalid_argument("Compte déjà rapproché pour cette date");
  } else {
    for (auto it : trans) {
      if (it->getDate() >= dernier_rapprochement &&
         it->getDate() <= date) {
        it->setRapproche();
        StorageManager::getStorageManager()->rapprocherTransaction(it);
      }
    }
    dernier_rapprochement = date;
  }
}

Monnaie AbstractCompteReel::getSoldeByDate(const QDate &date) const {
  Monnaie soldeDate;
  QVector<Operation *> operations =
      TransactionManager::getInstance()->getOperationsByCompteByDate(*this,
                                                                     date);
  for (auto it : operations) {
    if (it->getCompte().getType() == comptes::TypeCompte::ACTIFS ||
        it->getCompte().getType() == comptes::TypeCompte::DEPENSES) {
      soldeDate -= it->getCredit();
      soldeDate += it->getDebit();
    } else {
      soldeDate += it->getCredit();
      soldeDate -= it->getDebit();
    }
  }
  return soldeDate;
}
