/*!
 * \file fenetrecreationcompte.cpp
 * \author Rémy HUET <utc@remyhuet.fr>
 * \date 2020-06-09
 * \brief Définitions pour la classe FenetreCreationCompte
 */

#include "headers/fenetrecreationcompte.hpp"

#include <QDebug>
#include <QMessageBox>
#include <stdexcept>

#include "headers/comptemanager.hpp"
#include "headers/monnaie.hpp"
#include "ui_fenetrecreationcompte.h"

FenetreCreationCompte::FenetreCreationCompte(QWidget *parent)
    : QDialog(parent), ui(new Ui::FenetreCreationCompte) {
  ui->setupUi(this);

  ui->dateDateEdit->setDate(
      QDateTime::currentDateTime()
          .date());  // Par défaut, le champ de saisie de date pour le solde
                     // initial est la date du jour.
  ui->typeCompteComboBox->addItems(
      {"Actifs", "Passifs", "Revenus", "Dépenses"});

  auto comptes = comptes::CompteManager::getInstance()->getComptes();
  for (auto compte : comptes) {
    if (compte->isVirtuel()) {
      ui->compteParentComboBox->addItem(compte->getNom());
    }
    if (!compte->isVirtuel() &&
        compte->getType() == comptes::TypeCompte::PASSIFS) {
      ui->compteSourceComboBox->addItem(compte->getNom());
    }
  }
}

FenetreCreationCompte::~FenetreCreationCompte() { delete ui; }

void FenetreCreationCompte::on_compteSansParentCheckBox_stateChanged(
    int state) {
  if (state == Qt::CheckState::Checked) {
    ui->compteParentLabel->setEnabled(false);
    ui->compteParentComboBox->setEnabled(false);
    ui->typeCompteLabel->setEnabled(true);
    ui->typeCompteComboBox->setEnabled(true);
  } else {
    auto compte = comptes::CompteManager::getInstance()->getCompte(
        ui->compteParentComboBox->currentText());
    if (compte->getType() == comptes::DEPENSES ||
        compte->getType() == comptes::REVENUS) {
      ui->soldeInitialGroupBox->setChecked(false);
      ui->soldeInitialGroupBox->setEnabled(false);
    } else {
      ui->soldeInitialGroupBox->setEnabled(true);
    }
    ui->compteParentLabel->setEnabled(true);
    ui->compteParentComboBox->setEnabled(true);
    ui->typeCompteLabel->setEnabled(false);
    ui->typeCompteComboBox->setEnabled(false);
  }
}

void FenetreCreationCompte::on_nomLineEdit_textChanged(const QString &text) {
  ui->labelTransactionLineEdit->setText("S_Init_" + text);
}

void FenetreCreationCompte::on_compteVirtuelCheckBox_stateChanged(int state) {
  if (state == Qt::CheckState::Checked) {
    ui->soldeInitialGroupBox->setChecked(false);
    ui->soldeInitialGroupBox->setEnabled(false);
  } else {
    if (ui->typeCompteComboBox->currentIndex() == 2 ||
        ui->typeCompteComboBox->currentIndex() == 3) {
      ui->soldeInitialGroupBox->setEnabled(false);
    } else {
      ui->soldeInitialGroupBox->setEnabled(true);
    }
  }
}

void FenetreCreationCompte::on_typeCompteComboBox_currentIndexChanged(
    int index) {
  if (!ui->compteVirtuelCheckBox->isChecked()) {
    if (index == 2 || index == 3) {
      ui->soldeInitialGroupBox->setChecked(false);
      ui->soldeInitialGroupBox->setEnabled(false);
    } else {
      ui->soldeInitialGroupBox->setEnabled(true);
    }
  }
}

void FenetreCreationCompte::on_compteParentComboBox_currentIndexChanged(
    int index) {
  if (ui->compteParentComboBox->isEnabled()) {
    auto compte = comptes::CompteManager::getInstance()->getCompte(
        ui->compteParentComboBox->currentText());
    if (compte->getType() == comptes::DEPENSES ||
        compte->getType() == comptes::REVENUS) {
      ui->soldeInitialGroupBox->setChecked(false);
      ui->soldeInitialGroupBox->setEnabled(false);
    } else {
      ui->soldeInitialGroupBox->setEnabled(true);
    }
  }
}

void FenetreCreationCompte::on_buttonBox_accepted() {
  qDebug() << "Vérification des informations";

  // Par défaut, on considère que tout est bon
  bool ok = true;

  if (ui->nomLineEdit->text().isEmpty()) {  // Le compte n'a pas de nom
    ok = false;
    QMessageBox::critical(this, "Erreur formulaire",
                          "Le nom du compte ne doit pas être vide");
  }
  if (ui->compteSansParentCheckBox->isChecked() &&
      ui->typeCompteComboBox->currentText()
          .isNull()) {  // L'option "sans parent" est coché mais aucun type
                        // n'est spécifié
    ok = false;
    QMessageBox::critical(
        this, "Erreur formulaire",
        "Si le compte n'a pas de parent, un type doit être spécifié.");
  }

  if (!ui->compteSansParentCheckBox->isChecked() &&
      ui->compteParentComboBox->currentText()
          .isNull()) {  // L'option "sans parent" est décochée mais aucun parent
                        // n'est spécifié
    ok = false;
    QMessageBox::critical(this, "Erreur formulaire",
                          "Le compte parent doit être spécifié");
  }

  if (ui->soldeInitialGroupBox->isChecked() &&
      ui->labelTransactionLineEdit->text()
          .isEmpty()) {  // L'option "solde initial" est cochée mais aucun
                         // lablel n'est donné à la transaction
    ok = false;
    QMessageBox::critical(
        this, "Erreur formulaire",
        "La transaction associée au solde initial doit comporter un label");
  }

  if (ui->soldeInitialGroupBox->isChecked() &&
      ui->compteSourceComboBox->currentText()
          .isNull()) {  // L'option "solde initial" est cochée mas aucun compte
                        // source n'est séélectionné
    ok = false;
    QMessageBox::critical(
        this, "Erreur formulaire",
        "Le solde initial doit venir d'un compte de passifs source.");
  }

  if (ui->soldeInitialGroupBox->isChecked() &&
      !ui->dateDateEdit->date()
           .isValid()) {  // L'option "solde initial" est cochée mais aucune
                          // date valide n'est associée à la transaction
    ok = false;
    QMessageBox::critical(this, "Erreur formulaire",
                          "Le solde initial doit être associé à une "
                          "transaction avec une date valide");
  }

  if (ui->soldeInitialGroupBox->isChecked() &&
      ui->montantDoubleSpinBox->text().isNull()) {
    ok = false;
    QMessageBox::critical(this, "Erreur formulaire",
                          "Le solde initial doit être associé à un montant.");
  }

  try {
    Monnaie(ui->montantDoubleSpinBox->text());
  } catch (std::invalid_argument &e) {
    ok = false;
    QMessageBox::critical(
        this, "Erreur formulaire",
        "Impossible de régler le solde initial : " + QString(e.what()));
  }

  if (comptes::CompteManager::getInstance()->getCompte(
          ui->nomLineEdit->text()) != nullptr) {
    ok = false;
    QMessageBox::critical(
        this, "Erreur formulaire",
        "Il est impossible d'avoir deux comptes avec le même nom");
  }

  if (ok) this->accept();
}

QString FenetreCreationCompte::getNom() const {
  return ui->nomLineEdit->text();
}

bool FenetreCreationCompte::isVirtuel() const {
  return ui->compteVirtuelCheckBox->isChecked();
}

bool FenetreCreationCompte::isSansParent() const {
  return ui->compteSansParentCheckBox->isChecked();
}

comptes::TypeCompte FenetreCreationCompte::getType() const {
  return static_cast<comptes::TypeCompte>(
      ui->typeCompteComboBox->currentIndex());
}

comptes::AbstractCompte *FenetreCreationCompte::getParent() const {
  return comptes::CompteManager::getInstance()->getCompte(
      ui->compteParentComboBox->currentText());
}

bool FenetreCreationCompte::hasSoldeInitial() const {
  return ui->soldeInitialGroupBox->isChecked();
}

QString FenetreCreationCompte::getLabelSoldeInitial() const {
  return ui->labelTransactionLineEdit->text();
}

comptes::Compte<comptes::TypeCompte::PASSIFS>
    *FenetreCreationCompte::getCompteSoldeInitial() const {
  return dynamic_cast<comptes::Compte<comptes::TypeCompte::PASSIFS> *>(
      comptes::CompteManager::getInstance()->getCompte(
          ui->compteSourceComboBox->currentText()));
}

QDate FenetreCreationCompte::getDateSoldeInitial() const {
  return ui->dateDateEdit->date();
}

Monnaie FenetreCreationCompte::getMontantSoldeInitial() const {
  return Monnaie(ui->montantDoubleSpinBox->text());
}
