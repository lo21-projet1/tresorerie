/*!
 * \file fenetremodifiertransaction.cpp
 * \author Pascal Quach
 * \author Arthur Wacquez
 * \author Rémy HUET <utc@remyhuet.fr>
 * \date 2020-06-17
 * \brief Implémentation de la fenêtre FenetreModifierTransaction
 */

#include "headers/fenetremodifiertransaction.hpp"

#include <QDoubleSpinBox>
#include <QMessageBox>
#include <QTableWidgetItem>

#include "headers/abstractcompte.hpp"
#include "headers/transactionmanager.hpp"
#include "ui_fenetreajoutertransaction.h"

FenetreModifierTransaction::FenetreModifierTransaction(QWidget* parent,
                                                       Transaction* transaction)
    : FenetreAjouterTransaction(parent) {
  getLineEditReference()->setDisabled(true);
  getQDateTransaction()->setDisabled(true);

  getLineEditReference()->setText(transaction->getReference());
  getQDateTransaction()->setDate(transaction->getDate());
  getLineEditDescription()->setText(transaction->getDescription());

  // TODO : remonter/transformer en une méthode sur FenetreModifierTransaction
  // TODO : Abstraire l'id de la colonne débit/crédit
  auto operations = transaction->getOperations();

  for (auto op : operations) {
    auto i = getListeOperations()->rowCount();
    getListeOperations()->insertRow(i);
    auto nom = op->getCompte().getNom();
    qDebug() << "FMT :: nom compte " << nom;
    auto id = op->getCompte().getId();
    qDebug() << "FMT :: nom compte " << id;
    getListeOperations()->setItem(i, 0, new QTableWidgetItem(nom));
    getListeOperations()->item(i, 0)->setData(Qt::UserRole, id);

    getListeOperations()->setItem(
        i, 2, new QTableWidgetItem(op->getCredit().getMontant()));
    getListeOperations()->setItem(
        i, 1, new QTableWidgetItem(op->getDebit().getMontant()));
  }
}
