/*!
 * \file fenetreajoutertransaction.cpp
 * \author Pascal Quach <pascal.quach.utc@gmail.com>
 * \author Arthur Wacquez <arthur.wacquez@etu.utc.fr>
 * \date 2020-06-17
 * \brief Implémentation de la fenête FenetreAjouterTransacion
 */
#include "headers/fenetreajoutertransaction.hpp"

#include <QDebug>
#include <QDialog>
#include <QMessageBox>
#include <stdexcept>

#include "headers/comptemanager.hpp"
#include "headers/dbmanager.hpp"
#include "headers/qcomboboxdelegate.hpp"
#include "headers/qdoublespinboxdelegate.hpp"
#include "headers/transactionmanager.hpp"
#include "headers/abstractcomptereel.hpp"
#include "ui_fenetreajoutertransaction.h"

FenetreAjouterTransaction::FenetreAjouterTransaction(QWidget *parent)
    : QDialog(parent), ui(new Ui::FenetreAjouterTransaction) {
  ui->setupUi(this);
  auto combo_box = new QComboBoxDelegate(getListeOperations());
  auto spin_debit = new QDoubleSpinBoxDelegate(getListeOperations());
  auto spin_credit = new QDoubleSpinBoxDelegate(getListeOperations());
  ui->tableWidget_liste_operations->horizontalHeader()->setStretchLastSection(true);
  getListeOperations()->setItemDelegateForColumn(0, combo_box);
  getListeOperations()->setItemDelegateForColumn(1, spin_debit);
  getListeOperations()->setItemDelegateForColumn(2, spin_credit);
  ui->dateEdit_date->setDate(QDate::currentDate());
}

FenetreAjouterTransaction::~FenetreAjouterTransaction() { delete ui; }

QLineEdit *FenetreAjouterTransaction::getLineEditReference() {
  return ui->lineEdit_reference;
}
QLineEdit *FenetreAjouterTransaction::getLineEditReference() const {
  return ui->lineEdit_reference;
}
QDateEdit *FenetreAjouterTransaction::getQDateTransaction() {
  return ui->dateEdit_date;
}
QDateEdit *FenetreAjouterTransaction::getQDateTransaction() const {
  return ui->dateEdit_date;
}
QLineEdit *FenetreAjouterTransaction::getLineEditDescription() {
  return ui->lineEdit_description;
}
QLineEdit *FenetreAjouterTransaction::getLineEditDescription() const {
  return ui->lineEdit_description;
}
QTableWidget *FenetreAjouterTransaction::getListeOperations() {
  return ui->tableWidget_liste_operations;
}
QTableWidget *FenetreAjouterTransaction::getListeOperations() const {
  return ui->tableWidget_liste_operations;
}
QLabel *FenetreAjouterTransaction::getLabelIdCompte() {
  return ui->label_id_compte;
}
QLabel *FenetreAjouterTransaction::getLabelIdCompte() const {
  return ui->label_id_compte;
}
QLabel *FenetreAjouterTransaction::getLabelTypeCompte() {
  return ui->label_type_compte;
}
QLabel *FenetreAjouterTransaction::getLabelTypeCompte() const {
  return ui->label_type_compte;
}
QLabel *FenetreAjouterTransaction::getLabelVirtuelCompte() {
  return ui->label_virtuel_compte;
}
QLabel *FenetreAjouterTransaction::getLabelVirtuelCompte() const {
  return ui->label_virtuel_compte;
}
QLabel *FenetreAjouterTransaction::getLabelSoldeCompte() {
  return ui->label_solde_compte;
}
QLabel *FenetreAjouterTransaction::getLabelSoldeCompte() const {
  return ui->label_solde_compte;
}

comptes::AbstractCompte const *FenetreAjouterTransaction::getCompte(int i) {
  return comptes::CompteManager::getInstance()->getCompte(
      getIdCompteOperation(i));
}

const QString FenetreAjouterTransaction::getReference() {
  return getLineEditReference()->text();
}

const QDate FenetreAjouterTransaction::getDate() {
  return getQDateTransaction()->date();
}

const QString FenetreAjouterTransaction::getDescription() {
  return getLineEditDescription()->text();
}

int FenetreAjouterTransaction::getIdCompteOperation(int i) {
  return isOperationCellEmpty(i, 0)
             ? 0
             : getListeOperations()
                   ->model()
                   ->data(getListeOperations()->model()->index(i, 0),
                          Qt::UserRole)
                   .toInt();
}

QString FenetreAjouterTransaction::getNomCompteOperation(int i) {
  return isOperationCellEmpty(i, 0)
             ? QString()
             : QString(getListeOperations()->item(i, 0)->text());
}

Monnaie FenetreAjouterTransaction::getDebitOperation(int i) {
  return isOperationCellEmpty(i, 1)
             ? QString("0,00")
             : QString(getListeOperations()->item(i, 1)->text());
}

Monnaie FenetreAjouterTransaction::getCreditOperation(int i) {
  return isOperationCellEmpty(i, 2)
             ? QString("0,00")
             : QString(getListeOperations()->item(i, 2)->text());
}

QVector<Operation *> FenetreAjouterTransaction::getOperations() {
  QVector<Operation *> *operations = new QVector<Operation *>;
  // qDebug() << "FenêtreAjouterTransaction here, just trying to get all "
  //          "operations out to FenetrePrincipale. Please be nice.";
  for (auto i = 0; i != getListeOperations()->rowCount(); i++) {
    qDebug() << getListeOperations()
                    ->model()
                    ->data(getListeOperations()->model()->index(i, 0),
                           Qt::UserRole)
                    .toInt();
  }
  for (auto i = 0; i != getListeOperations()->rowCount(); i++) {
    auto compte = comptes::CompteManager::getInstance()->getCompte(
        getIdCompteOperation(i));

    qDebug() << "FAT::getOperations : id compte " << getIdCompteOperation(i);
    qDebug() << "FAT::getOperations : instance compte " << compte->getNom()
             << " " << compte->getId() << " " << compte->getType() << " "
             << compte->getSolde().getMontant();
    auto *operation = TransactionManager::getInstance()->creerOperation(
        *comptes::CompteManager::getInstance()->getCompte(
            getIdCompteOperation(i)),
        getDebitOperation(i), getCreditOperation(i));
    if (operation == nullptr) {
      QMessageBox::warning(this, "Erreur création d'opération",
                           "La création d'opération a échoué (Opération n°" +
                               QString::number(i) + ")");
      // qDebug() << "Sucks to be you. It failed.";
    }
    // qDebug() << "Well, everything looks fine.";
    operations->push_back(operation);
  }
  return *operations;
}

bool FenetreAjouterTransaction::isOperationCellEmpty(int i, int j) {
  bool empty = (!getListeOperations()->item(i, j) ||
                getListeOperations()->item(i, j)->text().isEmpty() ||
                getListeOperations()->item(i, j)->text() == "0,00");
  if (empty) {
    return (empty);
  } else {
    try {
      return (Monnaie(getListeOperations()->item(i, j)->text()) ==
              Monnaie("0,00"));
    } catch (std::invalid_argument) {
      return (false);
    }
  }
}

void FenetreAjouterTransaction::on_DialogButtons_accepted() {
  qDebug() << "Vérification des informations";

  // Verification de la référence
  if (getLineEditReference()->text().isEmpty()) {
    QMessageBox::warning(this, "Erreur formulaire",
                         "La référence ne doit pas être vide");
    return;
  }

  // Verification de la description
  if (getLineEditDescription()->text().isEmpty()) {
    QMessageBox::warning(this, "Erreur formulaire",
                         "La description ne doit pas être vide");
    return;
  }

  // Vérification
  if (getListeOperations()->rowCount() < 2) {
    QMessageBox::warning(this, "Erreur formulaire",
                         "Il faut au moins 2 opérations");
    return;
  }

  // Vérification des opérations
  for (auto i = 0; i != getListeOperations()->rowCount(); i++) {
    if (!getListeOperations()->item(i, 0)) {
      QMessageBox::warning(
          this, "Erreur formulaire",
          "La compte associé ne doit pas être vide (Opération n°" +
              QString::number(i + 1) + ")");
      return;
    }
    if (isOperationCellEmpty(i, 1) && isOperationCellEmpty(i, 2)) {
      QMessageBox::warning(this, "Erreur formulaire",
                           "Débit et crédit ne peuvent pas être vides tous les "
                           "deux(Opération n°" +
                               QString::number(i + 1) + ")");
      return;
    }

    else if (!isOperationCellEmpty(i, 1) && !isOperationCellEmpty(i, 2)) {
      QMessageBox::warning(
          this, "Erreur formulaire",
          "Débit et crédit ne peuvent pas être remplis tous les "
          "deux(Opération n°" +
              QString::number(i + 1) + ")");
      return;
    } else {
      if (!isOperationCellEmpty(i, 1)) {
        try {
          Monnaie monnaie(getListeOperations()->item(i, 1)->text());
          qDebug() << "Débit correct";
        } catch (std::invalid_argument &) {
          QMessageBox::warning(this, "Erreur formulaire",
                               "Débit n'est pas de type monnaie (forme "
                               "XXXX.YY) (Opération n°" +
                                   QString::number(i + 1) + ")");
          return;
        }
      }

      if (!isOperationCellEmpty(i, 2)) {
        try {
          Monnaie monnaie(getListeOperations()->item(i, 2)->text());
          qDebug() << "Crédit correct";
        } catch (std::invalid_argument &) {
          QMessageBox::warning(
              this, "Erreur formulaire",
              "Crédit n'est pas de type monnaie (forme XXXX.YY) (Opération n°" +
                  QString::number(i + 1) + ")");
          return;
        }
      }
    }

    auto compte=comptes::CompteManager::getInstance()->getCompte(getIdCompteOperation(i));
    QDate dernierrapprochement=dynamic_cast<comptes::AbstractCompteReel*>(compte)->getDernierRapprochement();
    if (dernierrapprochement>ui->dateEdit_date->date()){
        QMessageBox::warning(
            this, "Erreur formulaire",
            compte->getNom()+": impossible d'ajouter une transaction datant d'avant le dernier rapprochement. Dernier rapprochement: " +
                dernierrapprochement.toString("dd/MM/yyyy"));
        return;
    }
  }

  // Vérification de la validité de la transaction
  auto TM = TransactionManager::getInstance();
  if (TM->creerTransaction(getDate(), getReference(), getDescription(),
                           getOperations()) == nullptr) {
    QMessageBox::warning(this, "Erreur création de transaction",
                         "La création de la transaction a échoué.");

    return;
  }
  this->accept();
}

void FenetreAjouterTransaction::
    on_tableWidget_liste_operations_currentItemChanged(
        QTableWidgetItem *current, QTableWidgetItem *previous) {
  if (current != nullptr && current->column() == 0) {
    qDebug() << "CurrentItemChanged() TableWidget liste_opération";
    // Todo change to id
    auto compte = comptes::CompteManager::getInstance()->getCompte(
        current->data(Qt::UserRole).toInt());
    getLabelIdCompte()->setText(QString::number(compte->getId()));
    getLabelTypeCompte()->setText(
        DBManager::getInstance()->tCompteToString(compte->getType()));
    getLabelVirtuelCompte()->setText(compte->isVirtuel() ? "Virtuel" : "Réel");
    getLabelSoldeCompte()->setText(compte->getSolde().getMontant());
  }
}

void FenetreAjouterTransaction::on_pushButton_ajouter_operation_clicked() {
  getListeOperations()->insertRow(getListeOperations()->rowCount());
}

void FenetreAjouterTransaction::on_pushButton_retirer_operation_clicked() {
  getListeOperations()->removeRow(getListeOperations()->rowCount() - 1);
}
