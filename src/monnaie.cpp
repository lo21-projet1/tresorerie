/*!
 * \file monnaie.cpp
 * \author Rémy HUET <utc@remyhuet.fr>
 * \date 2020-06-09
 * \brief Déclarations pour la classe Monnaie
 */

#include "headers/monnaie.hpp"

#include <QDebug>
#include <QString>
#include <QStringList>
#include <array>
#include <stdexcept>

static constexpr int RESOLUTION = 100;
static constexpr std::array<char, 2> SEPARATEURS = {',', '.'};

Monnaie::Monnaie(const QString &m) { setMontant(m); }

QString Monnaie::getMontant() const noexcept {
  auto entiere = QString::number(montant / RESOLUTION);
  auto decimaleN = montant % RESOLUTION;
  if (decimaleN < 0) decimaleN = -decimaleN;
  auto decimale = QString::number(decimaleN);
  if (decimale.length() == 1) decimale = "0" + decimale;
  return entiere + SEPARATEURS[0] + decimale;
}

void Monnaie::setMontant(const QString &m) {
  QStringList list;

  bool ok = false;
  for (auto sep : SEPARATEURS) {
    list = m.split(sep);
    if (list.length() == 2 && list[1].length() == 2) {
      ok = true;
      break;
    }
  }
  if (!ok) {
    throw std::invalid_argument(
        "Impossible de construire un objet Monnaie depuis " + m.toStdString());
  }

  auto partieEntiere = list[0].toInt(&ok);
  if (!ok)
    throw std::invalid_argument(
        "Impossible de récupérer la partie entière de " + m.toStdString());
  auto partieDecimale =
      (partieEntiere < 0) ? -list[1].toInt(&ok) : list[1].toInt(&ok);
  if (!ok)
    throw std::invalid_argument(
        "Impossible de récupérer la partie décimale de " + m.toStdString());

  montant = partieEntiere * RESOLUTION + partieDecimale;
}

Monnaie::operator QString() const noexcept { return getMontant(); }

QDataStream &operator<<(QDataStream &os, const Monnaie &monnaie) {
  os << static_cast<QString>(monnaie);
  return os;
}
