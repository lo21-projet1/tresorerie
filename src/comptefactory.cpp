/*!
 * \file comptefactory.cpp
 * \author Rémy HUET <utc@remyhuet.fr>
 * \date 2020-06-02
 * \brief Définitions des fonctions de la classe CompteFactory
 */

#include "headers/comptefactory.hpp"

#include <QDebug>
#include <stdexcept>

#include "headers/comptereel.hpp"
#include "headers/comptevirtuel.hpp"
#include "headers/storagemanager.hpp"
#include "headers/transactionmanager.hpp"

comptes::AbstractCompte *comptes::CompteFactory::creerCompte(
    unsigned int id, const QString &nom, comptes::AbstractCompte &parent,
    bool virtuel, const Monnaie &solde, Compte<PASSIFS> *compteSInitial,
    const QDate &dateSInitial, const QDate &dateDernierRapprochement) {
  if (!parent.isVirtuel()) {
    qDebug() << "Le compte parent doit être un compte virtuel";
  }

  auto compte = comptes::CompteFactory::creerCompte(
      id, nom, parent.getType(), virtuel, solde, compteSInitial, dateSInitial,
      dateDernierRapprochement);

  switch (parent.getType()) {
    case ACTIFS:
      dynamic_cast<CompteVirtuel<ACTIFS> &>(parent)
          .addSousCompte(dynamic_cast<Compte<ACTIFS> *>(compte));
      break;
    case PASSIFS:
      dynamic_cast<CompteVirtuel<PASSIFS> &>(parent)
          .addSousCompte(dynamic_cast<Compte<PASSIFS> *>(compte));
      break;
    case REVENUS:
      dynamic_cast<CompteVirtuel<REVENUS> &>(parent)
          .addSousCompte(dynamic_cast<Compte<REVENUS> *>(compte));
      break;
    case DEPENSES:
      dynamic_cast<CompteVirtuel<DEPENSES> &>(parent)
          .addSousCompte(dynamic_cast<Compte<DEPENSES> *>(compte));
      break;
  }

  return compte;
}

comptes::AbstractCompte *comptes::CompteFactory::loadCompte(
    unsigned int id, const QString &nom, bool isVirtuel,
    comptes::TypeCompte type, const Monnaie &solde, const QDate &date) {
  auto compte = creerCompte(id, nom, type, isVirtuel);
  if (!isVirtuel) {
    dynamic_cast<AbstractCompteReel *>(compte)->solde = solde;
    dynamic_cast<AbstractCompteReel *>(compte)->dernier_rapprochement = date;
  }
  return compte;
}

comptes::AbstractCompte *comptes::CompteFactory::loadCompte(
    unsigned int id, const QString &nom, bool isVirtuel,
    comptes::AbstractCompte *parent, const Monnaie &solde, const QDate &date) {
  auto compte = creerCompte(id, nom, *parent, isVirtuel);
  if (!isVirtuel) {
    dynamic_cast<AbstractCompteReel *>(compte)->solde = solde;
    dynamic_cast<AbstractCompteReel *>(compte)->dernier_rapprochement = date;
  }
  qDebug() << "CompteFactory::loadCompte : création du compte "
           << compte->getId() << " " << compte->getNom() << " "
           << compte->getType() << " " << compte->getSolde().getMontant() << " "
           << compte;
  return compte;
}

comptes::AbstractCompte *comptes::CompteFactory::creerCompte(
    unsigned int id, const QString &nom, comptes::TypeCompte type, bool virtuel,
    const Monnaie &solde, Compte<PASSIFS> *compteSInitial,
    const QDate &dateSInitial, const QDate &dateDernierRapprochement) {
  AbstractCompte *compte;
  if (virtuel) {
    switch (type) {
      case ACTIFS:
        compte = new CompteVirtuel<ACTIFS>(nom, id);
        break;
      case PASSIFS:
        compte = new CompteVirtuel<PASSIFS>(nom, id);
        break;
      case REVENUS:
        compte = new CompteVirtuel<REVENUS>(nom, id);
        break;
      case DEPENSES:
        compte = new CompteVirtuel<DEPENSES>(nom, id);
        break;
      default:
        qDebug()
            << "Le type de compte passé au CompteFactory ne correspond à aucun "
               "type connu";
        break;
    }
  } else {
    switch (type) {
      case ACTIFS:
        compte = new CompteReel<ACTIFS>(nom, id, dateDernierRapprochement);
        break;
      case PASSIFS:
        compte = new CompteReel<PASSIFS>(nom, id, dateDernierRapprochement);
        break;
      case REVENUS:
        compte = new CompteReel<REVENUS>(nom, id, dateDernierRapprochement);
        break;
      case DEPENSES:
        compte = new CompteReel<DEPENSES>(nom, id, dateDernierRapprochement);
        break;
      default:
        qDebug()
            << "Le type de compte passé au CompteFactory ne correspond à aucun "
               "type connu";
        break;
    }

    setSoldeInitial(compte, solde, compteSInitial, dateSInitial);
  }
  return compte;
}

comptes::AbstractCompte *comptes::CompteFactory::creerCompte(
    const QString &nom, comptes::AbstractCompte &parent, bool virtuel,
    const Monnaie &solde, Compte<PASSIFS> *compteSInitial,
    const QDate &dateSInitial, const QDate &dateDernierRapprochement) {
  auto id = StorageManager::getStorageManager()->getNextIndexCompte();
  auto compte = creerCompte(id, nom, parent, virtuel, solde, compteSInitial,
                            dateSInitial, dateDernierRapprochement);

  return compte;
}

comptes::AbstractCompte *comptes::CompteFactory::creerCompte(
    const QString &nom, comptes::TypeCompte type, bool virtuel,
    const Monnaie &solde, Compte<PASSIFS> *compteSInitial,
    const QDate &dateSInitial, const QDate &dateDernierRapprochement) {
  auto id = StorageManager::getStorageManager()->getNextIndexCompte();
  auto compte = creerCompte(id, nom, type, virtuel, solde, compteSInitial,
                            dateSInitial, dateDernierRapprochement);

  return compte;
}

void comptes::CompteFactory::setSoldeInitial(
    AbstractCompte *compte, const Monnaie &solde,
    Compte<comptes::PASSIFS> *compteSInitial, const QDate &dateSInitial) {
  if (solde) {
    if (compteSInitial == nullptr || !dateSInitial.isValid()) {
      qDebug()
          << "Si un solde initial est non nul, le pointeur vers le compte d'où "
             "provient le solde et la date de la transaction doivent être "
             "spécifiés";
    } else {
        if (compte->getType()==ACTIFS){
          QVector<Operation *> operations = {
              TransactionManager::getInstance()->creerOperation(*compte, solde,
                                                                Monnaie("0,00")),
              TransactionManager::getInstance()->creerOperation(
                  *compteSInitial, Monnaie("0,00"), solde)};
          TransactionManager::getInstance()->ajouterTransaction(
              dateSInitial, "S_INIT_" + compte->getNom(),
              "Solde initial " + compte->getNom(), operations);
        }
        else{
            QVector<Operation *> operations = {
                TransactionManager::getInstance()->creerOperation(*compte,Monnaie("0,00"),
                                                                                    solde),
                TransactionManager::getInstance()->creerOperation(
                    *compteSInitial,solde,Monnaie("0,00"))};
            TransactionManager::getInstance()->ajouterTransaction(
                dateSInitial, "S_INIT_" + compte->getNom(),
                "Solde initial " + compte->getNom(), operations);
        }
    }
  }
}
