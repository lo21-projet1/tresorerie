/*!
 * \file fenetregenerationrapport.cpp
 * \author Bastien Bouré
 * \date 2020-06-18
 * \brief Implémentation de la fenêtre FenetreGenerationRapport
 */

#include "headers/fenetregenerationrapport.hpp"

#include <QMessageBox>

#include "headers/comptemanager.hpp"
#include "ui_fenetregenerationrapport.h"

FenetreGenerationRapport::FenetreGenerationRapport(QWidget *parent)
    : QDialog(parent), ui(new Ui::FenetreGenerationRapport) {
  ui->setupUi(this);

  auto CM = comptes::CompteManager::getInstance();

  this->setWindowTitle("Génératon de Rapports");
  this->resize(400, 100);

  ui->D_Bilan->setDate(QDate::currentDate());
  //ui->D_Bilan->setMaximumDate(QDate::currentDate());

  ui->D_resultat->setDate(QDate::currentDate());
  //ui->D_resultat->setMaximumDate(QDate::currentDate());

  ui->LE_FilenameBilan->setText("Bilan" + CM->getNomAssociation() + " du " +
                                QDate::currentDate().toString("dd-MM-yyyy"));
  ui->LE_FilenameResultat->setText("Resultat" + CM->getNomAssociation() +
                                   " du " +
                                   QDate::currentDate().toString("dd-MM-yyyy"));
}

FenetreGenerationRapport::~FenetreGenerationRapport() { delete ui; }

void FenetreGenerationRapport::on_valider_accepted() {
  auto CM = comptes::CompteManager::getInstance();
  if (!ui->G_Resultat->isChecked() && !ui->G_Bilan->isChecked()) {
    QMessageBox::critical(
        this, "Pas de rapport séléctionné",
        "Vous devez au moins séléctionné une génération de rapport");
  } else {
    if (ui->G_Bilan->isChecked()) {
      if (ui->LE_FilenameBilan->text() != "") {
        CM->genererBilan(ui->D_Bilan->date(), ui->LE_FilenameBilan->text());
      } else {
        QMessageBox::critical(
            this, "Pas de nom de fichier",
            "Vous devez entrer le nom du fichier pour la génération du Bilan");
        return;
      }
    }
    if (ui->G_Resultat->isChecked()) {
      if (ui->LE_FilenameResultat->text() != "") {
        CM->genererResultat(ui->D_resultat->date(),
                            ui->LE_FilenameResultat->text());
      } else {
        QMessageBox::critical(this, "Pas de nom de fichier",
                              "Vous devez entrer le nom du fichier pour la "
                              "génération du Resultat");
        return;
      }
    }
  }
  QMessageBox::information(
      this, "Rapports générés avec succès",
      "Vous pouvez retrouver les rapports dans le dossier rapports-generes");
  this->close();
}

void FenetreGenerationRapport::on_G_Bilan_toggled(bool arg1) {
  if (arg1) {
    ui->D_Bilan->setEnabled(true);
  } else {
    ui->D_Bilan->setEnabled(false);
  }
}

void FenetreGenerationRapport::on_G_Resultat_toggled(bool arg1) {
  if (arg1) {
    ui->D_resultat->setEnabled(true);
  } else {
    ui->D_resultat->setEnabled(false);
  }
}

void FenetreGenerationRapport::on_D_Bilan_dateChanged(const QDate &date) {
  auto CM = comptes::CompteManager::getInstance();
  ui->LE_FilenameBilan->setText("Bilan " + CM->getNomAssociation() + " du " +
                                date.toString("dd-MM-yyyy"));
}

void FenetreGenerationRapport::on_D_resultat_dateChanged(const QDate &date) {
  auto CM = comptes::CompteManager::getInstance();

  ui->LE_FilenameResultat->setText("Résultat " + CM->getNomAssociation() +
                                   " du " + date.toString("dd-MM-yyyy"));
}
