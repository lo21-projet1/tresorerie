/*!
 * \file transaction.cpp
 * \author Arthur Wacquez <arthur.wacquez@etu.utc.fr>
 * \author Pascal Quach <pascal.quach.utc@gmail.com>
 * \date 2020-06-09
 * \brief Implémentation des méthodes de la classe Transaction
 */

#ifndef LO21_TRANSACTION_CPP
#define LO21_TRANSACTION_CPP

#include "headers/transaction.hpp"

#include "headers/abstractcompte.hpp"
#include "headers/abstractcomptereel.hpp"

bool Transaction::checkValidite() {
//    qDebug() << "Hello there ! It's TransactionManager from "
//                "Transaction::checkValidite() ! Let's check the validity of this "
//                "transaction, shall we ?";
    Monnaie somme = Monnaie("-1,00");
    if (!operations.empty()) {
        somme.setMontant("0,00");
//        qDebug() << "Seems like there are some operations in this thing. Good.";
    }
    for (auto op : getOperations()) {
        somme += op->getCredit() - op->getDebit();
    }
//    qDebug() << "Aye, well, might want to check the sum. Is it equal to 0.00 ?";
//    qDebug() << QString(somme);
    return (somme == Monnaie("0,00"));
};
#endif  // LO21_TRANSACTION_CPP
