/*!
 * \file comptereel.cpp
 * \author Rémy HUET <utc@remyhuet.fr>
 * \author Noé Delatre
 * \date 2020-06-17
 * \brief Ce fichier contient les spécialisations des méthodes crediter et
 * debiter en fonction des types de compte.
 */

#include "headers/comptereel.hpp"

using namespace comptes;

template <>
void CompteReel<TypeCompte::ACTIFS>::crediter(const Monnaie& montant) {
  solde -= montant;
}
template <>
void CompteReel<TypeCompte::ACTIFS>::debiter(const Monnaie& montant) {
  solde += montant;
}

template <>
void CompteReel<TypeCompte::PASSIFS>::crediter(const Monnaie& montant) {
  solde += montant;
}
template <>
void CompteReel<TypeCompte::PASSIFS>::debiter(const Monnaie& montant) {
  solde -= montant;
}

template <>
void CompteReel<TypeCompte::REVENUS>::crediter(const Monnaie& montant) {
  solde += montant;
}
template <>
void CompteReel<TypeCompte::REVENUS>::debiter(const Monnaie& montant) {
  solde -= montant;
}

template <>
void CompteReel<TypeCompte::DEPENSES>::crediter(const Monnaie& montant) {
  solde -= montant;
}
template <>
void CompteReel<TypeCompte::DEPENSES>::debiter(const Monnaie& montant) {
  solde += montant;
}
