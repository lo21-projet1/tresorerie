/*!
 * \file transactionmanager.cpp
 * \author Pascal Quach <pascal.quach.utc@gmail.com>
 * \date 2020-06-10
 * \brief Implémentation du TransactionManager
 */

#include "headers/transactionmanager.hpp"

#include <QDebug>
#include <QMessageBox>
#include <algorithm>
#include <memory>

#include "headers/abstractcomptereel.hpp"
#include "headers/abstractcomptevirtuel.hpp"
#include "headers/compte.hpp"
#include "headers/comptemanager.hpp"
#include "headers/storagemanager.hpp"

Operation* TransactionManager::creerOperation(comptes::AbstractCompte& compte,
                                              Monnaie debit, Monnaie credit) {
  Operation* op = new Operation(debit, credit, compte);
  if (op->checkValidite()) {
    return op;
  }
  // qDebug() << "TM::creerOperation La création d'une opération a échoué.";
  return nullptr;
}

void TransactionManager::chargerCompteReel(comptes::AbstractCompte* compte) {
  //  qDebug() << "TransactionManager::chargerCompteReel : Hello there! "
  //           << compte->getNom();
  if (!compte->isVirtuel()) {
    auto SM = StorageManager::getStorageManager();
    chargerTransactions(SM->getTransactionsByCompte(compte));
    return;
  }
  //  qDebug() << "TransactionManager::chargerCompteReel : le compte passé en "
  //              "paramètre n'est pas un compte réel.";
}

void TransactionManager::chargerCompteVirtuel(comptes::AbstractCompte* compte) {
  auto CM = comptes::CompteManager::getInstance();
  //  qDebug() << "TransactionManager::chargerCompteVirtuel : Hello there! "
  //           << compte->getNom();
  auto comptes_fils = dynamic_cast<comptes::AbstractCompteVirtuel*>(compte)
                          ->getCompteFilsAbstract();
  for (auto it : comptes_fils) {
    if (it->isVirtuel()) {
      chargerCompteVirtuel(it);
    } else {
      chargerCompteReel(it);
    }
  }
}

void TransactionManager::chargerCompte(comptes::AbstractCompte* compte,
                                       bool vider) {
  qDebug() << "TM::chargerCompte : transactions empty? " << isEmpty();
  if (vider) {
    if (!isEmpty()) {
      viderTransactions();
    }
  }
  if (compte->isVirtuel()) {
    TransactionManager::getInstance()->chargerCompteVirtuel(compte);
  } else {
    TransactionManager::getInstance()->chargerCompteReel(compte);
  }
  sort();
}

void TransactionManager::chargerTous() {
  qDebug() << "TM:chargerTous";
  auto CM = comptes::CompteManager::getInstance();
  auto roots = CM->getComptesRoot();
  for (auto root : roots) {
    chargerCompte(root, false);
  }
  for (auto tr : transactions) {
    qDebug() << "TM::chargerTous : tr > " << tr->getReference();
  }
}

void TransactionManager::sortTransactions(QVector<Transaction*> vec,
                                          comparateurTransactions comp) {
  std::sort(vec.begin(), vec.end(), comp);
}

void TransactionManager::viderTransactions() {
  qDebug() << "Hello there TM::viderTransactions! ";
  for (auto t : this->transactions) {
    //    qDebug() << "tr : " << t->getDate() << " " << t->getReference() << " "
    //             << t->getDescription() << " " << t;
    delete t;
  }
  for (auto op : this->operations) {
    //    qDebug() << "op : " << op->getDebit().getMontant() << " "
    //             << op->getCredit().getMontant() << op->getCompte().getNom()
    //             << " "
    //             << op;
    delete op;
  }
  this->transactions.clear();
  this->operations.clear();
}

void TransactionManager::chargerTransaction(Transaction* transaction) {
  //  qDebug() << "TM:chargerTransaction here!";
  //  qDebug() << "pushing back tr : " << transaction->getDate() << " "
  //           << transaction->getReference() << " "
  //           << transaction->getDescription() << " " << transaction;
  this->transactions.push_back(transaction);
  for (auto op : transaction->getOperations()) {
    //    qDebug() << "pushing back op : " << op->getDebit().getMontant() << " "
    //             << op->getCredit().getMontant() << " " <<
    //             op->getCompte().getNom()
    //             << " " << op;
    this->operations.push_back(op);
  }
}

void TransactionManager::chargerTransactions(
    QVector<Transaction*> transactions) {
  for (auto tr : transactions) {
    chargerTransaction(tr);
  }
}

bool TransactionManager::isEmpty() { return this->transactions.empty(); }

TransactionManager::~TransactionManager() {
  for (auto t : transactions) {
    delete t;
  }
  for (auto op : operations) {
    delete op;
  }
};

Transaction* TransactionManager::ajouterTransaction(QDate date,
                                                    QString reference,
                                                    QString description,
                                                    QVector<Operation*> ops) {
  auto CM = comptes::CompteManager::getInstance();
  auto SM = StorageManager::getStorageManager();
  Transaction* tr = creerTransaction(date, reference, description, ops);
  if (tr->checkValidite()) {
    transactions.push_back(tr);
    CM->updateComptes(tr);
    SM->ajouterTransaction(tr);
    for (auto op : tr->getOperations()) {
      operations.push_back(op);
      SM->updateCompte(&(op->getCompte()));
    }

    return tr;
  }
  qDebug() << "L'ajout d'une transaction a échoué.";
  return nullptr;
};

Transaction* TransactionManager::creerTransaction(QDate date, QString reference,
                                                  QString description,
                                                  QVector<Operation*> ops) {
  Transaction* tr = new Transaction(date, reference, description, ops);
  // qDebug() << "Hello there, TransactionManager from creerTransaction
  // reporting "
  //          "live on the situation. I just tried creating a new Transaction. "
  //        "Was it though?";
  // qDebug() << "Transaction : " << tr->getDate() << " " << tr->getReference()
  //        << " " << tr;
  if (tr->checkValidite()) {
    // qDebug() << "Manager checked the validity. Beware!!";
    return tr;
  }
  // qDebug() << "TransactionManager::creerTransaction() La création d'une "
  //           "transaction a échoué.";
  return nullptr;
};

void TransactionManager::sort(comparateurTransactions comp) {
  std::sort(this->transactions.begin(), this->transactions.end(), comp);
}

const QVector<Transaction*> TransactionManager::getTransactionsByCompte(
    const comptes::AbstractCompte& compte) const {
  QVector<Transaction*> transactions_liees;
  auto liste_noms = comptes::CompteManager::getInstance()
                        ->getNomsComptesFilsReelsCompteVirtuel(&compte);
  for (auto tr : transactions) {
    bool ajouter = false;

    for (auto op : tr->getOperations()) {
      if (liste_noms.contains(op->getCompte().getNom())) {
        // qDebug() << "TM::getTransactionsByCompte : liste noms comptes réels "
        //          "trouvés : "
        //     << liste_noms;
        // qDebug() << "TM::getTransactionsByCompte : Trouvé une opération pour
        // : "
        //       << op->getCompte().getNom();
        ajouter = true;
      }
    }
    if (ajouter) {
      transactions_liees.push_back(tr);
    }
  }
  return (transactions_liees);
};

QVector<Operation*> TransactionManager::getOperationsByCompteByDate(
    const comptes::AbstractCompte& compte, const QDate& date) const {
  QVector<Operation*> operations_liees;
  auto liste_noms = comptes::CompteManager::getInstance()
                        ->getNomsComptesFilsReelsCompteVirtuel(&compte);
  for (auto tr : transactions) {
    if (tr->getDate() <= date) {
      for (auto op : tr->getOperations()) {
        if (liste_noms.contains(op->getCompte().getNom())) {
          operations_liees.push_back(op);
        }
      }
    }
  }
  return (operations_liees);
}

// QVectorIterator<Transaction*> find_trans()

void TransactionManager::supprimerTransaction(Transaction* transaction) {
  auto SM = StorageManager::getStorageManager();
  auto CM = comptes::CompteManager::getInstance();
  SM->supprimerTransaction(transaction);
  auto it = std::find(transactions.begin(), transactions.end(), transaction);
  auto trans_modif = (*it);
  CM->updateComptes(trans_modif, true);
  for (auto op : trans_modif->getOperations()) {
    auto it_operations = std::find(operations.begin(), operations.end(), op);
    delete *it_operations;
    operations.removeOne(*it_operations);
  }
  delete *it;
  transactions.removeOne(*it);
}

Transaction* TransactionManager::modifierTransaction(
    Transaction* transaction, QDate date, QString reference,
    QString description, QVector<Operation*> operations) {
  if (creerTransaction(date, reference, description, operations) != nullptr) {
    supprimerTransaction(transaction);
    auto tr = ajouterTransaction(date, reference, description, operations);
    return (tr);
  }
  qDebug() << "La modification d'une transaction a échoué.";
  return nullptr;
}

Transaction* TransactionManager::getTransaction(QString& ref, QDate& date) {
  for (auto it : transactions) {
    if ((it->getReference() == ref) && (it->getDate() == date)) {
      return (it);
    }
  }
  return nullptr;
}
