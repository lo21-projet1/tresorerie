/*!
 * \file fenetrerapprochementcompte.cpp
 * \author Bastien Bouré
 * \date 2020-06-09
 * \brief Implémentation de la classe RapprochementCompte
 */
#include "headers/fenetrerapprochementcompte.hpp"

#include <QMessageBox>
#include <QPushButton>
#include <stdexcept>

#include "ui_fenetrerapprochementcompte.h"

FenetreRapprochementCompte::FenetreRapprochementCompte(
    QWidget *parent, comptes::AbstractCompte *_compte)
    : QDialog(parent), ui(new Ui::FenetreRapprochementCompte), compte(_compte) {
  auto abstractcompte = dynamic_cast<comptes::AbstractCompteReel *>(compte);
  ui->setupUi(this);
  ui->date_donnee_D->setDate(
      abstractcompte->getDernierRapprochement().addDays(1));
  ui->date_donnee_D->setMinimumDate(
      abstractcompte->getDernierRapprochement().addDays(1));
  rafraichirSolde(abstractcompte->getDernierRapprochement().addDays(1));
  this->setWindowTitle("Rapprochement du compte : " + compte->getNom());
  if (!compte->isVirtuel()) {
    ui->dernier_rapprochement_D->setDate(
        dynamic_cast<comptes::AbstractCompteReel *>(compte)
            ->getDernierRapprochement());
    ui->dernier_rapprochement_S->setText(
        static_cast<QString>(dynamic_cast<comptes::AbstractCompteReel *>(compte)
                                 ->getSoldeRapproche()) +
        QString(" €"));

    QObject::connect(ui->date_donnee_D, SIGNAL(dateChanged(QDate const &)),
                     this, SLOT(rafraichirSolde(QDate const &)));
  } else {
    qDebug("Erreur tentative de rapprochement compte virtuel");
    QMessageBox::critical(this, "Erreur rapprochement",
                          "Impossible de rapprocher un compte virtuel");
    this->close();
  }
}

FenetreRapprochementCompte::~FenetreRapprochementCompte() { delete ui; }

void FenetreRapprochementCompte::rafraichirSolde(QDate const &date) {
  if (date <= ui->dernier_rapprochement_D->date()) {
    qDebug("Erreur dans la date selectionnée pour le rapprochement");
    QMessageBox::critical(this, "Erreur Date",
                          "La date sélectionnée doit être postérieure à la "
                          "date du dernier rapprochement");
    ui->date_donnee_D->setDate(ui->dernier_rapprochement_D->date().addDays(1));
  } else {
    if (!compte->isVirtuel()) {
      qDebug("Refresh solde");
      ui->date_donnee_S->setText(
          static_cast<QString>(
              dynamic_cast<comptes::AbstractCompteReel *>(compte)
                  ->getSoldeByDate(date)) +
          QString(" €"));
    } else {
      qDebug("Erreur compte virtuel dans le raffraichissement");
      QMessageBox::critical(this, "Erreur rapprochement",
                            "Impossible de rapprocher un compte virtuel");
      this->close();
    }
  }
}

void FenetreRapprochementCompte::on_valider_annuler_clicked(
    QAbstractButton *but) {
  if (static_cast<QPushButton *>(but) ==
      ui->valider_annuler->button(QDialogButtonBox::Apply)) {
    qDebug("Ok");
    if (!compte->isVirtuel()) {
      qDebug("Rapprochement");

      try {
        dynamic_cast<comptes::AbstractCompteReel *>(compte)->rapprocher(
            ui->date_donnee_D->date());
        QMessageBox::information(this, "Rapprochement effectué",
                                 "Le compte a été rapproché avec succès");
        this->close();
      } catch (std::invalid_argument &e) {
        QMessageBox::critical(this, "Erreur de rapprochement",
                              QString(e.what()));
      }

    } else {
      qDebug("Erreur compte virtuel dans le raffraichissement");
      QMessageBox::critical(this, "Erreur rapprochement",
                            "Impossible de rapprocher un compte virtuel");
      this->close();
    }
  }
}

QDate FenetreRapprochementCompte::getDateRapprochement() {
  return (ui->date_donnee_D->date());
}
