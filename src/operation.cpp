/*!
 * \file operation.cpp
 * \author Pascal Quach <pascal.quach.utc@gmail.com
 * \author Arthur Wacquez <arthur.wacquez@etu.utc.fr>
 * \brief Implémentation de la classe Opération
 * \date 2020-06-09
 */

#include "headers/operation.hpp"

bool Operation::checkValidite() {
//  qDebug() << "Hello, this is TransactionManager from Operation::checkValidite "
//              "! Let's check the validity of what you just entered, aye? :)";
  if (getDebit() < Monnaie("0,00")) {
    setCredit(getCredit() - getDebit());
    setDebit(Monnaie("0,00"));
//    qDebug() << "Well, seems like you entered a negative debit. Fancy complex "
//                "transaction, uh?";
  } else if (getCredit() < Monnaie("0,00")) {
    setDebit(getDebit() - getCredit());
    setCredit(Monnaie("0,00"));
//    qDebug() << "Well... a negative credit ! Are you perharps in denial ? "
//                "Making everything a credit won't add anything to your "
//                "account.";
  }
  if (getDebit() != Monnaie("0,00") && getCredit() != Monnaie("0,00")) {
    setDebit(getDebit() - getMin());
    setCredit(getCredit() - getMin());
//    qDebug() << "Ah ha ! You entered both... Don't do that.";
  }
//  qDebug() << "Seems like everyting is fine now. Good job !";
  return (getMin() == Monnaie("0,00"));
}
