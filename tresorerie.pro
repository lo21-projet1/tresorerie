QT += widgets
QT += sql
QT += printsupport

SOURCES += \
    src/fenetregenerationrapport.cpp \
    src/abstractcomptereel.cpp \
    src/comptefactory.cpp \
    src/comptereel.cpp \
    src/fenetreajoutertransaction.cpp \
    src/fenetrecreationcompte.cpp \
    src/fenetremodifiertransaction.cpp \
    src/fenetreprincipale.cpp \
    src/fenetrerapprochementcompte.cpp \
    src/main.cpp \
    src/monnaie.cpp \
    src/qcomboboxdelegate.cpp \
    src/qdoublespinboxdelegate.cpp \
    src/transactionmanager.cpp \
    src/dbmanager.cpp \
    src/operation.cpp \
    src/transaction.cpp \
    src/comptemanager.cpp \

HEADERS += \
    headers/fenetregenerationrapport.hpp \
    headers/abstractcompte.hpp \
    headers/abstractcomptereel.hpp \
    headers/abstractcomptevirtuel.hpp \
    headers/compte.hpp \
    headers/comptefactory.hpp \
    headers/comptevirtuel.hpp \
    headers/comptereel.hpp \
    headers/comptevirtuel.hpp \
    headers/compte.hpp \
    headers/dbmanager.hpp \
    headers/fenetreajoutertransaction.hpp \
    headers/fenetrecreationcompte.hpp \
    headers/fenetremodifiertransaction.hpp \
    headers/fenetreprincipale.hpp \
    headers/fenetrerapprochementcompte.hpp \
    headers/modelearbrescomptes.hpp \
    headers/monnaie.hpp \
    headers/operation.hpp \
    headers/qcomboboxdelegate.hpp \
    headers/qdoublespinboxdelegate.hpp \
    headers/singleton.hpp \
    headers/storagemanager.hpp \
    headers/transaction.hpp \
    headers/transactionmanager.hpp \
    headers/comptemanager.hpp

FORMS += \
    ui/fenetregenerationrapport.ui \
    ui/fenetreajoutertransaction.ui \
    ui/fenetreprincipale.ui \
    ui/fenetrecreationcompte.ui \
    ui/fenetrerapprochementcompte.ui

RESOURCES += \
    ressources.qrc

CONFIG(release, debug|release):DEFINES += QT_NO_DEBUG_OUTPUT
CONFIG += c++17


DISTFILES +=
