CREATE TABLE CompteVirtuel (
	id INTEGER,
	t VARCHAR NOT NULL,
	nom VARCHAR NOT NULL,
	PRIMARY KEY(id),
	CONSTRAINT C_TYPE CHECK (t LIKE 'actif' OR t LIKE 'passif' OR t LIKE 'depense' OR t LIKE 'recette')
);

CREATE TABLE CompteReel (
	id INTEGER,
	nom VARCHAR NOT NULL,
	solde BIGINT NOT NULL,
	dernierRapprochement DATE,
	PRIMARY KEY(id)
);

CREATE TABLE Closure (
	id INTEGER AUTO_INCREMENT,
	parent INTEGER,
	filsV INTEGER UNIQUE,
	filsR INTEGER UNIQUE,
	PRIMARY KEY(id),
	FOREIGN KEY (parent) REFERENCES CompteVirtuel,
	FOREIGN KEY (filsV) REFERENCES CompteVirtuel,
	FOREIGN KEY (filsR) REFERENCES CompteReel,
	CHECK ((filsV IS NOT NULL AND filsR IS NULL) OR (filsV IS NULL AND filsR IS NOT NULL))
);

CREATE TABLE Transac (
	ref VARCHAR,
	tStamp TIMESTAMP,
	tDesc VARCHAR,
	isRapproche INT,
	PRIMARY KEY (ref, tStamp)
);

CREATE TABLE Operation (
	tRef VARCHAR,
	tStamp TIMESTAMP,
	cId INTEGER,
	debit BIGINT NOT NULL,
	credit BIGINT NOT NULL,
	PRIMARY KEY(tRef, tStamp, cId),
	FOREIGN KEY (tRef, tStamp) REFERENCES Transac,
	FOREIGN KEY (cId) REFERENCES CompteReel,
	CHECK (debit >= 0 AND credit >= 0)
);

INSERT INTO CompteVirtuel VALUES (1, 'actif', 'actif');
INSERT INTO CompteVirtuel VALUES (2, 'passif', 'passif');
INSERT INTO CompteVirtuel VALUES (3, 'depense', 'depense');
INSERT INTO CompteVirtuel VALUES (4, 'recette', 'recette');

INSERT INTO Closure (parent, filsV) VALUES (1, 1);
INSERT INTO Closure (parent, filsV) VALUES (2, 2);
INSERT INTO Closure (parent, filsV) VALUES (3, 3);
INSERT INTO Closure (parent, filsV) VALUES (4, 4);
