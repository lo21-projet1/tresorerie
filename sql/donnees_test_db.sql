/* insertion de données test */

INSERT INTO CompteVirtuel VALUES (1, 'passif', 'Passifs', 100);
INSERT INTO CompteVirtuel VALUES (2, 'actif', 'Actifs', 900);
INSERT INTO CompteVirtuel VALUES (3, 'depense', 'Depenses', 1000);
INSERT INTO CompteVirtuel VALUES (4, 'recette', 'Recettes', 1800);

INSERT INTO CompteReel VALUES (5, 'Capitaux propres', 100);

INSERT INTO CompteReel VALUES (6, 'Caisse', 100);
INSERT INTO CompteReel VALUES (7, 'Livret A', 0);
INSERT INTO CompteReel VALUES (8, 'Banque', 800);

INSERT INTO CompteReel VALUES (9, 'Achat de materiel', 800);
INSERT INTO CompteReel VALUES (10, 'frais de personnel', 200);

INSERT INTO CompteReel VALUES (11, 'Cotisations', 600);
INSERT INTO CompteReel VALUES (12, 'Subventions', 200);
INSERT INTO CompteReel VALUES (13, 'Billetterie', 1000);

INSERT INTO Closure VALUES (1, 1, 1, NULL);
INSERT INTO Closure VALUES (2, 2, 2, NULL);
INSERT INTO Closure VALUES (3, 3, 3, NULL);
INSERT INTO Closure VALUES (4, 4, 4, NULL);

INSERT INTO Closure VALUES (5, 1, NULL, 5);

INSERT INTO Closure VALUES (6, 2, NULL, 6);
INSERT INTO Closure VALUES (7, 2, NULL, 7);
INSERT INTO Closure VALUES (8, 2, NULL, 8);

INSERT INTO Closure VALUES (9, 3, NULL, 9);
INSERT INTO Closure VALUES (10, 3, NULL, 10);

INSERT INTO Closure VALUES (11, 4, NULL, 11);
INSERT INTO Closure VALUES (12, 4, NULL, 12);
INSERT INTO Closure VALUES (13, 4, NULL, 13);

INSERT INTO Transac VALUES ('t1', '2020-05-10 21:12:36', 'subventions', 0);
INSERT INTO Operation VALUES('t1', '2020-05-10 21:12:36', 12, 0, 200);
INSERT INTO Operation VALUES('t1', '2020-05-10 21:12:36', 8, 200, 0);

INSERT INTO Transac VALUES ('t2', '2020-04-02 15:12:47', 'prevente', 0);
INSERT INTO Operation VALUES('t2', '2020-04-02 15:12:47', 13, 0, 300);
INSERT INTO Operation VALUES('t2', '2020-04-02 15:12:47', 8, 300, 0);

INSERT INTO Transac VALUES ('t3', '2020-05-02 15:12:47', 'vente', 0);
INSERT INTO Operation VALUES('t3', '2020-05-02 15:12:47', 13, 0, 700);
INSERT INTO Operation VALUES('t3', '2020-05-02 15:12:47', 8, 700, 0);

INSERT INTO Transac VALUES ('t4', '2020-05-06 12:45:27', 'cotisations 2020', 0);
INSERT INTO Operation VALUES('t4', '2020-05-06 12:45:27', 11, 0, 600);
INSERT INTO Operation VALUES('t4', '2020-05-06 12:45:27', 8, 600, 0);

INSERT INTO Transac VALUES('t5', '2020-05-12 12:07:26', 'depenses edition 2020', 0);
INSERT INTO Operation VALUES('t5', '2020-05-12 12:07:26', 8, 0, 1000);
INSERT INTO Operation VALUES('t5', '2020-05-12 12:07:26', 9, 800, 0);
INSERT INTO Operation VALUES('t5', '2020-05-12 12:07:26', 10, 200, 0);

INSERT INTO Transac VALUES('t6', '2020-05-14 03:02:54', 'deplacement louche d''argent', 0);
INSERT INTO Operation VALUES('t6', '2020-05-14 03:02:54', 5, 0, 100);
INSERT INTO Operation VALUES('t6', '2020-05-14 03:02:54', 6, 100, 0);

/* donnees délibérément non cohérentes :

// répétition d'id
INSERT INTO CompteVirtuel VALUES(5, 'recette', 'Mecettes', 200);

// transaction non équilibrée
INSERT INTO Transac VALUES('t7', '2020-05-14 03:02:54', 'apparition d''argent');
INSERT INTO Operation VALUES('t7', '2020-05-14 03:02:54', 5, 0, 1000);
INSERT INTO Operation VALUES('t7', '2020-05-14 03:02:54', 6, 100, 0);

// soldes non cohérents entre les comptes reels et virtuels
UPDATE CompteVirtuel SET solde = 10 WHERE id = 1;

// solde d'un compte reel non cohérent
UPDATE CompteReel SET solde = 0 WHERE id = 8;

*/
