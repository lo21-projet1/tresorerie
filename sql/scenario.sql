/*2  Création des comptes d’actifs*/

INSERT INTO CompteVirtuel VALUES (5, 'actif', 'Actifs');
INSERT INTO Closure (parent, filsV) VALUES (1, 5);
INSERT INTO CompteVirtuel VALUES (6, 'actif', 'BigBank');
INSERT INTO Closure (parent, filsV) VALUES (5, 6);

/* Compte chèque */
INSERT INTO CompteReel VALUES (7, 'Compte chèque', 125687, '2020-05-11 00:00:00');
INSERT INTO Closure (parent, filsR) VALUES (6, 7);

INSERT INTO CompteReel VALUES (8, 'Capitaux Propres', 0, '2020-05-10 00:00:00');
INSERT INTO Closure (parent, filsR) VALUES (2, 8);

INSERT INTO Transac VALUES ('l1', '2020-05-11 00:00:00', 'Solde Initial', 0);
INSERT INTO Operation VALUES ('l1', '2020-05-11 00:00:00', 7, 125687, 0);
INSERT INTO Operation VALUES ('l1', '2020-05-11 00:00:00', 8, 0, 125687);
UPDATE CompteReel SET solde = solde + 125687 WHERE id = 8;

/* Livret A */
INSERT INTO CompteReel VALUES (9, 'Livret A', 369770, '2020-05-11 00:00:00');
INSERT INTO Closure (parent, filsR) VALUES (3, 9);

INSERT INTO Transac VALUES ('l2', '2020-05-11 00:00:00', 'Initialisation Livret A', 0);
INSERT INTO Operation VALUES ('l2', '2020-05-11 00:00:00', 9, 369770, 0);
INSERT INTO Operation VALUES ('l2', '2020-05-11 00:00:00', 8, 0, 369770);
UPDATE CompteReel SET solde = solde + 369770 WHERE id = 8;

/* Caisse */
INSERT INTO CompteReel VALUES (10, 'Caisse', 16830, '2020-05-11 00:00:00');
INSERT INTO Closure (parent, filsR) VALUES (5, 10);

INSERT INTO Transac VALUES ('l3', '2020-05-11 00:00:00', 'Initialisation Caisse', 0);
INSERT INTO Operation VALUES ('l3', '2020-05-11 00:00:00', 10, 16830, 0);
INSERT INTO Operation VALUES ('l3', '2020-05-11 00:00:00', 8, 0, 16830);
UPDATE CompteReel SET solde = solde + 16830 WHERE id = 8;

/* Chèques à encaisser */
INSERT INTO CompteReel VALUES (11, 'Chèques à encaisser', 7000, '2020-05-11 00:00:00');
INSERT INTO Closure (parent, filsR) VALUES (5, 11);

INSERT INTO Transac VALUES ('l4', '2020-05-11 00:00:00', 'Initialisation chèques', 0);
INSERT INTO Operation VALUES ('l4', '2020-05-11 00:00:00', 11, 7000, 0);
INSERT INTO Operation VALUES ('l4', '2020-05-11 00:00:00', 8, 0, 7000);
UPDATE CompteReel SET solde = solde + 7000 WHERE id = 8;


/* 3  Création des comptes de passifs */

INSERT INTO CompteVirtuel VALUES (12, 'passif', 'Passifs');
INSERT INTO Closure (parent, filsV) VALUES (2, 12);
INSERT INTO CompteReel VALUES (13, 'Emprunt', 120000, '2020-05-11 00:00:00');
INSERT INTO Closure (parent, filsR) VALUES (12, 13);

INSERT INTO Transac VALUES ('l5', '2020-05-11 00:00:00', 'Initialisation Emprunt', 0);
INSERT INTO Operation VALUES ('l5', '2020-05-11 00:00:00', 13, 0, 120000);
INSERT INTO Operation VALUES ('l5', '2020-05-11 00:00:00', 8, 120000, 0);
UPDATE CompteReel SET solde = solde - 120000 WHERE id = 8;


/* 4  Création des comptes de dépenses et de recettes */

/* Dépense & Recettes */
INSERT INTO CompteVirtuel VALUES (14, 'depense', 'Dépenses');
INSERT INTO Closure (parent, filsV) VALUES (3, 14);
INSERT INTO CompteVirtuel VALUES (15, 'recette', 'Recettes');
INSERT INTO Closure (parent, filsV) VALUES (4, 15);

/* Comptes de Dépense */
INSERT INTO CompteReel VALUES (16, 'Assurance', 0, '2020-05-11 00:00:00');
INSERT INTO Closure (parent, filsR) VALUES (14, 16);
INSERT INTO CompteVirtuel VALUES (17, 'depense', 'Frais bancaire');
INSERT INTO Closure (parent, filsV) VALUES (14, 17);
INSERT INTO CompteReel VALUES (18, 'Tenue de compte', 0, '2020-05-11 00:00:00');
INSERT INTO Closure (parent, filsR) VALUES (17, 18);
INSERT INTO CompteReel VALUES (19, 'Intérêts d''emprunt', 0, '2020-05-11 00:00:00');
INSERT INTO Closure (parent, filsR) VALUES (17, 19);
INSERT INTO CompteReel VALUES (20, 'Frais de personnel', 0, '2020-05-11 00:00:00');
INSERT INTO Closure (parent, filsR) VALUES (14, 20);
INSERT INTO CompteReel VALUES (21, 'Matériel', 0, '2020-05-11 00:00:00');
INSERT INTO Closure (parent, filsR) VALUES (14, 21);

/* Comptes de Recette */
INSERT INTO CompteReel VALUES (22, 'Cotisations', 0, '2020-05-11 00:00:00');
INSERT INTO Closure (parent, filsR) VALUES (15, 22);
INSERT INTO CompteReel VALUES (23, 'Dons', 0, '2020-05-11 00:00:00');
INSERT INTO Closure (parent, filsR) VALUES (15, 23);
INSERT INTO CompteReel VALUES (24, 'Intérêts d''épargne', 0, '2020-05-11 00:00:00');
INSERT INTO Closure (parent, filsR) VALUES (15, 24);
INSERT INTO CompteReel VALUES (25, 'Subventions', 0, '2020-05-11 00:00:00');
INSERT INTO Closure (parent, filsR) VALUES (15, 25);
