/* creation des vues */

CREATE VIEW v_coherence_operations_transaction AS
  SELECT SUM(o.credit) - SUM(o.debit) as credit_moins_debit
  FROM Operation o
  GROUP BY o.tRef, o.tStamp;

CREATE VIEW v_conflit_id_compte AS
  SELECT cv.id as id_problematique
  FROM CompteVirtuel cv, CompteReel cr
  WHERE cv.id = cr.id;

CREATE VIEW v_coherence_solde_cReel_operations AS
  SELECT cr.id,
      CASE
      WHEN (cv.t LIKE 'actif' OR cv.t LIKE 'depense') THEN SUM(o.debit)-SUM(o.credit)-cr.solde
      ELSE SUM(o.debit)-SUM(o.credit)+cr.solde
    END AS difference_solde_operations
  FROM Operation o, CompteReel cr, Closure c, CompteVirtuel cv
  WHERE o.cId = c.filsR
  AND c.parent = cv.id
  AND c.filsR = cr.id
  GROUP BY o.cID, cr.solde, cr.id, cv.t;

CREATE VIEW Comptes AS
  SELECT id id, 1 isVirtuel, nom nom, t t, NULL solde
    FROM CompteVirtuel
  UNION
  SELECT id id, 0 isVirtuel, nom nom, NULL t, solde solde
  FROM CompteReel;
