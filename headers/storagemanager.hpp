/*!
 * \file storagemanager.hpp
 * \author Sébastien Darche <darchese@etu.utc.fr>
 * \date 2020-05-08
 * \brief Classe abstraite StorageManager
 * \details Abstraction du singleton StorageManager
 * qui s'occupe de la gestion des comptes, transactions
 * et opérations, sans s'occuper de l'implémentation
 * réelle du stockage (fichier, bdd, ...)
 */

#ifndef STORAGEMANAGER_H
#define STORAGEMANAGER_H

#include <QString>
#include <QVector>
#include <memory>

#include "headers/compte.hpp"
#include "headers/operation.hpp"
#include "headers/transaction.hpp"

/*!
 * \class StorageManager
 * \brief Design Pattern Strategy pour le stockage persistant des données
 * \details L'interface abstraite StorageManager propose une interface
 * centralisée faisant abstraction du réel moyen de stockage des données.
 */
class StorageManager {
  // Strategy
 public:
  /*!
   * \brief Renvoie l'instance du StorageManager implémenté
   * \returns shared_pointer<StorageManager> sur le storage manager réel
   */
  static std::shared_ptr<StorageManager> getStorageManager();
  /*!
   * \brief Ouverture d'un fichier de sauvegarde
   * \details Il est nécessaire d'ouvrir un fichier de sauvegarde avant de
   * pouvoi utiliser le storagemanager, quelque soit l'implémentation de
   * l'interface
   */
  virtual void ouvrirFichier(const QString nom_fichier) = 0;
  /*!
   * \brief Initialisation d'un fichier de sauvegarde
   * \details La méthode intialise un fichier chargé au préalable
   * pour le préparer au stockage de données (création de tables,
   * initialisation hiérarchie de stockage)
   */
  virtual void initFichier() = 0;

  /* Getters */
  /*!
   * \brief Renvoie un QVector des comptes situés à la racine du stockage
   * \details Cherche en mémoire les comptes situés à la racine du fichier de
   * stockage Cela permet par la suite de construire l'arborescence par appel à
   * la méthode getComptesFils(comptes::AbstractCompte* compte)
   */
  virtual QVector<comptes::AbstractCompte*> getComptesRacine() const = 0;
  /*!
   * \brief Renvoie un QVector des comptes fils d'un compte
   * \param compte Le compte père
   * \details Cherche en mémoire les comptes fils d'un compte
   * passé en paramètre, les génère par l'intermédiaire du
   * CompteFactory puis renvoie un vecteur les contenant
   */
  virtual QVector<comptes::AbstractCompte*> getComptesFils(
      comptes::AbstractCompte* compte) const = 0;
  /*!
   * \brief Renvoie le premier compte possédant le nom nom_com
   * \param nom_com Nom du compte
   */
  virtual comptes::AbstractCompte* getCompte(QString nom_com) const = 0;
  /*!
   * \brief Renvoie un pointeur vers l'unique Compte d'id id_compte
   * \param id_compte id du compte recherché
   */
  virtual comptes::AbstractCompte* getCompte(int id_compte) const = 0;
  /*!
   * \brief Renvoie les transactions rattachées à un compte
   * \param compte Compte concerné par la requête
   * \return QVector contenant les transactions dans lesquelles le
   * compte est concerné
   */
  virtual QVector<Transaction*> getTransactionsByCompte(
      comptes::AbstractCompte* compte) const = 0;
  /*!
   * \brief Renvoie les transactions rattachées à un compte
   * \param id_compte l'id du compte concerné par la requête
   * \return QVector contenant les transactions dans lesquelles le
   * compte est concerné
   */
  virtual QVector<Transaction*> getTransactionsByIdCompte(
      int id_compte) const = 0;
  /*!
   * \brief Renvoie les opérations composant une transaction
   * \param transac la transaction concernée par la requête
   * \return QVector contenant les opérations composant la transaction
   */
  virtual QVector<Operation*> getOperationsByTransaction(
      Transaction& transac) const = 0;
  /*!
   * \brief Renvoie les opérations composant une transaction, identifiée par une
   * date et une référence
   * \param date date de la transaction
   * \param ref ref de la transaction
   * \return QVector contenant les opérations composant la transaction
   */
  virtual QVector<Operation*> getOperationsByTransaction(
      const QDate& date, const QString& ref) const = 0;

  /* Stockage */
  /*!
   * \brief Crée un nouveau compte dans le système de stockage
   * \param compte le compte à stocker
   * \details Ne pas oublier d'update le père puis le nouveau fils par le biais
   * de la méthode updateCompte
   */
  virtual void ajouterCompte(comptes::AbstractCompte* compte) = 0;
  /*!
   * \brief Met à jour un compte dans le système de stockage
   * \param compte le compte à stocker
   * \details si le compte n'existe pas dans le système de stockage,
   * il sera créé par un appel à ajouterCompte(). Cette fonction est à appeler
   * successivement sur le père puis sur le nouveau fils afin d'assurer la
   * cohérence des données dans l'arborescence des comptes du stockage.
   */
  virtual void updateCompte(comptes::AbstractCompte* c) = 0;
  /*!
   * \brief Crée une nouvelle transaction dans le système de stockage
   * \param transac la transaction à stocker
   */
  virtual void ajouterTransaction(Transaction* transac) = 0;
  /*!
   * \brief Met à jour une transaction dans le système de stockage
   * \param transac la transaction à stocker
   * \details si la transaction n'existe pas dans le système de stockage,
   * elle sera créée par un appel à ajouterTransaction()
   */
  virtual void updateTransaction(Transaction* transac) = 0;
  /*!
   * \brief Marque une transaction comme rapprochée
   * \param transac la transaction à rapprocher
   * \details Au prochain chargement (de l'application),
   * la transaction sera marquée comme rapprochée
   */
  virtual void rapprocherTransaction(Transaction* transac) = 0;
  /*!
   * \brief Supprime un certain compte du stockage
   * \param compte Compte à supprimer
   * \details Cette méthode n'est à appeler seulement à la
   * suppression définitive d'un compte, il est préféré d'utiliser
   * updateCompte()
   */
  virtual void supprimerCompte(comptes::AbstractCompte* compte) = 0;
  /*!
   * \brief Supprime une transaction
   * \param transac Transaction à supprimer
   */
  virtual void supprimerTransaction(Transaction* transac) = 0;
  /*!
   * \brief Retourne le fichier actuellement chargé par le storageManager
   */
  virtual const QString getLoadedFile() = 0;

  virtual int getNextIndexCompte() = 0;

  /*!
   * \brief Transforme un type de compte en une QString utilisable
   * par le système de stockage
   */
  static const QString tCompteToString(comptes::TypeCompte t);
  /*!
   * \brief Transforme un une QString en un enum TypeCompte
   */
  static comptes::TypeCompte stringToTCompte(const QString s);
};

#endif  // STORAGEMANAGER_H
