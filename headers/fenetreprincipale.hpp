/*!
 * \file fenetreprincipale.hpp
 * \author Rémy HUET <utc@remyhuet.fr>
 * \date 2020-04-23
 * \brief Déclarations de la classe FenetrePrincipale
 * \details Cette classe correspond à la fenêtre principale de l'application Qt.
 * Elle est générée grâce à QtDesigner. L'ensembles des éléments associés est
 * donc accessible par le pointeur ui.
 */

#ifndef LO21_FENETREPRINCIPALE_HPP
#define LO21_FENETREPRINCIPALE_HPP

#include <QMainWindow>
#include <QFileDialog>
#include "modelearbrescomptes.hpp"
#include "dbmanager.hpp"

namespace Ui {
class FenetrePrincipale;
}

/*!
 * \class FenetrePrincipale fenetreprincipale.hpp
 * \brief Classe FenetrePrincipale
 * \details Cette classe utilise la classe Ui::FenetrePrincipale, auto générée
 * par QtDesigner
 */
class FenetrePrincipale : public QMainWindow {
  Q_OBJECT
 public:
  /*!
   * \brief Constructeur trivial de la classe
   * \param parent le QWidget parent de la classe
   */
  explicit FenetrePrincipale(QWidget *parent = nullptr);
  /*!
   * \brief Destructeur de la classe
   */
  ~FenetrePrincipale();

  /*!
   * \brief Afficher les comptes actuellement en mémoire dans le
   * tree view
   */
  void AfficherComptes();

  /*!
   * \brief Raffraichissement de l'affichage des comptes,
   * méthode à appeler dès qu'une modification est faite sur
   * les comptes
   * \param affichage, si oui ou non réafficher les comptes en
   * mémoire après avoir clear l'affichage, paramètre à mettre
   * sur false lors de la création d'un nouveau fichier
   */
  void RefreshAffichageCompte(bool affichage = true);

  /*!
   * \brief Affiche les transactions liées au compte passé
   * en paramètre
   * \param compte dont on veut les paramètres
   */
  void AfficherTransactions(comptes::AbstractCompte *compte);

  /*!
   * \brief Raffraichissement de l'affichage des transactions,
   * méthode à appeler dès qu'une modification est faites sur les transactions
   * du compte sélectionné et dès qu'un nouveau compte est séléctionné
   * \param compte le compte actuellement sélectionné
   */
  void RefreshAffichageTransaction(comptes::AbstractCompte *compte = nullptr);

 private:
  /*!
   * \brief enableActions Active les actions relatives à la gestion de la
   * trésorerie.
<<<<<<< Updated upstream
   * \param isVirtuel Passage en paramètre de la virtualité du compte
   * sélectionné.
   * \default isVirtuel est par défaut true, pour ne pas activer l'action
   * rapprocher.
   */
  void enableActions(bool isVirtuel = true);
  /*!
   * \brief Méthode récursive permettant l'affichage des fils du compte
   * passé en paramètre
   * \param pere le compte dont on veut afficher les fils
   * \return une vector de QList de QStandardItem permettant l'ajout à l'item
   * père
   */
  QVector<QList<QStandardItem *>> AfficherFils(comptes::AbstractCompte *pere);

  /*!
   * \brief Retournes les transactions de tout les sous comptes d'un
   * compte virtuel passé en paramètre
   * \param pere le compte virtuel dont on veut les transactions
   * \return QVector<Transactions*> toutes les transactions liées
   * aux sous comptes de pere
   */
  QVector<Transaction *> GetTransactionFils(comptes::AbstractCompte *pere);
  /*!
   * \brief Composant UI
   * \details Ce composant permet l'interface avec le code auto-généré par les
   * outils UI de Qt. C'est par lui que l'on accède aux différents éléments de
   * la fenêtre
   */
  Ui::FenetrePrincipale *ui;
  /*!
   * \brief Modele de données
   * \details Ce modèle sert à l'affichage des comptes sous la forme d'arbre
   * dans le QTreeView de l'UI.
   */
  QStandardItemModel *arbre_comptes;
  /*!
   * \brief Modele de données
   * \details Ce modèle sert à l'affichage des transactions sous la forme d'un
   * tableau dans le QTableView de l'UI.
   */
  QStandardItemModel *liste_transactions;

 public slots:
  /*!
   * \brief Slot appelé lors de l'activation du bouton ajouter transfert
   */
  void on_action_ajouter_transfert_triggered();
  /*!
   * \brief Slot appelé lors de l'activation du bouton nouveau compte
   */
  void on_action_nouveau_compte_triggered();
  /*!
   * \brief Slot pour la fenetre de rapprochement
   */
  void on_action_rapprocher_triggered();
  /*!
       * \brief Slot pour la cloture du livre
       */
  void on_action_clore_livre_triggered();
  /*!
   * \brief Slot bouton Ouvrir
   * \details Ce slot ouvre une boîte de dialogue afin de sélectionner un
   * fichier
   * pour instancier la base de données.
   */
  void on_action_ouvrir_triggered();
  /*!
   * \brief Slot bouton Nouveau fichier
   * \details Ce slot permet à l'utilidateur de créer un nouveau fichier vide de
   * base de données.
   */
  void on_action_nouveau_fichier_triggered();
  /*!
   * \brief Slot bouton Modifier transfert
   * \details Ce slot lance la fenêtre de modification de transaction
   * Il n'est activable que si une transaction est sélectionnée
   */
  void on_action_modifier_transfert_triggered();
  /*!
   * \brief on_action_quitter_triggered Quitter l'application
   * \details Ce slot demande à l'utilisateur confirmation qu'il souhaite
   * quitter l'application. Il servira éventuellement à lancer des commandes de
   * sauvegarde de paramètres, etc.
   */
  void on_action_quitter_triggered();

 private slots:
  /*!
   * \brief Slot appelé lors de la selection d'un nouveau compte
   * \param selected le compte selectionné
   * \param deselected le compte précédement sélectionné
   */
  void selectionCompte(const QItemSelection &selected,
                       const QItemSelection &deselected);
  /*!
   * \brief Slot appelé lors de l'expansion d'une partie de l'arbre
   * afin de raffraichir la taille des colonnes
   * \param index de l'objet étendu
   */
  void on_arbres_comptes_expanded(const QModelIndex &index);
  /*!
   * \brief Slot appelé lors de la selection d'une nouvelle transaction
   * \param selected la transaction sélectionnée
   * \param deselected la transaction précédement sélectionnée
   */
  void selectionTransaction(const QItemSelection &selected,
                            const QItemSelection &deselected);
  /*!
   * \brief Slot appellé pour générer des rapports pour les comptes
   */
  void on_action_generer_rapport_triggered();

};

#endif  // LO21_FENETREPRINCIPALE_HPP
