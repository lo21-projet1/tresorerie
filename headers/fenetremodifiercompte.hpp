/*!
 * \file fenetremodifiercompte.hpp
 * \author Pascal Quach <pascal.quach.utc@gmail.com>
 * \date 2020-06-18
 * \brief Déclarations de la fenetre de modification des comptes
 */

#ifndef FENETREMODIFIERCOMPTE_HPP
#define FENETREMODIFIERCOMPTE_HPP

#include "headers/fenetrecreationcompte.hpp"

class FenetreModifierCompte : public FenetreCreationCompte {
 public:
  /*!
   * \brief FenetreModifierCompte hérite de FenetreCreationCompte pour
   * permettre de modifier un compte (son nom), ou le supprimer.
   * \param parent Le parent du QWidget, ici la feneêtre principale.
   * \param compte Le compte qui doit être modifiée.
   */
  FenetreModifierCompte(QWidget* parent, comptes::AbstractCompte* compte);
  /*!
   * \brief  ~FenetreModifierCompte() Destructeur par défaut
   */
  virtual ~FenetreModifierCompte() = default;
};

#endif  // FENETREMODIFIERCOMPTE_HPP
