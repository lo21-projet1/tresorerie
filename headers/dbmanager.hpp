/*!
 * \file dbmanager.hpp
 * \author Sébastien Darche <darchese@etu.utc.fr>
 * \date 2020-06-16
 * \brief Singleton DBManager pour stockage physique des données
 * \details Implémentation de l'interface StorageManager
 * pour le stockage des données dans une base de données SQLite
 */

#ifndef DBMANAGER_H
#define DBMANAGER_H

#include <QSqlDatabase>
#include <QtSql>

#include "headers/singleton.hpp"
#include "headers/storagemanager.hpp"

#define ID_ACTIF 1
#define ID_PASSIF 2
#define ID_DEPENSE 3
#define ID_RECETTE 4

/*!
 * \class DBManager dbmanager.hpp
 * \brief Implémentation de l'interface StorageManager pour une base de données
 * SQLite
 */
class DBManager : public StorageManager, public patterns::Singleton<DBManager> {
  friend class patterns::Singleton<DBManager>;
  friend class StorageManager;

 private:
  /* METHODES */
  DBManager();
  DBManager(const QString dbPath);
  void lireFichier(const QString& file);

  // Constructeur

  /* Attributs */
  QSqlDatabase db;
  QString dbPath;
  bool ouvert = false;

 public:
  // Interface du StorageManager
  /*!
   * \brief Ouverture d'un fichier de BDD sqlite
   * \details Le fichier doit être un fichier binaire sqlite3 généré par
   * l'application ou bien par SQLite3
   */
  void ouvrirFichier(const QString nom_fichier) override;
  /*!
   * \brief Initialisation de la base de données
   * \details Création des tables de la bdd et insertion des comptes "dummy"
   * actifs, passifs, dépense et recette.
   */
  void initFichier() override;
  /*!
   * \brief Renvoie un QVector des comptes situés à la racine du stockage
   * \details Cherche en mémoire les comptes situés à la racine de la base de
   * données (closure table). Cela permet par la suite de construire
   * l'arborescence par appel à la méthode
   * getComptesFils(comptes::AbstractCompte* compte)
   */
  virtual QVector<comptes::AbstractCompte*> getComptesRacine() const override;
  /*!
   * \brief Renvoie un QVector des comptes fils d'un compte
   * \param compte Le compte père
   * \details Cherche en mémoire les comptes fils d'un compte
   * passé en paramètre, les génère par l'intermédiaire du
   * CompteFactory puis renvoie un vecteur les contenant
   */
  virtual QVector<comptes::AbstractCompte*> getComptesFils(
      comptes::AbstractCompte* compte) const override;
  /*!
   * \brief Renvoie le premier compte possédant le nom nom_com
   * \param nom_com Nom du compte
   */
  virtual comptes::AbstractCompte* getCompte(QString nom_com) const override;
  /*!
   * \brief Renvoie l'unique compte en mémoire ayant pour id id_compte
   * \param id_compte id recherché
   */
  virtual comptes::AbstractCompte* getCompte(int id_compte) const override;
  /*!
   * \brief Renvoie les transactions rattachées à un compte
   * \param compte Compte concerné par la requête
   * \return QVector contenant les transactions dans lesquelles le
   * compte est concerné
   */
  virtual QVector<Transaction*> getTransactionsByCompte(
      comptes::AbstractCompte* compte) const override;
  /*!
   * \brief Renvoie les transactions rattachées à un compte
   * \param id_compte l'id du compte concerné par la requête
   * \return QVector contenant les transactions dans lesquelles le
   * compte est concerné
   */
  virtual QVector<Transaction*> getTransactionsByIdCompte(
      int id_compte) const override;
  /*!
   * \brief Renvoie les opérations composant une transaction
   * \param transac la transaction concernée par la requête
   * \return QVector contenant les opérations composant la transaction
   */
  virtual QVector<Operation*> getOperationsByTransaction(
      Transaction& transac) const override;
  /*!
   * \brief Renvoie les opérations composant une transaction, identifiée par une
   * date et une référence
   * \param date date de la transaction
   * \param ref ref de la transaction
   * \return QVector contenant les opérations composant la transaction
   */
  virtual QVector<Operation*> getOperationsByTransaction(
      const QDate& date, const QString& ref) const override;

  /* Stockage */
  /*!
   * \brief Crée un nouveau compte dans le système de stockage
   * \param compte le compte à stocker
   * \details Ne pas oublier d'update le père puis le nouveau fils par le biais
   * de la méthode updateCompte
   */
  virtual void ajouterCompte(comptes::AbstractCompte* compte) override;
  /*!
   * \brief Met à jour un compte dans le système de stockage
   * \param compte le compte à stocker
   * \details si le compte n'existe pas dans le système de stockage,
   * il sera créé par un appel à ajouterCompte(). Cette fonction est à appeler
   * successivement sur le père puis sur le nouveau fils afin d'assurer la
   * cohérence des données dans l'arborescence des comptes du stockage.
   */
  virtual void updateCompte(comptes::AbstractCompte* compte) override;
  /*!
   * \brief Crée une nouvelle transaction dans le système de stockage
   * \param transac la transaction à stocker
   */
  virtual void ajouterTransaction(Transaction* transac) override;
  /*!
   * \brief Met à jour une transaction dans le système de stockage
   * \param transac la transaction à stocker
   * \details si la transaction n'existe pas dans le système de stockage,
   * elle sera créée par un appel à ajouterTransaction()
   */
  virtual void supprimerCompte(comptes::AbstractCompte* compte) override;
  /*!
   * \brief Marque une transaction comme rapprochée
   * \param transac la transaction à rapprocher
   * \details Au prochain chargement (de l'application),
   * la transaction sera marquée comme rapprochée
   */
  virtual void updateTransaction(Transaction* transac) override;
  /*!
   * \brief Supprime un certain compte du stockage
   * \param compte Compte à supprimer
   * \details Cette méthode n'est à appeler seulement à la
   * suppression définitive d'un compte, il est préféré d'utiliser
   * updateCompte()
   */
  virtual void rapprocherTransaction(Transaction* transac) override;
  /*!
   * \brief Supprime une transaction
   * \param transac Transaction à supprimer
   */
  virtual void supprimerTransaction(Transaction* transac) override;
  /*!
   * \brief Retourne le fichier actuellement chargé par le storageManager
   */
  virtual const QString getLoadedFile() override { return dbPath; }

  /* DB utilities */
  /*!
   * \brief Retourne le prochain id de compte libre dans la bdd
   */
  virtual int getNextIndexCompte() override;
  int getVirtualParentId(int id_fils) const;
  /*!
   * \brief Vérifie si un compte est à la racine dans la bdd
   */
  bool isRacine(int id_compte) const;
  /*!
   * \brief Vérifie si un compte est bien présent dans la closure table
   * \details L'ensemble des comptes est censé être stocké dans la closure table
   * afin d'assurer sa cohérence. Si un compte n'est pas dans la closure table,
   * cela veut dire qu'il est à la racine, on le replace dans un des comptes
   * primitifs (actif, passif, dépense ou recettes)
   */
  bool isNotInClosure(int id_compte) const;
  /*!
   * \brief Place un compte au compte racine correspondant
   * \details Régularise la position d'un compte si ce dernier n'a pas de père,
   * en le plaçant dans la closure table comme fils d'un compte primitif
   */
  void setCompteInRacine(comptes::AbstractCompte* compte) const;
};

#endif  // DBMANAGER_H
