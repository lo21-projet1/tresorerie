/*!
 * \file transactionManager.hpp
 * \author Pascal Quach <pascal.quach.utc@gmail.com>
 * \author Rémy Huet <utc@remyhuet.fr>
 * \author Arthur Wacquez <arthur.wacquez@etu.utc.fr>
 * \date 2020-06-10
 * \brief Implémentation du TransactionManager
 */
#ifndef LO21_TRANSACTIONMANAGER_HPP
#define LO21_TRANSACTIONMANAGER_HPP

#include <QDate>
#include <QVector>

#include "headers/compte.hpp"
#include "headers/dbmanager.hpp"
#include "headers/monnaie.hpp"
#include "headers/operation.hpp"
#include "headers/singleton.hpp"
#include "headers/transaction.hpp"
/*!
 * \class TransactionManager
 * \brief Propose une implémentation de la classe TransactionManager
 * \details Le TransactionManager est un singleton qui gère
 * toutes les transactions actuellement chargées. On appelle également
 * l'instance du TransactionManager pour ajouter une transaction.
 */
class TransactionManager : public patterns::Singleton<TransactionManager> {
 public:
  /*!
   * \brief La structure comparateurTransactions sert de classe de comparaison
   * pour le tri d'objets Transactions.
   */
  struct comparateurTransactions {
   public:
    /*!
     * \enum ordre L'enum ordre correspond aux différents types de tri possible.
     */
    enum ordre { croissant, decroissant };

   private:
    /*!
     * \brief L'ordre choisi pour le comparateur de transactions.
     */
    ordre d;

   public:
    /*!
     * \brief comparateurTransactions Constructeur de la classe
     * comparateurTransactions
     * \param o l'ordre de tri choisi : croissant ou décroissant
     * \default La valeur par défaut de o est l'ordre croissant.
     */
    comparateurTransactions(ordre o = decroissant) : d(o) {}
    /*!
     * \brief operator() La comparaison de deux transactions selon l'ordre du
     * comparateur.
     * \param t1 Une référence constante sur la première transaction
     * \param t2 Une référence constante sur la deuxième transaction
     * \return Retourne la comparaison stricte des dates de t1 et t2 selon la
     * relation d'ordre spécifié à la construction du comparateur.
     */
    bool operator()(Transaction const& t1, Transaction const& t2) {
      if (d == ordre::croissant) {
        return t1.getDate() < t2.getDate();
      } else {
        return t1.getDate() > t2.getDate();
      }
    }
    /*!
     * \brief operator() La comparaison de deux transactions selon l'ordre du
     * comparateur.
     * \param t1 Une référence constante sur la première transaction
     * \param t2 Une référence constante sur la deuxième transaction
     * \return Retourne la comparaison stricte des dates de t1 et t2 selon la
     * relation d'ordre spécifié à la construction du comparateur.
     */
    bool operator()(Transaction& t1, Transaction& t2) {
      if (d == ordre::croissant) {
        return t1.getDate() < t2.getDate();
      } else {
        return t1.getDate() > t2.getDate();
      }
    }
    /*!
     * \brief operator() La comparaison de deux transactions selon l'ordre du
     * comparateur.
     * \param t1 Un pointeur constant sur la première transaction
     * \param t2 Un pointeur constant sur la deuxième transaction
     * \return Retourne la comparaison stricte des dates de t1 et t2 selon la
     * relation d'ordre spécifié à la construction du comparateur.
     */
    bool operator()(Transaction const* t1, Transaction const* t2) {
      if (d == ordre::croissant) {
        return t1->getDate() < t2->getDate();
      } else {
        return t1->getDate() > t2->getDate();
      }
    }
    /*!
     * \brief operator() La comparaison de deux transactions selon l'ordre du
     * comparateur.
     * \param t1 Un pointeur sur la première transaction
     * \param t2 Un pointeur sur la deuxième transaction
     * \return Retourne la comparaison stricte des dates de t1 et t2 selon la
     * relation d'ordre spécifié à la construction du comparateur.
     */
    bool operator()(Transaction* t1, Transaction* t2) {
      if (d == ordre::croissant) {
        return t1->getDate() < t2->getDate();
      } else {
        return t1->getDate() > t2->getDate();
      }
    }
  };

 private:
  /*!
   * \details La déclaration d'amitié est nécessaire pour que le DP
   * Singleton puisse construire un nouveau TransactionManager
   * en utilisant le constructeur.
   * \brief Déclaration d'amitié entre TransactionManager
   * et Singleton<TransactionManager>
   */
  friend class patterns::Singleton<TransactionManager>;
  /*!
   * \brief TransactionManager Constructeur par défaut de la classe
   * TransactionManager
   */
  TransactionManager() = default;

  /*!
   * \brief transactions Le vecteur des transactions
   * \details Toutes les adresses des transactions sont contenues dans un
   * QVector de telle sorte à pourvoir les accéder à travers les getters
   * définies à l'aide de QVectorIterator.
   */
  QVector<Transaction*> transactions;
  /*!
   * \brief operations Le vecteur des opérations
   * \details Toutes les adresses des opérations sont contenues dans un QVector
   * dont TransactionManager a la responsabilité (création et destruction).
   * Les transactions disposent elle d'un vecteur de pointeurs vers les
   * opérations
   */
  QVector<Operation*> operations;

  /*!
   * \brief sort Trie le vecteur transactions par ordre croissant des dates.
   * \details comp Un comparateurTransactions qui donne l'ordre dans lequel il
   * faut trier les transactions
   */
  void sort(comparateurTransactions comp = comparateurTransactions());
  /*!
   * \brief chargerCompteReel charge en mémoire toutes les transactions d'un
   * compte réel
   * \param compte Le compte réel dont il faut charger les transactions
   */
  void chargerCompteReel(comptes::AbstractCompte* compte);
  /*!
   * \brief chargerCompteVirtuel charge en mémoire toutes les transactions des
   * comptes fils
   * du compte virtuel
   * \param compte le compte virtuel dont il faut charger les transactions
   * \details La méthode fait appel récursivement à chargerCompteReel
   */
  void chargerCompteVirtuel(comptes::AbstractCompte* compte);
  /*!
   * \brief viderTransactions Supprime les transactions chargés en mémoire
   * et vide le vecteur
   * \details La méthode est appelée lorsqu'il faut vider complètement le
   * vecteur de transactions. Cela arrive lorsqu'on change de compte sur l'arbre
   * des comptes, et qu'on doit recharger toutes les transactions. On libère
   * alors la mémoire, et il faut aussi vider le vecteur.
   */
  void viderTransactions();
  /*!
   * \brief chargerTransaction charge en mémoire la transaction passée en
   * paramètre
   * \param transaction la transaction passée en paramètre
   */
  void chargerTransaction(Transaction* transaction);
  /*!
   * \brief chargerTransaction charge en mémoire les transaction passée en
   * paramètre
   * \param transaction la transaction passée en paramètre
   */
  void chargerTransactions(QVector<Transaction*> transactions);

 public:
  /*!
   * \brief isEmpty Cherche à savoir si le TM est vide de
   * transactions/opérations
   * ou non
   * \return TRUE s'il n'y a n'y a pas de transactions.
   * \return FALSE s'il y existe des transactions en mémoire.
   * \details  S'il y n'y a pas de transactions, il ne devrait pas y avoir
   * d'opérations ...
   */
  bool isEmpty();
  /*!
   * \brief getTransactionsByCompte Obtenir toutes les transactions associées à
   * un compte.
   * \details La méthode renvoie toutes les adresses des transactions du compte
   * concerné dans un QVector. On parcourt le vecteur de transactions
   * du TransactionManager pour filtrer toutes les transactions dont
   * la liste des opérations font apparaître le compte.
   * \param compte Référence sur le compte pour lequel on cherche les
   * transactions
   * \return QVector<Transaction*> Retourne un QVector<Transaction*> contenant
   * des pointeurs vers toutes les transactions du compte passé en paramètre
   */
  const QVector<Transaction*> getTransactionsByCompte(
      const comptes::AbstractCompte& compte) const;

  /*!
   * \brief getOperationsByCompteByDate Retourne un vecteur sur les opérations
   * associés au compte passé en paramètre doot la date est antérieure ou égale
   * à la date passée en paramètre
   * \param compte Référence sur le compte pour lequel on souhaite les
   * opérations
   * \param date Date jusqu'à laquelle on souhaite les opérations
   * \return QVector<Operation*> Retourne un QVector des pointeurs sur les
   * opérations
   * liées au compte passé en paramètre et dont la date correspond
   */
  QVector<Operation*> getOperationsByCompteByDate(
      const comptes::AbstractCompte& compte, const QDate& date) const;
  /*!
   * \brief getComptesById Obtenir tous les comptes associés à une transaction.
   * \param transaction La transaction pour laquelle on cherche
   * tous les comptes associés.
   * \details On parcourt la liste des opérations de la transaction
   * et on renvoie un QVector<AbstractCompte> contenant tous les comptes
   * uniques concernés par les opérations de la transaction.
   * \return Retourne un QVector<AbstractCompte*> contenant des pointeurs
   * vers tous les comptes associés à la transaction.
   * \deprecated La méthode n'est pas très utile. Elle n'est pas implémentée.
   */
  QVector<comptes::AbstractCompte*> getComptesById(
      const Transaction& transaction) const;
  /*!
   * \brief ajouterTransaction Ajout d'une transaction dans le Storage
   * \param date La date à laquelle la transaction s'effectue
   * \param reference La référence de la transaction.
   * \param description Une description de la transaction (motif, note, ...)
   * \param operations Un QVector<Operation*> d'adresses d'objets Opérations
   * de la transaction.
   * \details Etant donné la liste des opérations, et les détails de
   * la transaction, on utilise la méthode ajouterTransaction pour écrire
   * dans le Storage la transaction en appelant le StorageManager en
   * passant en paramètre l'objet Transaction crée.
   * \return Transaction* L'adresse de l'objet Transcation est retourné
   * \return nullptr La transaction n'est pas valide
   */
  Transaction* ajouterTransaction(QDate date, QString reference,
                                  QString description,
                                  QVector<Operation*> operations);
  /*!
   * \brief creerTransaction Création d'une transaction sans stockage
   * \param date La date à laquelle la transaction s'effectue
   * \param reference La référence de la transaction.
   * \param description Une description de la transaction (motif, note, ...)
   * \param operations Un QVector<Operation*> d'adresses d'objets Opérations
   * de la transaction.
   * \details La méthode créee un transaction par appel au constructeur de
   * Transaction, stocke celle-ci dans le vecteur propre du TransactionManager
   * puis la retourne. Il est à noter que cette méthode est à appeler que
   * lorsque la transaction est chargée depuis le StorageManager, auquel cas
   * elle n'a pas besoin d'y être restockée.
   * \return Transaction* L'adresse de l'objet Transcation est retourné
   * \return nullptr La transaction n'est pas valide
   */
  Transaction* creerTransaction(QDate date, QString reference,
                                QString description,
                                QVector<Operation*> operations);
  /*!
   * \brief creerOperation Création d'un objet Opération, et ajout d'un
   * pointeur.
   * \param compte La référence au compte concerné
   * \param debit Le
   * montant du débit le cas échéant.
   * \param credit Le montant du crédit le cas
   * échéant.
   * \details La méthode permet la création d'une opération dans le
   * respect des contraintes (non-négativité, existence mutuellement
   * exclusive) et l'application des règles de réajustement si nécessaire.
   * \return Operation* L'adresse de l'objet Opération créé est retourné.
   * \return nullptr L'opération n'est pas valide
   */
  Operation* creerOperation(comptes::AbstractCompte& compte, Monnaie debit,
                            Monnaie credit);
  /*!
   * \brief chargerCompte charge toutes les contenus d'un compte réel ou virtuel
   * \param compte Un compte, virtuel ou réel.
   * \param vider Booléen qui empêche le vidage des transactions si set à false.
   */
  void chargerCompte(comptes::AbstractCompte* compte, bool vider = true);
  /*!
   * \brief charge toutes les transactions de la base de données
   * \details Utilisé lors de la génération de rapports.
   */
  void chargerTous();
  /*!
   * \brief sortTransactions trie le vecteur de transactions dans dans l'ordre
   * croissant des dates
   * \param vec Le vecteur de transactions à trier.
   */
  void sortTransactions(
      QVector<Transaction*> vec,
      comparateurTransactions comp = comparateurTransactions());

  Operation* ChargerOperation(comptes::AbstractCompte& compte, Monnaie debit,
                              Monnaie credit);
  /*!
   * \brief supprimerTransaction Suppression d'une transaction du vecteur
   * transactions
   * \param compte Un pointeur vers la transaction en question
   * \details La méthode va commencer par supprimer les opérations liées a la
   * transaction du vecteur operations. Elle va update le solde des comptes en
   * question
   * en effectuant l'opération inverse (si l'opération était un crédit de 50, il
   * débitera
   * le compte de 50). Puis, il va supprimer la transaction du vecteur
   */
  void supprimerTransaction(Transaction* transaction);

  /*!
   * \brief modifierTransaction Modification d'une transaction dans le storage
   * \param transaction Une référence sur la transaction a modifier
   * \param date La date à laquelle la transaction s'effectue
   * \param reference La référence de la transaction.
   * \param description Une description de la transaction (motif, note, ...)
   * \param operations Un QVector<Operation*> d'adresses d'objets Opérations
   * de la transaction.
   * \details La méthode fait appel a la méthode supprimerTransaction, puis
   * crée une nouvelle transaction a l'aide de la méthode ajouterTransaction
   * \return Transaction* L'adresse de l'objet Transcation est retourné
   * \return nullptr La transaction n'est pas valide
   */
  Transaction* modifierTransaction(Transaction* transaction, QDate date,
                                   QString reference, QString description,
                                   QVector<Operation*> operations);
  /*!
   * \brief getTransaction renvoie un pointeur sur une transaction trouvée a
   * l'aide de ref et date
   * \param date une référence sur date de la transaction
   * \param ref une référence sur la référence de la transaction
   * \return Un pointeur sur la transaction avec cette référence et cette date
   * \return nullptr si la transaction n'existes pas
   * \details La méthode parse toutes les transactions et renvoie la transaction
   * avec les informations
   * correspondantes si elle est trouvée
   */
  Transaction* getTransaction(QString& ref, QDate& date);

  /*!
   * \brief Destructeur de l'objet Transaction Manager.
   * \details Il faut détruire les Transactions et les Opérations liées. On
   * parcourt les vecteurs en entier, et on libère la mémoire de chaque objet.
   */
  ~TransactionManager();
};

#endif  // LO21_TRANSACTIONMANAGER_HPP
