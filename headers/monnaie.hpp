/*!
 * \file monnaie.hpp
 * \author Rémy HUET <utc@remyhuet.fr>
 * \date 2020-04-13
 * \brief Définition de la classe monnaie
 */

#ifndef LO21_MONNAIE_HPP
#define LO21_MONNAIE_HPP

#include <QString>

/*!
 * \class Monnaie monnaie.hpp
 * \brief Classe Monnaie
 * \details Le but de cette classe est de fournir un type d'une précision
 * parfaite pour la manipulation de données monétaires. En effet, les types à
 * virgule flottante (float / double...) ne sont pas adaptés à la représentation
 * de valeurs exactes
 */
class Monnaie {
  /*!
   * \brief Déclaration d'amitié avec le DBManager qui gèrera
   * le stockage physique des données
   */
  friend class DBManager;

 public:
  /*!
   * \brief Constructeur par défaut
   */
  constexpr Monnaie() = default;

  /*!
   * \brief Construit un objet de type Monnaie à partir d'un QString
   * \param m le montant en unités (xxxxx.yy) à partir duquel construire l'objet
   */
  Monnaie(const QString& m);

  /*!
   * \brief Retourne le montant
   * \return le montant en unités (xxxx.yy)
   */
  QString getMontant() const noexcept;

  /*!
   * \brief Fixe le montant d'une certaine valeur à partir d'une chaîne de
   * caractères
   * \param m le montant en unités sous la forme xxxx.yy
   */
  void setMontant(const QString& m);

  // Conversions:

  /*!
   * \brief Conversion en QString du montant
   * \details Cet opérateur permet une conversion implicite du type Monnaie en
   * QString. Le montant est en unités sous la forme xxxxx.yy (tronquée au
   * centime)
   */
  operator QString() const noexcept;

  /*!
   * \brief Conversion implicite de Monnaie en bool
   * \details renvoie true si le montant est différent de 0, false sinon
   */
  constexpr operator bool() const noexcept { return montant != 0; };
  // Opérateurs non binaires

  /*!
   * \brief Ajoute un objet de type Monnaie
   * \param m l'objet à ajouter
   * \return une référence sur l'objet courant
   * \details Ajoute le montant de m au montant de l'objet
   */
  constexpr Monnaie& operator+=(const Monnaie& m) noexcept {
    montant += m.montant;
    return *this;
  };
  /*!
   * \brief Soustrait un objet de type Monnaie
   * \param m l'objet à soustraire
   * \return une référence sur l'objet courant
   * \details soustrait le montant de m au montant de l'objet
   */
  constexpr Monnaie& operator-=(const Monnaie& m) noexcept {
    montant -= m.montant;
    return *this;
  };

  /*!
   * \brief L'opérateur operator< est déclaré friend pour pouvoir accéder aux
   * montants
   * \details cf. implémentation libre dans monnaie.hpp
   */
  constexpr friend bool operator<(const Monnaie& m1,
                                  const Monnaie& m2) noexcept;

 private:
  long long int montant{0};  //!< Le montant en centimes
  /*!
   * \brief Déclaration d'amitié entre Monnaie et DBManager
   * \details La déclaration d'amitié est nécessaire pour que le
   * DBManager puisse créer des objets de type Monnaie en entrant
   * directement le montant en centimes.
   */
  friend class DBManager;
};

// Autres opérateurs
// Opérations

/*!
 * \brief Ajoute deux objets de type Monnaie
 * \param m1
 * \param m2
 * \return un objet Monnaie dont le montant est la somme des montants de m1 et
 * m2
 */
constexpr Monnaie operator+(Monnaie m1, const Monnaie& m2) noexcept {
  m1 += m2;
  return m1;
};
/*!
 * \brief Soustrait deux objets de type Monnaie
 * \param m1
 * \param m2
 * \return un objet Monnaie dont le montant est la soustraction du montant de m2
 * à celui de m1
 */
constexpr Monnaie operator-(Monnaie m1, const Monnaie& m2) noexcept {
  m1 -= m2;
  return m1;
};

// Comparaisons
/*!
 * \brief Compare les montants de deux objets de type Monnaie
 * \param m1
 * \param m2
 * \return true si le montant de m1 est < au montant de m2, false sinon
 */
constexpr bool operator<(const Monnaie& m1, const Monnaie& m2) noexcept {
  return m1.montant < m2.montant;
};
/*!
 * \brief Compare les montants de deux objets de type Monnaie
 * \param m1
 * \param m2
 * \return true si le montant de m1 est > au montant de m2, false sinon
 */
constexpr bool operator>(const Monnaie& m1, const Monnaie& m2) noexcept {
  return m2 < m1;
};
/*!
 * \brief Compare les montants de deux objets de type Monnaie
 * \param m1
 * \param m2
 * \return true si le montant de m1 est == au montant de m2, false sinon
 */
constexpr bool operator==(const Monnaie& m1, const Monnaie& m2) noexcept {
  return !(m1 < m2 || m2 < m1);
};
/*!
 * \brief Compare les montants de deux objets de type Monnaie
 * \param m1
 * \param m2
 * \return true si le montant de m1 est != du montant de m2, false sinon
 */
constexpr bool operator!=(const Monnaie& m1, const Monnaie& m2) noexcept {
  return !(m1 == m2);
};
/*!
 * \brief Compare les montants de deux objets de type Monnaie
 * \param m1
 * \param m2
 * \return true si le montant de m1 est <= au montant de m2, false sinon
 */
constexpr bool operator<=(const Monnaie& m1, const Monnaie& m2) noexcept {
  return !(m1 > m2);
};
/*!
 * \brief Compare les montants de deux objets de type Monnaie
 * \param m1
 * \param m2
 * \return true si le montant de m1 est >= au montant de m2, false sinon
 */
constexpr bool operator>=(const Monnaie& m1, const Monnaie& m2) noexcept {
  return !(m1 < m2);
};

/*!
 * \brief Écrit la valeur d'un objet Monnaie sur un QDataStream
 * \param os le stream sur lequel écrire l'objet Monnaie
 * \param monnaie l'objet Monnaie sur lequel l'opérateur est utilisé
 * \return Renvoie os
 */
QDataStream& operator<<(QDataStream& os, const Monnaie& monnaie);

#endif  // LO21_MONNAIE_HPP
