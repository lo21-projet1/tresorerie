/*!
 * \file compte.hpp
 * \author Bastien BOURÉ
 * \author Rémy HUET <utc@remyhuet.fr>
 * \date 2020-05-23
 * \brief Définition de la casse compte
 */

#ifndef LO21_COMPTE_HPP
#define LO21_COMPTE_HPP

#include "abstractcompte.hpp"

namespace comptes {

/*!
 * \class Compte compte.hpp
 * \brief Classe Compte
 * \details Le but de cette classe est de fournir une interface
 * avec un template pour les comptes réels et virtuels
 */
template <TypeCompte type>
class Compte : public virtual AbstractCompte {
 public:
  virtual TypeCompte getType() const noexcept override final { return type; }
};
}  // namespace comptes

#endif
