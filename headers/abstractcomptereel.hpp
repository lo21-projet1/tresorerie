/*!
 * \file abstractcomptereel.hpp
 * \author Rémy HUET <utc@remyhuet.fr>
 * \date 2020-06-12
 * \brief Définition de l'interface AbstracCompteReel
 */

#ifndef LO21_ABSTRACTCOMPTEREEL_HPP
#define LO21_ABSTRACTCOMPTEREEL_HPP

#include <QDate>
#include <QDebug>

#include "headers/abstractcompte.hpp"
#include "headers/dbmanager.hpp"

namespace comptes {

/*!
 * \brief Interface AbstractCompteReel
 * \class AbstractCompteReel abstractcomptereel.hpp
 */
class AbstractCompteReel : public virtual AbstractCompte {
 public:
  AbstractCompteReel(const QString& n, int i,
                     QDate rapprochement)
      : AbstractCompte(n, i), dernier_rapprochement(rapprochement) {}

  /*!
   *\brief Destructeur par défaut
   */
  virtual ~AbstractCompteReel() = default;

  /*!
   * \brief Indique si le compte est virtuel ou non
   * \return false
   */
  virtual bool isVirtuel() const noexcept override final { return false; }

  /*!
   * \brief Renvoie le solde du compte
   * \return le solde propre du compte réel
   */
  virtual Monnaie getSolde() const override final { return solde; }

  /*!
   * \brief Crédite le compte du montant
   * \param montant le montant à créditer
   */
  virtual void crediter(const Monnaie& montant) = 0;

  /*!
   * \brief débite le compte du montant
   * \param montant le montant à débiter
   */
  virtual void debiter(const Monnaie& montant) = 0;

  /*!
   * \brief rapproche le  compte à la date passé en paramètre
   * \param date du rapprochement
   */
  virtual void rapprocher(QDate const& date);

  /*!
   * \brief Retourne la date du dernier rapprochement
   * \return la date du dernier rapprochement pour ce compte
   */
  inline QDate const& getDernierRapprochement() {
    return dernier_rapprochement;
  }

  /*!
   * \brief Retourne le solde au dernier rapprochement
   * \return le solde au dernier rapprochement
   */
  inline Monnaie getSoldeRapproche() {
    return getSoldeByDate(dernier_rapprochement);
  }

  /*!
   * \brief retourne le solde du compte à la date passée en paramètre
   * \param date à laquelle on souhaite le solde
   * \return le solde à la date indiquée
   */
  virtual Monnaie getSoldeByDate(QDate const& date) const override final;

 protected:
  Monnaie solde{"0,00"};  //< solde du compte
  QDate dernier_rapprochement;

 private:
  friend class CompteFactory;
};
}  // namespace comptes

#endif  // LO21_ABSTRACTCOMPTEREEL_HPP
