/*!
 * \file fenetrerapprochementcompte.hpp
 * \author Bastien Bouré
 * \date 2020-06-09
 * \brief Implémentation de la fenetre pour le rapprochement des comptes
 * elle est générée par QtDesigner l'ensemble des éléments sont donc
 * accessible par le pointeur ui.
 */

#ifndef LO21_RAPPROCHEMENTCOMPTE_HPP
#define LO21_RAPPROCHEMENTCOMPTE_HPP

#include <QDialog>

#include "comptereel.hpp"
#include "ui_fenetrerapprochementcompte.h"

namespace Ui {
class FenetreRapprochementCompte;
}

/*!
 * \class RapprochementCompte rapprochementcompte.hpp
 * \brief Classe de la fenetre Rapprochement Compte
 * \details Cette classe utilise la classe UI::RapprochementCompte auto générée
 * par QtDesigner
 */
class FenetreRapprochementCompte : public QDialog {
  Q_OBJECT

 public:
  /*!
   * \brief Constructeur de la classe, prend le compte à rapprocher
   * en paramètre
   * \param parent le parent de la classe
   * \param _compte le compte à rapprocher
   */
  FenetreRapprochementCompte(QWidget *parent = nullptr,
                             comptes::AbstractCompte *_compte = nullptr);
  /*!
   * \brief Destructeur de la classe
   */
  ~FenetreRapprochementCompte();

 public slots:
  /*!
   * \brief Slot permettant de raffraichir le solde affiché
   * en fonction de la date choisie sur l'interface
   * \param date la nouvelle date choisie
   */
  void rafraichirSolde(QDate const &date);

  /*!
   * \brief Slot permettant de rapprocher le compte à la date
   * choisie
   */
  void on_valider_annuler_clicked(QAbstractButton *);
  /*!
   * \brief Renvoie la date choisie pour le rapprochement
   * \return La date choisie par l'utilisateur de type QDate
   */
  QDate getDateRapprochement();

 private:
  /*!
   * \brief Composant UI
   * \details Ce composant permet l'interface avec le code auto-généré par les
   * outils UI de Qt. C'est par lui que l'on accède aux différents éléments de
   * la fenêtre
   */
  Ui::FenetreRapprochementCompte *ui;

  /*!
   * \brief compte le compte à rapprocher
   */
  comptes::AbstractCompte *compte;
};

#endif  // LO21_RAPPROCHEMENTCOMPTE_HPP
