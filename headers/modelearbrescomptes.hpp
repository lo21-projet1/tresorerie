/*!
 * \file modelearbrescomptes.hpp
 * \author Rémy HUET <utc@remyhuet.fr>
 * \date 2020-05-06
 * \brief Ce fichier définit une classe servant de modèle pour l'arbre des
 * comptes
 */

#ifndef LO21_MODELEARBRECOMPTES_HPP
#define LO21_MODELEARBRECOMPTES_HPP

#include <QStandardItemModel>

/*!
 * \class ModeleArbreCompte modelearbrecomptes.hpp
 * \brief Classe ModeleArbreCompte
 * \details Pour le moment, la seule utilité de cette classe est de permettre un
 * alignement à droite pour la colonne des soldes uniquement, ce qui n'est pas
 * possible autrement.
 */
class ModeleArbreCompte : public QStandardItemModel {
  /*!
   * \brief Utilisation des constructeurs de QStandardItemModel
   */
  using QStandardItemModel::QStandardItemModel;

  /*!
   * \brief Cette fonction est appelée par les vues lors de la récupération de
   * données du modèle
   * \details Elle est override par la classe ModeleArbreCompte afin d'avoir le
   * comportement suivant :
   * - Si la donnée demandée est l'alignement de la colonne 2 (colonne des
   * soldes), elle renvoie Qt::AlignRight
   * - Sinon elle appelle la fonction de la classe mère avec les mêmes arguments
   * \param index l'index de l'élément demandé, permettant de savoir dans quelle
   * colonne il se situe.
   * \param role quelle est la donnée demandée.
   * \return
   * Qt::AlignRight si l'on cherche l'alignement de la colonne des soldes,
   * QStandardItemModel::data(index, role) sinon
   */
  QVariant data(const QModelIndex &index, int role) const override {
    if (index.column() == 2 && role == Qt::TextAlignmentRole)
      return Qt::AlignRight;
    return QStandardItemModel::data(index, role);
  };
};

#endif  // LO21_MODELEARBRECOMPTES_HPP
