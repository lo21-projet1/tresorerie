/*!
 * \file fenetregenerationrapport.hpp
 * \author Bastien Bouré
 * \date 2020-06-17
 * \brief Définition de la fenêtre de génération des rapports
 */

#ifndef FENETREGENERATIONRAPPORT_HPP
#define FENETREGENERATIONRAPPORT_HPP

#include <QDialog>

namespace Ui {
class FenetreGenerationRapport;
}

/*!
 * \class FenetreGenerationRapport fenetregenerationrapport.hpp
 * \brief Classe représentant la fenêtre de génération des rapports
 * \details Cette fenêtre utilise un composant Ui auto-généré par QtDesigner
 */
class FenetreGenerationRapport : public QDialog {
  Q_OBJECT

 public:
  /*!
   * \brief Constructeur de la fenêtre
   * \param parent Widget parent (facultatif)
   */
  explicit FenetreGenerationRapport(QWidget *parent = nullptr);
  ~FenetreGenerationRapport();

 private slots:

  /*!
   * \brief slot on_valider_accepted
   * \details Ce slot utilise les fonctionnalités d'auto-connect de Qt.
   */
  void on_valider_accepted();

  /*!
   * \brief on_G_Bilan_toggled
   * \param arg1
   * \details Ce slot utilise les fonctionnalités d'auto-connect de Qt.
   */
  void on_G_Bilan_toggled(bool arg1);

  /*!
   * \brief on_G_Resultat_toggled
   * \param arg1
   * \details Ce slot utilise les fonctionnalités d'auto-connect de Qt.
   */
  void on_G_Resultat_toggled(bool arg1);

  /*!
   * \brief on_D_Bilan_dateChanged
   * \param date
   * \details Ce slot utilise les fonctionnalités d'auto-connect de Qt.
   */
  void on_D_Bilan_dateChanged(const QDate &date);

  /*!
   * \brief on_D_resultat_dateChanged
   * \param date
   * \details Ce slot utilise les fonctionnalités d'auto-connect de Qt.
   */
  void on_D_resultat_dateChanged(const QDate &date);

 private:
  Ui::FenetreGenerationRapport
      *ui;  //< Contient l'interface de la fenêtre, auto-généré par uic
};

#endif  // FENETREGENERATIONRAPPORT_HPP
