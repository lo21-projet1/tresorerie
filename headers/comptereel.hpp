/*!
 * \file comptereel.hpp
 * \author Rémy HUET <utc@remyhuet.fr>
 * \author Bastien Bouré
 * \date 2020-06-12
 * \brief Déclaration de la classe CompteReel
 */

#ifndef LO21_COMPTEREEL_HPP
#define LO21_COMPTEREEL_HPP

#include "headers/abstractcomptereel.hpp"
#include "headers/compte.hpp"

namespace comptes {

template <TypeCompte type>
class CompteReel : public Compte<type>, public AbstractCompteReel {
 public:
  CompteReel(const QString& nom, int id,
             QDate rapprochement=QDate(2000,01,01))
      : AbstractCompte(nom, id), AbstractCompteReel(nom, id, rapprochement){};

  virtual void crediter(const Monnaie& montant) override final;
  virtual void debiter(const Monnaie& montant) override final;
};

};  // namespace comptes

#endif  // LO21_COMPTEREEL_HPP
