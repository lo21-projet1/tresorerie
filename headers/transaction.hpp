/*!
 * \file transaction.hpp
 * \author Arthur Wacquez <arthur.wacquez@etu.utc.fr>
 * \author Pascal Quach <pascal.quach.utc@gmail.com>
 * \author Bastien Bouré
 * \date 2020-06-09s
 * \brief Implémentation de la classe Transaction
 */

#ifndef LO21_TRANSACTION_HPP
#define LO21_TRANSACTION_HPP

#include <QDate>
#include <QString>
#include <QVector>

#include "operation.hpp"

class Transaction {
 private:
  /*!
   * \brief La date à laquelle s'est déroulée la transaction
   */
  QDate date;
  /*!
   * \brief La référence de la transaction, c'est à dire son nom, son titre,
   * etc.
   */
  QString reference;
  /*!
   * \brief La description de la transaction, ce que l'utilisateur précise.
   *
   */
  QString description;
  /*!
   * \brief Le vecteur de pointers sur les opérations liés à la transaction.
   * \details C'est TransactionManager qui est responsable de la création et
   * destruction des opérations. Lors de la création d'une transaction, une
   * référence vers les opérations sont passées) lors de l'initialisation des
   * transcations d'un compte.
   *
   */
  QVector<Operation*> operations;

  bool rapproche;  //!< Le compte est rapproché où non

  /*!
  * \brief Constructeur d'un nouvel objet Transaction.
  * \details Le constructeur utilisé est celui par défaut. Il sera appelé par
  * la Factory lié, ce qui présuppose une amitié.
  * \param date La date à laquelle la transaction s'est déroulée.
  * \param reference La référence de la transaction.
  * \param description La description de la transaction.
  * \param operations Le vecteur d'operations associés à la transaction. Le
  * vecteur
  * est composée d'un unique élément si la transaction est simple, et de plus
  * d'un si la transaction est répartie.
  */
  Transaction(QDate date, QString reference, QString description,
              QVector<Operation*> operations)
      : date{date},
        reference{reference},
        description{description},
        operations{operations},
        rapproche{false} {};
  /*!
   * \brief Le destructeur par défaut de l'objet Transaction.
   * \details La durée de vie des opérations pointées par opérations est gérée
   * par le TransactionManager. Les pointeurs pointent sur les mêmes adresses
   * que celles stockées dans le TransactionManager.
   */
  ~Transaction() = default;

  // Setteurs
  /*!
   * \brief Modifie la date de la transaction
   * \param date Un objet de type QDate
   */
  inline void setDate(QDate date) { this->date = date; };
  /*!
   * \brief Modifie la reference de la transaction
   * \param ref Un objet de type QString
   */
  inline void setReference(QString ref) { this->reference = ref; };
  /*!
   * \brief Modifie l'objet description.
   * \param desc Un objet de type QString
   */
  inline void setDescription(QString desc) { this->description = desc; };

 public:
  /*!
   * \brief setRapproche Methode permettant d'indiquer la transaction comme
   * étant rapprochée
   */
  inline void setRapproche() noexcept { rapproche = true; }
  /*!
   * \brief Renvoie une référence const vers l'objet date.
   *
   * \return const QDate& Référence const vers l'objet date
   */
  inline const QDate& getDate() const { return date; };
  /*!
   * \brief Renvoie une référence const vers l'objet reference.
   *
   * \return const QString& Référence const vers l'objet reference
   */
  inline const QString& getReference() const { return reference; };
  /*!
   * \brief Renvoie une référence const vers l'objet description.
   *
   * \return const QString& Référence const vers l'objet description.
   */
  inline const QString& getDescription() const { return description; };
  /*!
   * \brief Renvoie une référence const vers l'objet operations.
   *
   * \return const QVector<Operation&> référence const vers le vecteur
   * d'opérations liés à la transaction.
   */
  inline const QVector<Operation*> getOperations() const { return operations; };

  /*!
   * \brief Verifier la validite d'une transaction.
   * \details La fonction verifie que la transaction dispose d'une date,
   * reference et description ainsi que l'équilibre soit respectée (debit =
   * credit) et comporte au minimum 2 opérations : une opération de crédit, et
   * une opération de débit. \return true Si la transaction est valide, et donc
   * qu'aucune modification n'a été effectuée. \return false Si la transaction
   * n'est pas valide.
   */
  bool checkValidite();

  /*!
   * \brief isRapproche renvoie true si la transaction est rapprochée
   */
  inline bool isRapproche() const noexcept { return rapproche; }
  /*!
   * \brief Déclaration d'amitié entre Transaction et TransactionManager.
   */
  friend class TransactionManager;
};

#endif  // LO21_TRANSACTION_HPP
