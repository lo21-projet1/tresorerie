/*!
 * \file qcomboboxdelegate.hpp
 * \author Pascal Quach
 * \date 2020-06-10
 * \brief Déclaration de la classe QComboBoxDelegate
 */

#ifndef QCOMBOBOXDELEGATE_HPP
#define QCOMBOBOXDELEGATE_HPP

#include <QComboBox>
#include <QStyledItemDelegate>

/*!
 * \class QComboBoxDelegate qcomboboxdelegate.hpp
 * \brief Cette classe permet d'utiliser des ComboBox dans des vues telles qu'un
 * QTableView
 */
class QComboBoxDelegate : public QStyledItemDelegate {
  Q_OBJECT
 public:
  /*!
   * \brief QComboBoxDelegate Constructeur
   * \param parent Le parent duquel on délègue
   * \details QStyledItemDelegate fait partie du framework modèle/vues de Qt.
   */
  QComboBoxDelegate(QObject *parent = 0) : QStyledItemDelegate(parent) {}
  /*!
   * \brief ~QComboBoxDelegate Destructeur par défaut
   */
  ~QComboBoxDelegate() = default;
  /*!
   * \brief createEditor va créer et retourner un pointeur sur le ComboBox
   * \param parent Le parent dans lequel est inscrusté le ComboBox
   * \param option Les options du modèle
   * \param index L'index dans le modèle
   * \return Un pointeur sur le ComboBox
   */
  QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option,
                        const QModelIndex &index) const;
  /*!
   * \brief setEditorData permet de rentrer des données dans le ComboBox
   * \param editor L'objet dans lequel on rentre les données
   * \param index L'index dans le modèle
   * \details On récupère l'objet sous la forme d'un QWidget, qu'on cast à
   * l'aide de qobject_cast en un pointeur sur le type de widget voulu. En
   * l'occurence, on veut ici un QComboBox*
   */
  void setEditorData(QWidget *editor, const QModelIndex &index) const;
  /*!
   * \brief setModelData permet la mise à jour des données dans le modèle
   * \param editor L'objet duquel on prélève les données
   * \param model Le modèle dans lequel on rentre les données
   * \param index L'index actuel dans le modèle
   */
  void setModelData(QWidget *editor, QAbstractItemModel *model,
                    const QModelIndex &index) const;
};

#endif  // QCOMBOBOXDELEGATE_HPP
