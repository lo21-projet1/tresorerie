/*!
 * \file comptevirtuel.hpp
 * \author Bastien BOURÉ
 * \author Rémy HUET <utc@remyhuet.fr>
 * \date 2020-06-11
 * \brief Définition de la casse compte virtuel
 */

#ifndef LO21_COMPTEVIRTUEL_HPP
#define LO21_COMPTEVIRTUEL_HPP

#include <QVector>
#include <memory>

#include "headers/abstractcomptereel.hpp"
#include "headers/abstractcomptevirtuel.hpp"
#include "headers/compte.hpp"

namespace comptes {
/*!
 * \class CompteVirtuel comptevirtuel.hpp
 * \brief Classe CompteVirtuel
 * \details Un compte virutel est un compte qui n'a pas de solde propre, mais
 * peut avoir des sous-comptes.
 */
template <TypeCompte type>
class CompteVirtuel : public Compte<type>, public AbstractCompteVirtuel {
 public:
  /*!
   * \brief Construit un objet CompteVirtuel
   * \param n le nom du compte à créer
   * \param i l'id du compte
   */
  CompteVirtuel(const QString& n, int i) : AbstractCompte(n, i){};

  /*!
   * \brief Renvoie le solde du compte
   * \return le colde du compte sous la forme d'un objet Monnaie
   * \details Le solde d'un compte virtuel est défini comme la somme des soldes
   * de ses sous-comptes
   */
  virtual Monnaie getSolde() const override final {
    Monnaie s;
    int i = 0;
    for (auto c : sous_comptes) {
      s += c->getSolde();
      i++;
    }
    qDebug() << "CompteVirtuel::getSolde : " << s.getMontant() << " // " << i;
    return s;
  }

  /*!
   * \brief Récupère les sous compte du compte
   * \return une référence constante sur la liste des sous comptes du compte
   */
  const auto& getSousCompte() { return sous_comptes; }

  /*!
   * \brief addSousCompte
   * \param compte un pointeur vers le compte à ajouter à la liste des
   * sous-comptes.
   * \details le sous compte à ajouter doit être de même type que le compte
   */
  inline void addSousCompte(Compte<type>* compte) {
    sous_comptes.push_back(compte);
  }
  /*!
   * \brief Récupère les compte fils
   * \return Un vecteur de compte fils sous la forme de AbstractCompte*
   */
  virtual QVector<AbstractCompte*> getCompteFilsAbstract()
      const override final {
    QVector<AbstractCompte*> v;
    for (auto compte : sous_comptes) {
      v.push_back(dynamic_cast<AbstractCompte*>(compte));
    }
    return v;
  }

  /*!
   * \brief Retourne le solde à la date donnée
   * \param date à laquelle on veut le solde du compte
   * \return le solde à la date donnée
   */
  virtual Monnaie getSoldeByDate(QDate const& date) const override final {
    Monnaie solde;
    for (auto compte : sous_comptes) {
      if (dynamic_cast<AbstractCompte*>(compte)->isVirtuel()) {
        solde +=
            dynamic_cast<AbstractCompteVirtuel*>(compte)->getSoldeByDate(date);
      } else {
        solde +=
            dynamic_cast<AbstractCompteReel*>(compte)->getSoldeByDate(date);
      }
    }
    return solde;
  }

 private:
  QVector<Compte<type>*> sous_comptes;  //!< La liste des sous comptes
                                        //!< associés à ce compte
};

}  // namespace comptes
#endif
