/*!
 * \file operation.hpp
 * \author Arthur Wacquez <arthur.wacquez@etu.utc.fr>
 * \author Pascal Quach <pascal.quach.utc@gmail.com>
 * \author Rémy Huet <utc@remyhuet.fr>
 * \date 2020-05-22
 * \brief Implémentation de la classe Operation
 */

#ifndef LO21_OPERATION_HPP
#define LO21_OPERATION_HPP

#include "headers/abstractcompte.hpp"
#include "headers/monnaie.hpp"

class Operation {
 private:
  /*!
   * \brief Debit de l'operation
   */
  Monnaie debit;
  /*!
   * \brief Credit de l'operation
   */
  Monnaie credit;
  /*!
   * \brief Reference const sur le compte dont dépend l'operation
   */
  comptes::AbstractCompte& compte_lie;
  /*!
   * \brief Constructeur d'Opération
   * \param debit Montant de type Monnaie correspondant au débit de l'opération
   * \param credit Montant de type Monnaie correspondant au crédit de
   * l'opération
   * \param compte_lie Référence au compte lié à l'opération
   */
  Operation(Monnaie debit, Monnaie credit, comptes::AbstractCompte& compte_lie)
      : debit{debit}, credit{credit}, compte_lie{compte_lie} {};
  /*!
   * \brief Destructeur d'Opération par défaut
   */
  ~Operation() = default;

 public:
  /*!
   * \brief Renvoie une référence vers l'objet Compte.
   * \return Compte&  Renvoie une référence sur la référence
   * du compte lié à l'opération
   * \details Méthode qui renvoie une référence non const. A utiliser par les
   * Manager.
   * \warning Elle devrait être privée. Déclaration d'amitié à faire.
   */
  inline comptes::AbstractCompte& getCompte() { return compte_lie; };
  /*!
   * \brief Renvoie une référence const vers l'objet Compte.
   * \return const Compte&  Renvoie une référence const sur la référence const
   * du compte lié à l'opération
   */
  inline const comptes::AbstractCompte& getCompte() const {
    return compte_lie;
  };
  /*!
   * \brief Renvoie la valeur du debit de l'operation
   * \return const Monnaie& Renvoie une référence const sur l'objet debit de
   * type Monnaie
   */
  inline const Monnaie& getDebit() const { return debit; };
  /*!
   * \brief Renvoie la valeur du credit de l'operation
   * \return const Monnaie& Renvoie une référence const sur l'objet credit de
   * type Monnaie
   */
  inline const Monnaie& getCredit() const { return credit; };
  /*!
  * \brief Fonction appelée lors de checkValidite en cas d'erreur pour
  corriger la validité de la transaction
  * \details Si une transaction possède a la fois un debit et un credit (elle
  n'est donc pas valide), on utilise getMin() afin de déterminer le minimum
  des deux, et le soustraire au debit et au credit, corrigeant ainsi l'erreur
  * \return const Monnaie& Renvoie une référence const sur le minimum de debit
  et crédit, les deux de type Monnaie
  */
  inline const Monnaie& getMin() const {
    return (credit <= debit ? credit : debit);
  };
  /*!
   * \brief Modifie la valeur du debit de l'operation
   * \param montant Un objet de type Monnaie
   */
  inline void setDebit(Monnaie montant) { this->debit = montant; };
  /*!
   * \brief Modifie la valeur du credit de l'operation
   * \param montant Un objet de type Monnaie
   */
  inline void setCredit(Monnaie montant) { this->credit = montant; };
  /*!
  * \brief Verifier la validite d'une operation
  * \details La fonction vérifie que l'opération dispose d'une reference vers un
  compte, et qu'on l'a debit XOR credit. Elle fait appel a getMin() pour
  corriger les erreurs liées au credit/debit
  * \return true Si l'opération n'a pas été modifiée
  * \return false Si l'opération a été modifiée
  */
  bool checkValidite();
  /*!
   * \brief Déclaration d'amitié entre Opération et TransactionManager.
   */
  friend class TransactionManager;
  // TODO : amitié Opération CM ?
};
#endif  // LO21_OPERATION_HPP
