/*!
 * \file abstractcomptevirtuel.hpp
 * \author Rémy HUET <utc@remyhuet.fr>
 * \date 2020-06-16
 * \brief Définition de l'interface AbstractCompteVirtuel
 */

#ifndef LO21_ABSTRACTCOMPTEVIRTUEL_HPP
#define LO21_ABSTRACTCOMPTEVIRTUEL_HPP

#include <memory>

#include "headers/abstractcompte.hpp"

namespace comptes {

/*!
 * \brief Interface AbstractCompteVirtuel
 * \class AbstractCompteVirtuel abstractcomptevirtuel.hpp
 */
class AbstractCompteVirtuel : public virtual AbstractCompte {
 public:
  /*!
   * \brief Destructeur par défaut
   */
  virtual ~AbstractCompteVirtuel() = default;

  /*!
   * \brief Indique si le compte est virtuel ou non
   * \return true
   */
  virtual bool isVirtuel() const noexcept override final { return true; }
  /*!
   * \brief Récupère les compte fils
   * \return Un vecteur de compte fils sous la forme de AbstractCompte*
   */
  virtual QVector<AbstractCompte*> getCompteFilsAbstract() const = 0;

  /*!
   * \brief Retourne le solde à la date donnée
   * \param date à laquelle on veut le solde du compte
   * \return le solde à la date donnée
   */
  virtual Monnaie getSoldeByDate(QDate const& date) const override = 0;
};
}  // namespace comptes

#endif  // LO21_ABSTRACTCOMPTEVIRTUEL_HPP
