/*!
 * \file qdoublespinboxdelegate.hpp
 * \author Pascal Quach
 * \date 2020-06-17
 * \brief Déclaration de la classe QDoubleSpinBoxDelegate
 */

#ifndef QDOUBLESPINBOXDELEGATE_HPP
#define QDOUBLESPINBOXDELEGATE_HPP

#include <QComboBox>
#include <QStyledItemDelegate>

/*!
 * \brief Cette classe permet d'utiliser des QDoubleSpinBox dans des vues Qt
 * telles que QTableView
 * \class QDoubleSpinBoxDelegate qdoublespinboxdelegate.hpp
 */
class QDoubleSpinBoxDelegate : public QStyledItemDelegate {
  Q_OBJECT
 public:
  /*!
   * \brief QDoubleSpinBoxDelegate Constructeur
   * \param parent Le parent duquel on délègue
   * \details QStyledItemDelegate fait partie du framework modèle/vues de Qt.
   */
  QDoubleSpinBoxDelegate(QObject *parent = 0) : QStyledItemDelegate(parent) {}
  /*!
   * \brief ~QDoubleSpinBoxDelegate Destructeur par défaut
   */
  ~QDoubleSpinBoxDelegate() = default;
  /*!
   * \brief createEditor va créer et retourner un pointeur sur le QDoubleSpinBox
   * \param parent Le parent dans lequel est inscrusté le QDoubleSpinBox
   * \param option Les options du modèle
   * \param index L'index dans le modèle
   * \return Un pointeur sur le QDoubleSpinBox
   */
  QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option,
                        const QModelIndex &index) const;
  /*!
   * \brief setEditorData permet de rentrer des données dans le QDoubleSpinBox
   * \param editor L'objet dans lequel on rentre les données
   * \param index L'index dans le modèle
   * \details On récupère l'objet sous la forme d'un QWidget, qu'on cast à
   * l'aide de qobject_cast en un pointeur sur le type de widget voulu. En
   * l'occurence, on veut ici un QComboBox*
   */
  void setEditorData(QWidget *editor, const QModelIndex &index) const;
  /*!
   * \brief setModelData permet la mise à jour des données dans le modèle
   * \param editor L'objet duquel on prélève les données
   * \param model Le modèle dans lequel on rentre les données
   * \param index L'index actuel dans le modèle
   */
  void setModelData(QWidget *editor, QAbstractItemModel *model,
                    const QModelIndex &index) const;
};

#endif  // QDOUBLESPINBOXDELEGATE_HPP
