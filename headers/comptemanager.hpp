/*!
 * \file comptemanager.hpp
 * \author Bastien BOURÉ
 * \author Pascal QUACH <pascal.quach.utc@gmail.com>
 * \author Rémy HUET <utc@remyhuet.fr>
 * \date 2020-06-14
 * \brief Définition de la classe CompteManager
 */

#ifndef LO21_COMPTEMANAGER_HPP
#define LO21_COMPTEMANAGER_HPP

#include <QDate>
#include <QDebug>
#include <QString>
#include <QVector>
#include <QDir>

#include "abstractcompte.hpp"
#include "compte.hpp"
#include "comptefactory.hpp"
#include "singleton.hpp"
#include "storagemanager.hpp"

namespace comptes {
/*!
 * \class CompteManager comptemanager.hpp
 * \brief La classe CompteManager à pour but de référencer et de garder
 * en mémoire les comptes actuellement charger et de gérer l'accès aux
 * comptes
 * \details La classe ComptaManager pourra créer des comptes et va gérer
 * le lien et la cohérence avec la base de données
 */
class CompteManager : public patterns::Singleton<CompteManager> {
 public:
  /*!
   * \brief Initialise les comptes de la base de donnée en mémoire.
   * Cette méthode est à appeller à chaque ouverture de nouveau fichier.
   */
  void Init();

  /*!
   * \brief Retourne le nom de l'association
   * \return le nom_association
   */
  inline QString getNomAssociation() const { return nom_association; }

  /*!
   * \brief Retourne sous forme de QVector de pointeurs comptes chargés en
   * mémoire \return un QVector de AbstractCompte* comprenant tout les comptes
   * chargés
   */
  inline QVector<comptes::AbstractCompte*> getComptes() const {
    return comptes_charges;
  }

  /*!
   * \brief Retourne sous forme de QVector de AbstractCompte*
   * les comptes chargés en mémoire dont le nom correspond à nom
   * \param nom indique le nom des comptes recherchés
   * \return un QVector comprenant tout les comptes chargés dont
   * le nom correspond
   * \deprecated Nom considéré unique, plusieurs comptes ne peuvent plus
   * avoir le même nom. Préferer AbstractCompte* getCompte(Qstring).
   */
  QVector<comptes::AbstractCompte*> getComptes(QString const& nom) const;

  /*!
   * \brief Retourne un AbstractCompte* du compte dont le
   * nom correspond qu'il se trouve en mémoire
   * \param nom indique le nom du compte recherché
   * \return un pointeur sur le compte trouvé ou nullptr si le
   * compte n'existe pas
   * \warning Attention, le nom d'un compte n'est pas unique. La méthode
   * retourne le premier compte trouvé.
   */
  comptes::AbstractCompte* getCompte(QString const& nom);

  /*!
   * \brief Retourne un AbstractCompte* du compte dont l'id
   * correspond qu'il se trouve en mémoire
   * \param id indique l'id du compte recherché
   * \return un pointeur sur le compte trouvé ou nullptr si le
   * compte n'existe pas
   */
  comptes::AbstractCompte* getCompte(int id);
  /*!
   * \brief getIdsComptesReels Renvoie une liste des ids des comptes réels
   * chargés en mémoire
   * \return Une référence sur un QVector<int> contenant tous les ids des
   * comptes réels
   */
  QVector<int> getIdsComptesReels();

  /*!
   * \brief getNomsComptesReels Renvoie une liste des noms des comptes réels
   * chargés en mémoire
   * \return Une référence sur une QStringList contenant tous les noms des
   * comptes réels
   */
  QStringList getNomsComptesReels();
  /*!
   * \brief getNomsComptesFilsReelsCompteVirtuel renvoie une liste des noms des
   * comptes fils REELS chargés en mémoire dont l'ascendance contient le compte
   * virtuel passé en paramètre
   * \param compte_virtuel Le compte virtuel dont on cherche les noms des fils
   * REELS
   * \return La liste des noms des comptes fils REELS du compte virtuel
   */
  QStringList getNomsComptesFilsReelsCompteVirtuel(
      AbstractCompte* compte_virtuel);
  /*!
   * \brief getNomsComptesFilsReelsCompteVirtuel renvoie une liste des noms des
   * comptes fils REELS chargés en mémoire dont l'ascendance contient le compte
   * virtuel passé en paramètre
   * \param compte_virtuel Le compte virtuel dont on cherche les noms des fils
   * REELS
   * \return La liste des noms des comptes fils REELS du compte virtuel
   */
  QStringList getNomsComptesFilsReelsCompteVirtuel(
      AbstractCompte const* compte_virtuel);

  /*!
   * \brief getComptesRoot Renvoie la liste des comptes racines
   * \return QVector<AbstractCompte*> des comptes racines
   */
  inline QVector<AbstractCompte*> getComptesRoot() const { return root; }
  /*!
   * \brief Indique le nom de l'association utilisant l'application
   * \param nom indique le nom de l'association
   */
  inline void setNomAssociation(QString const& nom) { nom_association = nom; }

  /*!
   * \brief Crée un compte en déduisant son type de celui de son parent et
   * l'ajoute à la base de donnée
   * \param nom le nom à attribuer au compte
   * \param parent le compte parent, qui donnera le type du compte créé
   * \param virtuel indique si le compte créé doit être virtuel ou non
   * \param solde un solde initial peut être donné à un compte non viruel
   * \param compteSInitial pointeur vers le compte de passif duquel vient le
   * solde initial
   * \param dateSInitial la date associée à la transaction réglant le solde
   * initial du compte
   * \param dateDernierRapprochement la date du dernier rapprochement pour
   * les comptes réels
   * \return un pointeur vers le nouveau compte ainsi créé
   *
   * \details La fonction utilise le type du parent pour créer un compte avec
   * les informations données. Ensuite, elle ajoute le compte créé à la liste
   * des fils du compte parent.
   * Si le solde initial est différent de 0, les attributs compteSInitial et
   * dateSInitial doivent obligatoirement être renseignés.
   * Si une date de dernier rapprochement est renseignée, l'attribut virtuel
   * doit obligatoirement être à false
   * L'id du compte est demandé au StorageManager
   */
  AbstractCompte* creerCompte(
      const QString& nom, AbstractCompte& parent, bool virtuel,
      const Monnaie& solde = Monnaie("0,00"),
      Compte<PASSIFS>* compteSInitial = nullptr,
      const QDate& dateSInitial = QDate(),
      const QDate& dateDernierRapprochement = QDate(2000,01,01));

  /*!
   * \brief Crée un compte d'un type donné l'ajoute aux comptes connus et
   * l'ajoute à la base de données
   * \details On ne peut pas donner de parent à un Compte dont on force le type
   * Si le solde initial est différent de 0, les attributs compteSInitial et
   * dateSInitial doivent obligatoirement être renseignés
   * \param nom le nom à attribuer au compte
   * \param type le type du Compte
   * \param virtuel indique si le compte créé doit être virtuel ou non
   * \param solde un solde initial peut être donné à un compte non virtuel
   * \param compteSInitial pointeur vers le compte de passif duquel vient le
   * solde initial
   * \param dateSInitial la date associée à la transaction réglant le solde
   * initial du compte
   *
   * \return un pointeur vers le nouveau compte ainsi créé
   */
  AbstractCompte* creerCompte(
      const QString& nom, TypeCompte type, bool virtuel,
      const Monnaie& solde = Monnaie("0,00"),
      Compte<PASSIFS>* compteSInitial = nullptr,
      const QDate& dateSInitial = QDate(),
      const QDate& dateDernierRapprochement = QDate(2000,01,01));

  /*!
   * \brief Crée un compte du type donné avec un id donné et l'ajoute à la
   * base de données
   * \param id l'identifiant unique du compte
   * \param nom le nom à attribuer au compte
   * \param type le type du compte
   * \param virtuel indique si le compte créé doit être virtuel ou non
   * \param solde un solde initial peut être donné à un compte non virtuel
   * \param compteSInitial pointeur vers le compte de passif duquel vient le
   * solde initial
   * \param dateSInitial la date associée à la transaction réglant
   * le solde initial du compte
   * \param dateDernierRapprochement la date du dernier rapprochement pour
   * les comptes réels
   * \return un pointeur vers le nouveau compte ainsi créé
   * \details On ne peut pas donner de parent à un Compte dont on force le type
   * Si le solde initial est différent de 0, les attributs compteSInitial et
   * dateSInitial doivent obligatoirement être renseignés
   * Si une date de dernier rapprochement est renseignée, l'attribut virtuel
   * doit obligatoirement être à false
   */
  AbstractCompte* creerCompte(
      unsigned int id, const QString& nom, TypeCompte type, bool virtuel,
      const Monnaie& solde = Monnaie("0,00"),
      Compte<PASSIFS>* compteSInitial = nullptr,
      const QDate& dateSInitial = QDate(),
      const QDate& dateDernierRapprochement = QDate(2000,01,01));

  /*!
   * \brief Crée un compte avec un parent et un id donnés l'ajoute aux comptes
   * connus et \param id l'identifiant unique du compte \param nom le nom à
   * attribuer au compte \param parent le compte parent, qui donnera le type du
   * compte créé \param virtuel indique si le compte est virtuel ou non \param
   * solde un solde initial peut être donné à un compte non virtuel \param
   * compteSInitial pointeur vers le compte de passif duquel vient le solde
   * initial \param dateSInitial la date associée à la transation réglant le
   * solde initial du compte \param dateDernierRapprochement la date du dernier
   * rapprochement pour les comptes réels \return un pointeur vers le nouveau
   * compte ainsi créé$ \details La fonction utilise le type du parent pour
   * créer un compte avec les informations données. Ensuite, elle ajoute le
   * compte créé à la liste des fils du compte parent. Si le solde initial est
   * différent de 0, les attributs compteSInitial et dateSInitial doivent
   * obligatoirement être renseignés. Si une date de dernier rapprochement est
   * renseignée, l'attribut virtuel doit obligatoirement être à false
   */
  AbstractCompte* creerCompte(
      unsigned int id, const QString& nom, AbstractCompte& parent, bool virtuel,
      const Monnaie& solde = Monnaie("0,00"),
      Compte<PASSIFS>* compteSInitial = nullptr,
      const QDate& dateSInitial = QDate(),
      const QDate& dateDernierRapprochement = QDate(2000,01,01));
  /*!
   * \brief debiterCompte débite le compte du montant, tous deux passés en
   * paramètre.
   * \param id_compte l'id du compte dont il faut débiter un montant
   * \param debit le montant qu'il faut débiter au compte
   */
  void debiterCompte(comptes::AbstractCompte* c, Monnaie debit);
  /*!
   * \brief crediterCompte crédite le compte du montant, tous deux passés en
   * paramètre.
   * \param id_compte l'id du compte dont il faut créditer un montant
   * \param credit le montant qu'il faut crébiter au compte
   */
  void crediterCompte(comptes::AbstractCompte* c, Monnaie credit);
  /*!
   * \brief Supprime tout les comptes chargés, méthode à appeller en fin de
   * programme
   */
  void supprimerComptes();

  /*!
   * \brief Destructeur de CompteManager appelant supprimerComptes
   * qui supprime tout les comptes chargés
   */
  ~CompteManager() { supprimerComptes(); }

  /*!
   * \brief Permet de cloturer le livre de comptes
   */
  void cloturer();
  /*!
   * \brief pathStuff
   * \details Création dossier etc.
   */
  void pathStuff();
  /*!
   * \brief Génère le bilan du livre de comptes
   */
  void genererBilan(const QDate& date,
                    const QString& filename =
                        "Bilan-" + QDate::currentDate().toString("dd-MM-yyyy"));

  /*!
   * \brief Génère le résultat du livre de comptes
   */
  void genererResultat(
      const QDate& date,
      const QString& filename = "Bilan-" +
                                QDate::currentDate().toString("dd-MM-yyyy"));
  /*!
   * \brief updateComptes Mise à jour des soldes des comptes selon la
   * transaction.
   * \param tr La transaction depuis laquelle modifier les soldes des comptes
   * \param suppression Booléen qui "renverse" la mise à jour. Annule l'effet
   * d'une transaction.
   */
  void updateComptes(Transaction const* tr, bool suppression = false);
  /*!
   * \brief isEmpty renvoie si les comptes chargés en mémoire sont vide.
   * \return TRUE s'il n'y a pas de comptes en mémoire
   * \return FALSE s'il y a des comptes en mémoire
   */
  bool isEmpty();

 private:
  /*!
   * \brief getComptesReels renvoie un vecteur contenant tous les comptes réels
   * chargés en mémoire
   * \return QVector<comptes::AbstractCompte*>
   */
  QVector<comptes::AbstractCompte*> getComptesReels();

  friend class patterns::Singleton<CompteManager>;
  /*!
   * \brief Renvoie un QVector contenant tout les sous fils du compte
   * passé en paramètre, même les sous fils de ses fils
   * \param pere le compte virtuel dont on veut les fils
   * \return QVector<AbstractCompte*> un vecteur contenant la totalité
   * des compte dérivant de pere.
   */
  void InitFils(comptes::AbstractCompte* pere);

  /*!
   * \brief Le compte passé en paramètre est root ou non
   * \param c le compte testé
   * \return true si le compte est root false sinon
   */
  bool isRoot(comptes::AbstractCompte* c) const;

  /*!
   * \brief Forme une chaine QString au format HTML de l'arbre des comptes
   * \param c le compte racine dont il faut afficher l'arbre
   * \param date la date à laquelle afficher les soldes
   * \param profondeur la profondeur actuelle de l'affichage pour les
   * tabulations
   * \return QString la chaine de caractères formatée
   */
  QString arbreCompteToHtml(comptes::AbstractCompte* c, QDate const& date,
                            int profondeur = 0) const;

  /*!
   * \brief Constructeur par défaut de CompteManager
   */
  CompteManager() = default;

  QString nom_association;  //!< le nom de l'association
  QVector<comptes::AbstractCompte*>
      comptes_charges;            //!< la liste des comptes chargés en mémoire
  QVector<AbstractCompte*> root;  //!< les comptes à la racine
};
}  // namespace comptes

#endif
