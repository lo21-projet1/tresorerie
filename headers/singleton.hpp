/*!
 * \file singleton.hpp
 * \author Rémy HUET <utc@remyhuet.fr>
 * \date 2020-04-17
 * \brief Implémentation du Design Pattern Singleton
 * \details Cette classe Singleton utilise les possibilité offertes par les
 * pointeurs intelligens, notamment les shared_ptr pour la destruction
 * automatique de l'instance à la fin du programme.
 * Les shared_ptr représentent une propriété partagée d'un objet.
 * L'ensemble des utilisateurs de la classe récupèrent un shared_ptr vers
 * l'instance.
 * Pour plus de détails sur l'utilisation, voir la page dédiée aux design
 * patterns : @ref patterns
 */

#ifndef LO21_SINGLETON_HPP
#define LO21_SINGLETON_HPP

#include <memory>

namespace patterns {

/*!
 * \class Singleton singleton.hpp
 * \tparam C la classe héritant de Singleton<C>
 * \brief Propose une implémentation de la classe Singleton
 * \details Pour être un singleton, la classe Fille doit hériter de
 * Singleton<Fille>, et déclarer cette classe comme amie.
 */
template <class C>
class Singleton {
 protected:
  Singleton() = default;

 public:
  Singleton(const Singleton&) = delete;
  Singleton& operator=(const Singleton&) = delete;

  /*!
   * \brief getInstance
   * \return renvoie l'instance unique de la classe Singleton.
   */
  static std::shared_ptr<C> getInstance() {
    static std::shared_ptr<C> instance(new C);

    return instance;
  }
};

};  // namespace patterns

#endif  // LO21_SINGLETON_HPP
