/*!
 * \file FenetreAjouterTransaction.hpp
 * \author Arthur WACQUEZ <arthur.wacquez@etu.utc.fr>
 * \author Rémy HUET <utc@remyhuet.fr>
 * \date 2020-06-13
 * \brief Déclaration de la classe FenetreCreationTransactuin
 * \details Cette classe correspond au formulaire de création d'une transaction.
 * Elle est générée grâce à QtDesigner. L'ensembles des éléments associés est
 * donc accessible par le pointeur ui.
 */

#ifndef LO21_FENETREAJOUTERTRANSACTION_HPP
#define LO21_FENETREAJOUTERTRANSACTION_HPP

#include <qdebug.h>

#include <QComboBox>
#include <QDateEdit>
#include <QDialog>
#include <QLabel>
#include <QLineEdit>
#include <QTableWidget>

#include "headers/abstractcompte.hpp"
#include "headers/comptemanager.hpp"
#include "headers/operation.hpp"
#include "headers/transaction.hpp"
namespace Ui {
class FenetreAjouterTransaction;
}

/*!
 * \class FenetreAjouterTransaction FenetreAjouterTransaction.hpp
 * \brief Déclaration de la classe FenetreAjouterTransaction
 * \details Cette classe utilise la classe Ui::FenetreAjouterTransaction, auto
 * générée par QtDesigner
 */
class FenetreAjouterTransaction : public QDialog {
  Q_OBJECT

 protected:
  /*!
   * \brief Composant UI
   * \details Ce composant permet l'interface avec le code auto-généré par
   * les outils UI de Qt. C'est par lui que l'on accède aux différents
   * éléments de la fenêtre
   */
  Ui::FenetreAjouterTransaction* ui;

 public:
  /*!
   * \brief Constructeur de la classe FenetreAjouternTransaction
   * \param parent pointeur vers le widget parent.
   */
  explicit FenetreAjouterTransaction(QWidget* parent = nullptr);
  /*!
   * Destructeur de la classe FenetreCreationCompte
   */
  ~FenetreAjouterTransaction();
  /*!
   * \brief getReference renvoie un pointeur
   * sur l'objet contenant la référence de la transaction
   * \return Un pointeur sur un objet QLineEdit
   */
  QLineEdit* getLineEditReference();
  /*!
   * \brief getReference renvoie un pointeur
   * sur l'objet contenant la référence de la transaction
   * \return Un pointeur sur un objet QLineEdit
   */
  QLineEdit* getLineEditReference() const;
  /*!
   * \brief getDateTransaction renvoie un pointeur sur l'objet contenant
   * la date de la transaction.
   * \return Un pointeur sur un objet QDateEdit
   */
  QDateEdit* getQDateTransaction();
  /*!
   * \brief getDateTransaction renvoie un pointeur sur l'objet contenant
   * la date de la transaction.
   * \return Un pointeur sur un objet QDateEdit
   */
  QDateEdit* getQDateTransaction() const;
  /*!
   * \brief getDescription renvoie un pointeur sur l'objet contenant
   * la description de la transaction
   * \return Un pointeur sur un objet QLineEdit
   */
  QLineEdit* getLineEditDescription();
  /*!
   * \brief getDescription renvoie un pointeur sur l'objet contenant
   * la description de la transaction
   * \return Un pointeur sur un objet QLineEdit
   */
  QLineEdit* getLineEditDescription() const;
  /*!
   * \brief getListeOperations renvoie un pointeur sur
   * l'objet contenant la liste des opérations de la transaction.
   * \return Un pointeur sur un objet QTableWidget
   */
  QTableWidget* getListeOperations();
  /*!
   * \brief getListeOperations renvoie un pointeur sur
   * l'objet contenant la liste des opérations de la transaction.
   * \return Un pointeur sur un objet QTableWidget
   */
  QTableWidget* getListeOperations() const;
  /*!
   * \brief getLabelIdCompte renvoie un pointeur sur
   * l'objet contenant l'ID du compte sélectionné
   * dans la liste des comptes.
   * \return Un pointeur sur un objet QLabel
   */
  QLabel* getLabelIdCompte();
  /*!
   * \brief getLabelIdCompte renvoie un pointeur sur
   * l'objet contenant l'ID du compte sélectionné
   * dans la liste des comptes.
   * \return Un pointeur sur un objet QLabel
   */
  QLabel* getLabelIdCompte() const;
  /*!
   * \brief getLabelIdCompte renvoie un pointeur sur
   * l'objet contenant le type du compte sélectionné
   * dans la liste des comptes.
   * \return Un pointeur sur un objet QLabel
   */
  QLabel* getLabelTypeCompte();
  /*!
   * \brief getLabelIdCompte renvoie un pointeur sur
   * l'objet contenant le type du compte sélectionné
   * dans la liste des comptes.
   * \return Un pointeur sur un objet QLabel
   */
  QLabel* getLabelTypeCompte() const;
  /*!
   * \brief getLabelIdCompte renvoie un pointeur sur
   * l'objet contenant une indication de si
   * le compte est virtuel ou non sélectionné
   * dans la liste des comptes.
   * \return Un pointeur sur un objet QLabel
   */
  QLabel* getLabelVirtuelCompte();
  /*!
   * \brief getLabelIdCompte renvoie un pointeur sur
   * l'objet contenant une indication de si
   * le compte est virtuel ou non sélectionné
   * dans la liste des comptes.
   * \return Un pointeur sur un objet QLabel
   */
  QLabel* getLabelVirtuelCompte() const;
  /*!
   * \brief getLabelIdCompte renvoie un pointeur sur
   * l'objet contenant le solde du compte sélectionné
   * dans la liste des comptes.
   * \return Un pointeur sur un objet QLabel
   */
  QLabel* getLabelSoldeCompte();
  /*!
   * \brief getLabelIdCompte renvoie un pointeur sur
   * l'objet contenant le solde du compte sélectionné
   * dans la liste des comptes.
   * \return Un pointeur sur un objet QLabel
   */
  QLabel* getLabelSoldeCompte() const;
  /*!
   * \brief getCompte renvoie un pointeur vers le compte associée à l'opération
   * n°i
   * \return un pointeur sur un objet AbstractCompte
   */
  comptes::AbstractCompte const* getCompte(int i);
  /*!
   * \brief getRef renvoie la référence associée à la transaction
   * \return un objet QString
   */
  QString const getReference();
  /*!
   * \brief getDate renvoie la date associée à la transaction
   * \return un objet QDate
   */
  QDate const getDate();
  /*!
   * \brief getDesc renvoie la description de la transaction
   * \return un objet QString
   */
  QString const getDescription();
  /*!
   * \brief getIdCompteOperation renvoie l'id du compte pour l'opération n°i
   * \param i le numéro de la ligne
   * \return Un objet QString contenant le nom du compte concerné
   */
  int getIdCompteOperation(int i);
  /*!
   * \brief getNomCompteOperation renvoie le nom du compte pour l'opération n°i
   * \param i le numéro de la ligne
   * \return Un objet QString contenant le nom du compte concerné
   * \details On rappelle que le nom d'un compte n'est pas unique, son id l'est.
   */
  QString getNomCompteOperation(int i);
  /*!
   * \brief getCreditOperation renvoie le montant du crédit pour l'opération n°i
   * \param i le numéro de la ligne
   * \return Un objet Monnaie contenant le montant du crédit
   */
  Monnaie getCreditOperation(int i);
  /*!
   * \brief getDebitOperation renvoie le montant du débit pour l'opération n°i
   * \param i le numéro de la ligne
   * \return Un objet Monnaie contenant le montant du débit
   */
  Monnaie getDebitOperation(int i);
  /*!
   * \brief getOperations() renvoie un vecteur de pointeurs sur les opérations
   * de la transaction
   * \return un QVector<Opération*>
   * \details getOperations crée les opérations présentes dans le formulaire.
   */
  QVector<Operation*> getOperations();
  /*!
   * \brief isOperationCellEmpty indique si une celulle dans la liste des
   * opérations est vide ou non.
   * \param i le numéro de la ligne
   * \param j le numéro de la colonne
   * \return TRUE si la cellule est vide
   * \return FALSE si la celulle est non vide
   * \details Lorsque qu'une cellule d'un QTableWidget est éditée pour la
   * première fois, un objet QTableWidgetItem est crée. Toute suppression
   * ultérieure du contenu de la celulle ne détruira pas l'objet. Pour bien
   * vérifier si une cellule est vide, il faut donc vérifier l'existence d'un
   * QTableWidgetItem, et s'il existe si le texte est vide !
   */
  bool isOperationCellEmpty(int i, int j);

 public slots:
  /*!
   * \brief Slot pushButton_ajouter_operation
   * \details Ce slot utilise les fonctions d'auto-connexion de Qt et uic.
   * Il sert à rajouter une ligne au tableau des opérations.
   */
  void on_pushButton_ajouter_operation_clicked();
  /*!
   * \brief Slot on_nomLineEdit_textChanged
   * \param text le texte courant
   * \details Ce slot utilise les fonctions d'auto-connexion de Qt et uic.
   * Il sert à retirer une ligne au tableau des opérations.
   */
  void on_pushButton_retirer_operation_clicked();

  /*!
   * \brief Slot on_buttonBox_accepted
   * \details Ce slot utilise les fonctions d'auto-connexion de Qt et uic.
   * Il sert à vérifier la présence des informations rentrées. L'intégrité des
   * données est assurée par les contructeurs des objets opération et
   * transaction qui se chargeront de rééquilibrer dans le cas d'une opération,
   * et de refuser la construction dans le cas d'une transaction mal équilibrée.
   */
  void on_DialogButtons_accepted();
  /*!
   * \brief on_tableWidget_liste_operation_currentItemChanged
   * \param current Pointeur sur l'objet QTableWidgetItem qui avait précédemment
   * le focus
   * \param previous Pointeur sur l'objet QTableWidgetItem qui a actuellement
   * le focus
   * \details Ce slot utilise les fonctions d'auto-connexion de Qt et uic.
   * Il sert à actualiser les informations du compte sélectionné dans
   * le tableau d'opérations.
   */
  void on_tableWidget_liste_operations_currentItemChanged(
      QTableWidgetItem* current, QTableWidgetItem* previous);
};

#endif  // LO21_FENETRECREATIONCOMPTE_HPP
