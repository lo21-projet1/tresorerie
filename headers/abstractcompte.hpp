/*!
 * \file abstractcompte.hpp
 * \author Bastien BOURÉ
 * \author Rémy HUET <utc@remyhuet.fr>
 * \date 2020-06-09
 * \brief Définition de la casse AbstractCompte
 */

#ifndef LO21_ABSTRACTCOMPTE_HPP
#define LO21_ABSTRACTCOMPTE_HPP

#include <QDebug>
#include <QString>

#include "monnaie.hpp"

/*!
 * \class AbstractCompe abstractcompte.hpp
 * \brief Classe AbstractCompte
 * \details Le but de cette classe est de fournir une interface
 * pour l'instanciation des différents comptes
 */

namespace comptes {

/*!
 * \enum TypeCompte abstractcompte.hpp
 * \brief enum TypeCompte
 * \details Enumération des 4 types de compte possibles
 */
enum TypeCompte : int { ACTIFS, PASSIFS, REVENUS, DEPENSES };

class AbstractCompte {
 public:
  /*!
   * \brief Retourne l'id du compte
   * \return l'id
   */
  constexpr int getId() const noexcept { return id; };

  /*!
   * \brief Retourne le type du compte
   * \return type
   */
  virtual TypeCompte getType() const noexcept = 0;

  /*!
   * \brief Retourne le nom du compte
   * \return le nom
   */
  inline QString getNom() const { return nom; };

  /*!
   * \brief Retourne le solde du compte
   * \return le solde
   */
  virtual Monnaie getSolde() const = 0;

  /*!
   * \brief Retourne l'attribut virtuel
   * \return virtuel
   */
  virtual bool isVirtuel() const noexcept = 0;

  /*!
   * \brief Affecte l'attribut nom avec un QString n
   * \param n le nom du compte à créer
   */
  void setNom(const QString& n) { nom = n; };

  /*!
   * \brief Retourne le solde à la date donnée
   * \param date à laquelle on veut le solde du compte
   * \return le solde à la date donnée
   */
  virtual Monnaie getSoldeByDate(QDate const& date) const = 0;
  /*!
   * \brief Destructeur par défaut
   */
  virtual ~AbstractCompte() = default;

 protected:
  /*!
   * \brief Construit un objet AbstractCompte à partir d'un QString
   * \param n le nom du compte à créer
   * \param i l'id du compte
   */
  AbstractCompte(const QString& n, int i) : id(i), nom(n) {}
  AbstractCompte() = default;

  int id;       //!< L'id du compte
  QString nom;  //!< Le nom du compte
};
}  // namespace comptes

#endif
