/*!
 * \file comptefactory.hpp
 * \author Rémy HUET <utc@remyhuet.fr>
 * \date 2020-06-14
 * \brief Définition de la classe CompteFactory
 */

#ifndef LO21_COMPTEFACTORY_HPP
#define LO21_COMPTEFACTORY_HPP

#include <QDate>

#include "headers/abstractcomptevirtuel.hpp"
#include "headers/compte.hpp"

namespace comptes {

/*!
 * \class CompteFactory comptefactory.hpp
 * \brief La classe CompteFectory est chargée de créer des objets de type
 * AbstractCompte
 * \details CompteFactory est une « classe statique » qui ne peut pas être
 * instanciée
 */
class CompteFactory {
 public:
  /*!
   * \brief Le constructeur par défaut est marqué delete car la classe ne
   * contient que des méthodes classiques et n'est donc pas instanciable
   */
  CompteFactory() = delete;

  /*!
   * \brief Crée un compte en déduisant son type de celui de son parent
   * \param nom le nom à attribuer au compte
   * \param parent le compte parent, qui donnera le type du compte créé
   * \param virtuel indique si le compte créé doit être virtuel ou non
   * \param solde un solde initial peut être donné à un compte non viruel
   * \param compteSInitial pointeur vers le compte de passif duquel vient le
   * solde initial
   * \param dateSInitial la date associée à la transaction réglant le solde
   * initial du compte
   * \param dateDernierRapprochement la date du dernier rapprochement pour
   * les comptes réels
   * \return un pointeur vers le nouveau compte ainsi créé
   *
   * \details La fonction utilise le type du parent pour créer un compte avec
   * les informations données. Ensuite, elle ajoute le compte créé à la liste
   * des fils du compte parent.
   * Si le solde initial est différent de 0, les attributs compteSInitial et
   * dateSInitial doivent obligatoirement être renseignés.
   * Si une date de dernier rapprochement est renseignée, l'attribut virtuel
   * doit obligatoirement être à false
   * L'id du compte est demandé au StorageManager
   */
  static AbstractCompte* creerCompte(
      const QString& nom, AbstractCompte& parent, bool virtuel,
      const Monnaie& solde = Monnaie("0,00"),
      Compte<PASSIFS>* compteSInitial = nullptr,
      const QDate& dateSInitial = QDate(),
      const QDate& dateDernierRapprochement = QDate::currentDate());

  /*!
   * \brief Crée un compte d'un type donné
   * \details On ne peut pas donner de parent à un Compte dont on force le type
   * Si le solde initial est différent de 0, les attributs compteSInitial et
   * dateSInitial doivent obligatoirement être renseignés
   * Si une date de dernier rapprochement est renseignée, l'attribut virtuel
   * doit obligatoirement être à false
   * \param nom le nom à attribuer au compte
   * \param type le type du Compte
   * \param virtuel indique si le compte créé doit être virtuel ou non
   * \param solde un solde initial peut être donné à un compte non virtuel
   * \param compteSInitial pointeur vers le compte de passif duquel vient le
   * solde initial
   * \param dateSInitial la date associée à la transaction réglant le solde
   * initial du compte
   * \param dateDernierRapprochement la date du dernier rapprochement pour
   * les comptes réels
   * \return un pointeur vers le nouveau compte ainsi créé
   */
  static AbstractCompte* creerCompte(
      const QString& nom, TypeCompte type, bool virtuel,
      const Monnaie& solde = Monnaie("0,00"),
      Compte<PASSIFS>* compteSInitial = nullptr,
      const QDate& dateSInitial = QDate(),
      const QDate& dateDernierRapprochement = QDate::currentDate());

  /*!
   * \brief Crée un compte du type donné avec un id donné
   * \param id l'identifiant unique du compte
   * \param nom le nom à attribuer au compte
   * \param type le type du compte
   * \param virtuel indique si le compte créé doit être virtuel ou non
   * \param solde un solde initial peut être donné à un compte non virtuel
   * \param compteSInitial pointeur vers le compte de passif duquel vient le
   * solde initial
   * \param dateSInitial la date associée à la transaction réglant
   * le solde initial du compte
   * \param dateDernierRapprochement la date du dernier rapprochement pour
   * les comptes réels
   * \return un pointeur vers le nouveau compte ainsi créé
   * \details On ne peut pas donner de parent à un Compte dont on force le type
   * Si le solde initial est différent de 0, les attributs compteSInitial et
   * dateSInitial doivent obligatoirement être renseignés
   * Si une date de dernier rapprochement est renseignée, l'attribut virtuel
   * doit obligatoirement être à false
   */
  static AbstractCompte* creerCompte(
      unsigned int id, const QString& nom, TypeCompte type, bool virtuel,
      const Monnaie& solde = Monnaie("0,00"),
      Compte<PASSIFS>* compteSInitial = nullptr,
      const QDate& dateSInitial = QDate(),
      const QDate& dateDernierRapprochement = QDate::currentDate());

  /*!
   * \brief Crée un compte aec un parent et un id donnés
   * \param id l'identifiant unique du compte
   * \param nom le nom à attribuer au compte
   * \param parent le compte parent, qui donnera le type du compte créé
   * \param virtuel indique si le compte est virtuel ou non
   * \param solde un solde initial peut être donné à un compte non virtuel
   * \param compteSInitial pointeur vers le compte de passif duquel vient le
   * solde initial
   * \param dateSInitial la date associée à la transation réglant le solde
   * initial du compte
   * \param dateDernierRapprochement la date du dernier rapprochement pour
   * les comptes réels
   * \return un pointeur vers le nouveau compte ainsi créé$
   * \details La fonction utilise le type du parent pour créer un compte avec
   * les informations données. Ensuite, elle ajoute le compte créé à la liste
   * des fils du compte parent.
   * Si le solde initial est différent de 0, les attributs compteSInitial et
   * dateSInitial doivent obligatoirement être renseignés.
   * Si une date de dernier rapprochement est renseignée, l'attribut virtuel
   * doit obligatoirement être à false
   */
  static AbstractCompte* creerCompte(
      unsigned int id, const QString& nom, AbstractCompte& parent, bool virtuel,
      const Monnaie& solde = Monnaie("0,00"),
      Compte<PASSIFS>* compteSInitial = nullptr,
      const QDate& dateSInitial = QDate(),
      const QDate& dateDernierRapprochement = QDate::currentDate());

  /*!
   * \brief Charge un compte existant
   * \param id
   * \param nom
   * \param isVirtuel
   * \param type
   * \param solde le solde actuel du compte
   * \param date la date du dernier rapprochement
   * \return
   */
  static AbstractCompte* loadCompte(unsigned int id, const QString& nom,
                                    bool isVirtuel, TypeCompte type,
                                    const Monnaie& solde = Monnaie("0,00"),
                                    const QDate& date = QDate::currentDate());

  /*!
   * \brief Charge un compte existant
   * \param id
   * \param nom
   * \param isVirtuel
   * \param parent
   * \param solde le solde actuel du compte
   * \param date la date du dernier rapprochement
   * \return
   */
  static AbstractCompte* loadCompte(unsigned int id, const QString& nom,
                                    bool isVirtuel, AbstractCompte* parent,
                                    const Monnaie& solde = Monnaie("0,00"),
                                    const QDate& date = QDate::currentDate());

 private:
  /*!
   * \brief Permet d'initialiser le solde d'un compte à la création
   * \param compte dont on doit initialiser le solde
   * \param solde le solde à donner au compte
   * \param compteSInitial le compte Passif associé à la transaction
   * \param dateSInitial la date de la transaction associée à la mise du
   * solde
   */
  static void setSoldeInitial(AbstractCompte* compte, const Monnaie& solde,
                              Compte<PASSIFS>* compteSInitial,
                              const QDate& dateSInitial);
};
}  // namespace comptes

#endif  // LO21_COMPTEFACTORY_HPP
