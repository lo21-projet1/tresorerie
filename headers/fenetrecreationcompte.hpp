/*!
 * \file fenetrecreationcompte.hpp
 * \author Rémy HUET <utc@remyhuet.fr>
 * \date 2020-06-09
 * \brief Déclaration de la classe FenetreCreationCompte
 * \details Cette classe correspond au formulaire de création d'un compte.
 * Elle est générée grâce à QtDesigner. L'ensembles des éléments associés est
 * donc accessible par le pointeur ui.
 */

#ifndef LO21_FENETRECREATIONCOMPTE_HPP
#define LO21_FENETRECREATIONCOMPTE_HPP

#include <QDate>
#include <QDialog>

#include "headers/compte.hpp"

namespace Ui {
class FenetreCreationCompte;
}

/*!
 * \class FenetreCreationCompte fenetrecreationcompte.hpp
 * \brief Déclaration de la classe FenetreCreationCompte
 * \details Cette classe utilise la classe Ui::FenetreCreationCompte, auto
 * générée par QtDesigner
 */
class FenetreCreationCompte : public QDialog {
  Q_OBJECT

 public:
  /*!
   * \brief Constructeur de la classe FenetreCreationCompte
   * \param parent pointeur vers le widget parent.
   */
  explicit FenetreCreationCompte(QWidget *parent = nullptr);
  /*!
   * Destructeur de la classe FenetreCreationCompte
   */
  ~FenetreCreationCompte();

  /*!
   * \brief Récupère le pointeur vers l'ui de la classe
   * \return ui::FenetreCreationCompte* pointeur vers l'ui de la fenêtre
   * \details Cela permet de récupérer les valeurs des champs du formulaire
   */
  const Ui::FenetreCreationCompte *getUi() const { return ui; }

 public slots:
  /*!
   * \brief Slot on_compteSansParentCheckBox_stateChanged
   * \param state un entier du type énuméré Qt::CheckState
   * \details Ce slot utilise les fonctions d'auto-connexion de Qt et uic.
   * Il sert à changer les états enabled du formulaire en fonction de si le
   * compte a un parent ou non.
   * Si le compte a un parent, on ne peut pas choisir son type. Si un compte n'a
   * pas de parent, on ne peut pas lui donner de parent.
   */
  void on_compteSansParentCheckBox_stateChanged(int state);
  /*!
   * \brief Slot on_nomLineEdit_textChanged
   * \param text le text courant
   * \details Ce slot utilise les fonctions d'auto-connexion de Qt et uic.
   * Il sert à donner une valeur par défaut  la transaction de solde initial en
   * fonction du nom du compte créé.
   */
  void on_nomLineEdit_textChanged(const QString &text);

  /*!
   * \brief Slot on_compteVirtuelCheckBox_stateChanged
   * \param state un entier du type énuméré Qt::CheckState
   * \details Ce slot utilise les fonctions d'auto-connexion de Qt et uic.
   * Il sert à interdire de mettre un solde initial à un compte virtuel.
   */
  void on_compteVirtuelCheckBox_stateChanged(int state);
  /*!
   * \brief Slot on_typeCompteComboBox_currentIndexChanged
   * \param index l'index de la ligne sélectionnée actuellement
   * \details Ce slot utilise les fonctions d'auto-connexion de Qt et uic.
   * Il sert à interdire de créer des comptes de dépense ou de recettes avec un solde initial
   */
  void on_typeCompteComboBox_currentIndexChanged(int index);
  /*!
   * \brief Slot on_compteParentComboBox_currentIndexChanged
   * \param index l'index de la ligne sélectionnée actuellement
   * \details Ce slot utilise les fonctions d'auto-connexion de Qt et uic.
   * Il sert à interdire de créer un fils d'un comptes de dépense ou de recettes ayant un solde initial
   */
  void on_compteParentComboBox_currentIndexChanged(int index);
  /*!
   * \brief Slot on_buttonBox_accepted
   * \details Ce slot utilise les fonctions d'auto-connexion de Qt et uic.
   * Il sert à verifier la cohérence des informations lors de la création du
   * compte.
   */ 
  void on_buttonBox_accepted();

  /*!
   * \brief getNom
   * \return Renvoie le nom du compte à créer
   */
  QString getNom() const;
  /*!
   * \brief isVirtuel
   * \return true si le compte à créer est virtuel, false sinon
   */
  bool isVirtuel() const;
  /*!
   * \brief isSansParent
   * \return true si le compte n'a pas de parent (et donc un type spécifié)
   */
  bool isSansParent() const;
  /*!
   * \brief getType
   * \return le type du compte à créer (si pas de parent)
   */
  comptes::TypeCompte getType() const;
  /*!
   * \brief getParent
   * \return Un pointeur vers le compte parent
   */
  comptes::AbstractCompte *getParent() const;
  /*!
   * \brief hasSoldeInitial
   * \return true si le compte est associé à un solde initial
   */
  bool hasSoldeInitial() const;
  /*!
   * \brief getLabelSoldeInitial
   * \return le label associé à la transaciont du solde initial
   */
  QString getLabelSoldeInitial() const;
  /*!
   * \brief getCompteSoldeInitial
   * \return le compte duquel vient le solde initial
   */
  comptes::Compte<comptes::TypeCompte::PASSIFS> *getCompteSoldeInitial() const;
  /*!
   * \brief getDateSoldeInitial
   * \return La date associée à la transaction du solde initial
   */
  QDate getDateSoldeInitial() const;
  /*!
   * \brief getMontantSoldeInitial
   * \return Le montant du solde initial
   */
  Monnaie getMontantSoldeInitial() const;

 private:
  /*!
   * \brief Composant UI
   * \details Ce composant permet l'interface avec le code auto-généré par
   * les outils UI de Qt. C'est par lui que l'on accède aux différents
   * éléments de la fenêtre
   */
  Ui::FenetreCreationCompte *ui;
};

#endif  // LO21_FENETRECREATIONCOMPTE_HPP
