/*!
 * \file fenetremodifiertransaction.hpp
 * \author Rémy HUET <utc@remyhuet.fr>
 * \date 2020-06-13
 * \brief Déclarations de la fenetre de modification des transactions
 */

#include "headers/fenetreajoutertransaction.hpp"

class FenetreModifierTransaction : public FenetreAjouterTransaction {
 public:
  /*!
 * \brief FenetreModifierTransaction hérite de FenetreAjouterTransaction pour
 * permettre de modifier une transaction non rapprochée.
 * \param parent Le parent du QWidget, ici la fenêtre principale
 * \param transacion La transaction qui doit être modifiée
 */
  FenetreModifierTransaction(QWidget* parent, Transaction* transaction);
  /*!
   * \brief ~FenetreModifierTransaction Destructeur par défaut
   */
  virtual ~FenetreModifierTransaction() = default;
};
