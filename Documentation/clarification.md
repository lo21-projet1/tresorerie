# LO21 clarification sujet

## Comptes

Gestion d'un unique compte racine (pour le moment)

-> Intérêt compte racine ? => on garde ça coûte rien

Types de comptes :
* Actifs = possessions
* Passifs = sommes dûes
* Revenus
* Dépenses

Arbre de comptes => Pas de restriction sur la profondeur

## Opérations

* 1 ou + comptes émetteurs
* 1 ou + comptes récepteurs

Débit augmente emploi diminue ressource :
* augmente passifs / revenus
* diminue actifs / dépenses

Crédit diminue emploi augmente ressource :
* diminue passifs / revenus
* augmente actifs / dépenses

## Transactions

= échange de valeurs entre comptes (transaction simple vs. répartie)

* Date
* Référence
* Titre
* Plusieurs triplets (`std::tuple` ?)
    * Compte
    * Débit
    * Crédit

Règles de validation :
* Au moins deux comptes (au moins deux triplets)
* Crédits / débits positifs (sinon passés en positif pour l'opposé)
* Pour chaque triplet une seule valeur pas nulle (débit / crédit) sinon retranchement
* **ÉQUILIBRE**

## Solde initial

* Solde à la création d'un compte = transaction depuis Passif:Capitaux propres

Compte actif : Crédite Passif:Capitaux propres & débite Actif:x (montant = solde initial)

## Rapprochement

À une date donnée = comparaison du solde (entre le solde dans l'appli et le solde « réel »).
Si OK => impossible de modifier les transactions avant rapprochement

## Clôture du livre

=> Remise à 0 des comptes de dépense et revenus => compte Résultat (Passif)

Deux transactions réparties :
* Dépenses -> crédite tous les comptes dépense / débite Résultat
* Revenus -> débite tous les comptes revenus / crédite Résultat

Troisième transaction :
* Résultat -> Excédent ou déficit

## Rapports

### Bilan

### Relevé recettes dépenses

### Résultat

## Fonctionnalités (& attentes)

Interface en ligne de commande : pas une priorité donc pas implémentée.

* Gérer les comptes (création / modification / suppression ? (pas d'affichage ni modif))
* Gérer les transactions (création / modification / suppression si elles n'ont pas été rapprochées)
* Calcul de solde
* Clôture
* Rapprochement
