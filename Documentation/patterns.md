# Les Design Patterns utilisés # {#patterns}

Pour notre projet, nous avons utilisé et implémenté certains Design Patterns.
L'objectif de cette page est de fournir des explications plus détaillées sur les choix techniques d'implémentation de Design Patterns, ainsi que sur leur utilisation dans notre code.
Cette page est en complément de la documentation Doxygen de notre programme. Il peut être nécessaire d'aller voir celle-ci pour plus de détails sur les fonctions.

## Design Pattern Singleton

L'objectif de ce Design Pattern est de proposer, pour une classe donnée, le fonctionnement suivant :
* La classe n'a qu'une seule instance à un moment donné dans le programme
* Cette instance est accessible depuis n'importe quel endroit du programme.

### Implémentation

L'implémentation retenue est la suivante :

```cpp
template <class C>
class Singleton {
 protected:
  Singleton() = default;

 public:
  Singleton(const Singleton&) = delete;
  Singleton& operator=(const Singleton&) = delete;

  static std::shared_ptr<C> getInstance() {
    static std::shared_ptr<C> instance(new C);

    return instance;
  }
};
```

### Utilisation

Pour qu'une classe `Fille` puisse avoir un comportement de Singleton, elle doit être déclarée de la manière suivante :

```cpp
class Fille : public Singleton<Fille> {
private:
    friend class Singleton<Fille>;
    Fille() {
        // À définir ou à =default
    };

public:
    // Méthodes publiques souhaitées
}
```

La classe peut ensuite définir autant de membres et de méthodes privées ou publiques que souhaité.
Il est **primordial** que la déclaration `friend class` soit respectée et que le constructeur `Fille()` soit redéclaré privé.

### Choix techniques et explications

Le but de cette classe est de fournir une interface simple de laquelle une classe peut hériter pour être une classe Singleton.

Tout d'abord, le constructeur par défaut `Singleton()` est déclaré privé, pour que la classe ne puisse pas être construite par ailleurs.
Ensuite, le constructeur et l'opérateur d'affectation par copie sont déclarés `deleted`. À noter que cette déclaration est publique, pour que tout le monde sache qu'ils sont supprimés (meilleure pratique que de les déclarer privés).

On retrouve enfin la fonction statique publique `getInstance()` qui crée la variable instance de la classe template `C` (la classe fille) de manière statique et renvoie un pointeur dessus.

Quelques précisions :

* L'utilisation de template pour la classe `Singleton` et l'héritage `class Fille : public Singleton<Fille>` permet, avec la déclaration `friend class Singleton<Fille>` de déléguer la construction de la classe Fille (ayant pourtant un constructeur privé) à la classe mère, et donc de ne pas avoir à redéfinir la méthode `getInstance()` dans chaque classe fille.
* Comme les constructeurs et opérateurs de copie sont `=deleted` pour la classe mère, ils ne pourront pas être utilisés pour la classe fille. Il n'y a donc pas besoin de le refaire pour la classe `Fille`.

À propos de l'utilisation des `std::shared_ptr` :
L'utilisation de tels pointeurs dits « pointeurs intelligents » permet les comportements suivants :
* Le passage de l'instance par `getInstance()` se fait par pointeur. Il est de toute façon impossible de le faire par copie (car la classe n'est pas copiable). Il faut donc utiliser des références ou des pointeurs.
* L'utilisation de tels pointeurs permet l'auto destruction de la classe `Fille` à la fin du programme. En effet, les `std::shared_ptr` procèdent à la destruction de la ressource pointée dès lors que plus aucun `std::shared_ptr` n'y fait référence. En rappelant qu'un pointeur est une variable **locale** qui est détruite à la fin du scope dans laquelle elle est définie, on obtient qu'à la fin d'un programme, toutes les références vers `instance` auront été détruites, sauf la référence statique. Celle-ci sera supprimée automatiquement à la fin du programme. Il n'y aura plus de référence vers `instance`, qui sera automatiquement détruite.
* À noter que l'utilisation de tels pointeurs ne dispense pas de définir un destructeur pour `Fille` si cela est nécessaire, mais s'assure juste qu'il sera forcément appelé à la fin du programme.


## Autres

D'autres Design Patterns on été utilisés pour la réalisation de ce projet, mais leur implémentation technique ne nécessite pas de documentatio particulière.
Ils sont plutôt décrits conceptuellement dans le rapport joint au projet.
