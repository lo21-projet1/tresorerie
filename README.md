# Projet # {#mainpage}

Ce dépôt contient les sources de notre projet de LO21 pour le semestre P20.
Le groupe est composé des personnes suivantes :

* Arthur Wacquez
* Bastien Bouré
* Noé Delattre
* Pascal Quach
* Rémy Huet
* Sébastien Darche

## License

Le projet est distribué sous license [GNU GPLv3](./LICENSE)

## Documentation

La documentation générée par Doxygen pour le projet est accessible via [ce lien](https://lo21-projet1.gitlab.utc.fr/tresorerie/html)

## Clarification

La note de clarification du projet est disponible [ici](./Documentation/clarification.md)
